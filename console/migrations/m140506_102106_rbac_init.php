<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

/**
 * Initializes RBAC tables
 *
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class m140506_102106_rbac_init extends \yii\db\Migration
{
    /**
     * @throws yii\base\InvalidConfigException
     * @return DbManager
     */
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    /**
     * @return bool
     */
    protected function isMSSQL()
    {
        return $this->db->driverName === 'mssql' || $this->db->driverName === 'sqlsrv' || $this->db->driverName === 'dblib';
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET UTF8 COLLATE UTF8_UNICODE_CI ENGINE=INNODB';
        }

        $this->createTable($authManager->ruleTable, [
            'NAME' => $this->string(64)->notNull(),
            'DATA' => $this->binary(),
            'CREATED_AT' => $this->integer(),
            'UPDATED_AT' => $this->integer(),
            'PRIMARY KEY (NAME)',
        ], $tableOptions);

        $this->createTable($authManager->itemTable, [
            'NAME' => $this->string(64)->notNull(),
            'TYPE' => $this->smallInteger()->notNull(),
            'DESCRIPTION' => $this->text(),
            'RULE_NAME' => $this->string(64),
            'DATA' => $this->binary(),
            'CREATED_AT' => $this->integer(),
            'UPDATED_AT' => $this->integer(),
            'PRIMARY KEY (NAME)',
            'FOREIGN KEY (RULE_NAME) REFERENCES ' . $authManager->ruleTable . ' (NAME)'.
                ($this->isMSSQL() ? '' : ' ON DELETE SET NULL ON UPDATE CASCADE'),
        ], $tableOptions);
        $this->createIndex('IDX-AUTH_ITEM-TYPE', $authManager->itemTable, 'TYPE');

        $this->createTable($authManager->itemChildTable, [
            'PARENT' => $this->string(64)->notNull(),
            'CHILD' => $this->string(64)->notNull(),
            'PRIMARY KEY (PARENT, CHILD)',
            'FOREIGN KEY (PARENT) REFERENCES ' . $authManager->itemTable . ' (NAME)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
            'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (NAME)'.
                ($this->isMSSQL() ? '' : ' ON DELETE CASCADE ON UPDATE CASCADE'),
        ], $tableOptions);

        $this->createTable($authManager->assignmentTable, [
            'ITEM_NAME' => $this->string(64)->notNull(),
            'USER_ID' => $this->string(64)->notNull(),
            'CREATED_AT' => $this->integer(),
            'PRIMARY KEY (ITEM_NAME, USER_ID)',
            'FOREIGN KEY (ITEM_NAME) REFERENCES ' . $authManager->itemTable . ' (NAME) ON DELETE CASCADE ON UPDATE CASCADE',
        ], $tableOptions);

        if ($this->isMSSQL()) {
            $this->execute("CREATE TRIGGER DBO.TRIGGER_AUTH_ITEM_CHILD
            ON DBO.{$authManager->itemTable}
            INSTEAD OF DELETE, UPDATE
            AS
            DECLARE @OLD_NAME VARCHAR (64) = (SELECT NAME FROM DELETED)
            DECLARE @NEW_NAME VARCHAR (64) = (SELECT NAME FROM INSERTED)
            BEGIN
            IF COLUMNS_UPDATED() > 0
                BEGIN
                    IF @OLD_NAME <> @NEW_NAME
                    BEGIN
                        ALTER TABLE {$authManager->itemChildTable} NOCHECK CONSTRAINT FK__AUTH_ITEM__CHILD;
                        UPDATE {$authManager->itemChildTable} SET CHILD = @NEW_NAME WHERE CHILD = @OLD_NAME;
                    END
                UPDATE {$authManager->itemTable}
                SET NAME = (SELECT NAME FROM INSERTED),
                TYPE = (SELECT TYPE FROM INSERTED),
                DESCRIPTION = (SELECT DESCRIPTION FROM INSERTED),
                RULE_NAME = (SELECT RULE_NAME FROM INSERTED),
                DATA = (SELECT DATA FROM INSERTED),
                CREATED_AT = (SELECT CREATED_AT FROM INSERTED),
                UPDATED_AT = (SELECT UPDATED_AT FROM INSERTED)
                WHERE NAME IN (SELECT NAME FROM DELETED)
                IF @OLD_NAME <> @NEW_NAME
                    BEGIN
                        ALTER TABLE {$authManager->itemChildTable} CHECK CONSTRAINT FK__AUTH_ITEM__CHILD;
                    END
                END
                ELSE
                    BEGIN
                        DELETE FROM DBO.{$authManager->itemChildTable} WHERE PARENT IN (SELECT NAME FROM DELETED) OR CHILD IN (SELECT NAME FROM DELETED);
                        DELETE FROM DBO.{$authManager->itemTable} WHERE NAME IN (SELECT NAME FROM DELETED);
                    END
            END;");
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        if ($this->isMSSQL()) {
            $this->execute('DROP TRIGGER DBO.TRIGGER_AUTH_ITEM_CHILD;');
        }

        $this->dropTable($authManager->assignmentTable);
        $this->dropTable($authManager->itemChildTable);
        $this->dropTable($authManager->itemTable);
        $this->dropTable($authManager->ruleTable);
    }
}
