<?php

use yii\db\Expression;
use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%USER}}', [
            'ID' => $this->primaryKey(),
            'USERNAME' => $this->string()->notNull()->unique(),
            'EMAIL' => $this->string()->notNull()->unique(),
            'FULL_NAME' => $this->string(),
            'DISPLAY_NAME' => $this->string(),
            'PASSWORD_HASH' => $this->string()->notNull(),
            'PASSWORD_RESET_TOKEN' => $this->string()->unique(),
            'AUTH_KEY' => $this->string(32)->notNull(),
            'STATUS' => $this->smallInteger()->notNull()->defaultValue(10),
            'CREATED_AT' => $this->dateTime()->notNull(),
            'UPDATED_AT' => $this->dateTime()->notNull(),
            'LOGIN_AT' => $this->dateTime(),
        ], $tableOptions);

        /**
         * Insert super administrator.
         * Initialize super administrator with user superadmin and password superadmin.
         * After installing this app success, change the username and password of superadmin immediately.
         */
        $this->insert('{{%USER}}', [
            'ID' => 1,
            'USERNAME' => 'superadmin',
            'EMAIL' => 'superadmin@uns.ac.id',
            'FULL_NAME' => 'Super Administrator',
            'DISPLAY_NAME' => 'Super Admin',
            'PASSWORD_HASH' => '$2y$13$WJIxqq6WBZUw7tyfN2oiH.WJtPntvLMjs6NG9uW0M3Lh71lImaEyu',
            'PASSWORD_RESET_TOKEN' => null,
            'AUTH_KEY' => '7QvEmdZDvaSxM1-oYoWkKso0ws6AHTX1',
            'STATUS' => 10,
            'CREATED_AT' => new Expression('NOW()'),
            'UPDATED_AT' => new Expression('NOW()'),
            'LOGIN_AT' => new Expression('NOW()'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%USER}}');
    }
}
