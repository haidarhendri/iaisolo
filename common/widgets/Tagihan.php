<?php

use common\models\IaiRiwayatIuran;
use common\models\User;
use kartik\export\ExportMenu;
use simpak\models\RefIuran;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$query = IaiRiwayatIuran::find()
    ->select(['A.ID_JENIS_IURAN', 'B.ID AS ID_USER'])
    ->from(['A' => IaiRiwayatIuran::tableName()])
    ->joinWith([
        'idUser' => function (ActiveQuery $q) {
            return $q->from(['B' => User::tableName()]);
        },
        'jenisIuran' => function (ActiveQuery $q) {
            return $q->from(['C' => RefIuran::tableName()]);
        },
    ], false)
    ->where([
        'B.ID' => Yii::$app->user->identity->ID,
        'A.STATUS_BAYAR' => 1
    ])
    ->distinct()
    ->asArray()->all();

$implode = array_map(function ($qy) {
    return $qy['ID_JENIS_IURAN'];
}, $query);

$list = RefIuran::find()
    ->select(['NAMA_IURAN'])
    ->where(['NOT IN', 'ID', $implode])
    ->asArray()->all();

if ($list) {
?>

    <div class="box box-danger">
        <div class="box-header with-border">
            <h3 class="box-title"><b>TAGIHAN ANDA</b></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></button>
                    <?= Html::a(
                        'Bayar Disini',
                        Url::to(['/anggota/iuran/index']),
                        ['class' => 'btn btn-success btn-xs']
                    ) ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <?php
            foreach ($list as $jenis) { ?>
                <div class="alert alert-danger">
                    <p><i class="icon fa fa-ban"></i> <?= $jenis['NAMA_IURAN'] ?></p>
                </div>
            <?php } ?>
        </div>
    </div>
<?php } ?>