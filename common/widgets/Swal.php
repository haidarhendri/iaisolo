<script>$("#ajaxCrudModal").modal("hide");</script>

<?php

use aryelds\sweetalert\SweetAlert;
use yii\helpers\Html;
use yii\bootstrap\Modal;

foreach (Yii::$app->session->getAllFlashes() as $message) {
    echo SweetAlert::widget([
        'options' => [
            'title' => (!empty($message['title'])) ? Html::encode($message['title']) : 'Title Not Set!',
            'text' => (!empty($message['text'])) ? Html::encode($message['text']) : 'Text Not Set!',
            'type' => (!empty($message['type'])) ? $message['type'] : SweetAlert::TYPE_INFO,
            'timer' => (!empty($message['timer'])) ? $message['timer'] : 4000,
            'showConfirmButton' =>  (!empty($message['showConfirmButton'])) ? $message['showConfirmButton'] : true
        ]
    ]);
} 

Modal::begin([
    'id' => 'ajaxCrudModal',
    'footer' => '',
    'size' => Modal::SIZE_LARGE,
]);
Modal::end();
Modal::begin([
    'id' => 'second-modal',
    'size' => Modal::SIZE_LARGE,
    'footer' => '',
]);
Modal::end();

