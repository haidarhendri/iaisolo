<?php
/**
 * @link https://iaisolo.id/admin/
 * @author Haidar H. Setyawan <13nightevil@gmail.com>
 * @copyright Copyright (c) 2019 NEXPLAN
 */

namespace common\helpers;

use Yii;

/**
 * Class DateHelper
 * @package common\helpers
 */
class DateHelper
{
    /**
     * @param int $start
     * @param int $end
     * @param int $order
     * @return array
     */
    public static function listYears($start = 1990, $end = 2018, $order = SORT_DESC)
    {
        $items = [];

        if ($order === SORT_ASC) {
            for ($i = $start; $i <= $end; $i++) {
                $items[$i] = $i;
            }
        } else {
            for ($i = $end; $i >= $start; $i--) {
                $items[$i] = $i;
            }
        }

        return $items;
    }

    /**
     * @param int $start
     * @param int $end
     * @param int $order
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public static function listMonths($start = 1, $end = 12, $order = SORT_ASC)
    {
        $items = [];

        if ($order === SORT_ASC) {
            for ($i = $start; $i <= $end; $i++) {
                $items[$i] = Yii::$app->formatter->asDate('2018-' . $i . '-01', 'php:F');
            }
        } else {
            for ($i = $end; $i >= $start; $i--) {
                $items[$i] = Yii::$app->formatter->asDate('2018-' . $i . '-01', 'php:F');
            }
        }

        return $items;
    }
}