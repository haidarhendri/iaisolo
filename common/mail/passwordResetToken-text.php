<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->PASSWORD_RESET_TOKEN]);
?>
Hi, <?= $user->USERNAME ?>,

Klik link dibawah ini untuk menyetelel ulang kata sandi anda :

<?= $resetLink ?>
