<?php

namespace common\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_RIWAYAT_AGENDA".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property int|null $ID_KEGIATAN
 * @property int|null $IS_KLAIM
 * @property string|null $FILE_URL
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiRiwayatAgenda extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'RIWAYAT_AGENDA';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_RIWAYAT_AGENDA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'ID_KEGIATAN', 'IS_BAYAR', 'IS_KLAIM', 'IS_HADIR', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['FILE_URL'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'Id Transaksi',
            'ID_KEGIATAN' => 'Id Kegiatan',
            'IS_BAYAR' => 'Status Bayar',
            'IS_KLAIM' => 'Status Klaim',
            'IS_HADIR' => 'Status Kehadiran',
            'FILE_URL' => 'File Url',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }
    public function getIdAgenda()
    {
        return $this->hasOne(IaiAgenda::className(), ['ID' => 'ID_KEGIATAN']);
    }
    public function getIdDokumen()
    {
        return $this->hasOne(IaiTransaksiDokumen::className(), ['ID_TRANSAKSI' => 'ID_TRANSAKSI']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
