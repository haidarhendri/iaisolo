<?php

namespace common\models\referensi;

use common\models\referensi\RefJenisDokumen;
use common\models\referensi\RefJenisUpload;
use Yii;

/**
 * This is the model class for table "CFG_JENIS_DOKUMEN".
 *
 * @property int $ID Primary key AI
 * @property int $ID_JENIS_UPLOAD ID from REF_JENIS_UPLOAD
 * @property int $ID_JENIS_DOKUMEN ID from REF_JENIS_DOKUMEN
 * @property int $ID_TIPE_DOKUMEN 1: No Upload 2: Upload 3: Sinkron
 * @property int $IS_REQUIRED 0: Tidak 1: Ya
 * @property string $CREATE_DATE
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_IP
 */
class CfgJenisDokumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'CFG_JENIS_DOKUMEN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_JENIS_UPLOAD', 'ID_JENIS_DOKUMEN'], 'required'],
            [['ID_JENIS_UPLOAD', 'ID_JENIS_DOKUMEN', 'ID_TIPE_DOKUMEN', 'IS_REQUIRED', 'IS_SHOW', 'CREATE_BY', 'UPDATE_BY', 'IS_SHOW'], 'integer'],
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_JENIS_UPLOAD' => 'Jenis Upload',
            'ID_JENIS_DOKUMEN' => 'Jenis Dokumen',
            'ID_TIPE_DOKUMEN' => 'Tipe Dokumen',
            'IS_REQUIRED' => 'Is Required',
            'IS_SHOW' => 'Is Show',
            'CREATE_DATE' => 'Create Date',
            'CREATE_BY' => 'Create By',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_DATE' => 'Update Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_IP' => 'Update Ip',
            'IS_SHOW' => 'Is Show'
        ];
    }

    public function getIdJenisUpload()
    {
        return $this->hasOne(RefJenisUpload::classname(), ['ID' => 'ID_JENIS_UPLOAD']);
    }

    public function getIdJenisDokumen()
    {
        return $this->hasOne(RefJenisDokumen::className(), ['ID' => 'ID_JENIS_DOKUMEN']);
    }
}
