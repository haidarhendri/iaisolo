<?php

namespace common\models\referensi;

use common\models\AppAuthAssignment;
use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Request;

/**
 * This is the model class for table "APP_AUTH_ITEM".
 *
 * @property string $NAME
 * @property int $TYPE
 * @property string|null $DESCRIPTION
 * @property string|null $RULE_NAME
 * @property string|null $DATA
 * @property int|null $CREATED_AT
 * @property int|null $UPDATED_AT
 *
 * @property APPAUTHASSIGNMENT[] $aPPAUTHASSIGNMENTs
 * @property APPUSER[] $uSERs
 * @property APPAUTHRULE $rULENAME
 * @property APPAUTHITEMCHILD[] $aPPAUTHITEMChildren
 * @property APPAUTHITEMCHILD[] $aPPAUTHITEMChildren0
 * @property RefAuthItem[] $children
 * @property RefAuthItem[] $pARENTs
 */
class RefAuthItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'APP_AUTH_ITEM';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAME', 'TYPE'], 'required'],
            [['TYPE', 'CREATED_AT', 'UPDATED_AT'], 'integer'],
            [['DESCRIPTION', 'DATA'], 'string'],
            [['NAME', 'RULE_NAME'], 'string', 'max' => 64],
            [['NAME'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'NAME' => 'Name',
            'TYPE' => 'Type',
            'DESCRIPTION' => 'Description',
            'RULE_NAME' => 'Rule Name',
            'DATA' => 'Data',
            'CREATED_AT' => 'Created At',
            'UPDATED_AT' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppAuthAssignment()
    {
        return $this->hasMany(AppAuthAssignment::className(), ['NAME' => 'ITEM_NAME']);
    }


    /**
     * @param string $key
     * @param string $value
     * @param array $conditions
     * @return array
     */
    public static function map($key = 'NAME', $value = 'DESCRIPTION', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, "VALUE" => $value])->where(['TYPE' => 1]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($access = ArrayHelper::remove($conditions, 'access')) {
            /* @var $identity \common\models\User */
            $user = Yii::$app->user;
            $identity = $user->identity;

            if ($user->can('role_operator_fakultas') && !$user->can('role_operator_universitas')) {
                $query->andWhere(['ID' => $identity->ID_UNIT]);
            }
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
