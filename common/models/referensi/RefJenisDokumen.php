<?php

namespace common\models\referensi;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "REF_JENIS_DOKUMEN".
 *
 * @property int $ID
 * @property string|null $JENIS_DOKUMEN
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class RefJenisDokumen extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_JENIS_DOKUMEN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['JENIS_DOKUMEN'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [['ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'JENIS_DOKUMEN' => 'Jenis Dokumen',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataConfig()
    {
        return $this->hasMany(CfgJenisDokumen::class, ['ID_JENIS_DOKUMEN' => 'ID']);
    }

    public static function map($key = 'ID', $value = 'JENIS_DOKUMEN', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, 'VALUE' => $value]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
