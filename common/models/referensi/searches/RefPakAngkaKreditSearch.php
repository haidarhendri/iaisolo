<?php

namespace common\models\referensi\searches;

use common\models\referensi\RefPakAngkaKredit;
use common\models\referensi\RefPakJenisKaryaIlmiah;
use common\models\referensi\RefPakJenisKegiatan;
use common\models\referensi\RefPakUnsur;
use common\models\referensi\RefPakUnsurSub;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;

/**
 * RefPakAngkaKreditSearch represents the model behind the search form about `common\models\referensi\RefPakAngkaKredit`.
 */
class RefPakAngkaKreditSearch extends RefPakAngkaKredit
{
    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['UNSUR', 'SUB_UNSUR']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_UNSUR', 'ID_SUB_UNSUR'], 'integer']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'UNSUR' => 'B.UNSUR',
                'SUB_UNSUR' => 'C.SUB_UNSUR',
                'JENIS_PENELITIAN' => 'D.NAMA',
                'JENIS_KEGIATAN' => 'E.KEGIATAN',
            ]);

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'A.ID_UNSUR' => $this->ID_UNSUR,
            'A.ID_SUB_UNSUR' => $this->ID_SUB_UNSUR,
        ]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'A.ID_UNSUR' => $this->ID_UNSUR,
            'A.ID_SUB_UNSUR' => $this->ID_SUB_UNSUR,
        ]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return RefPakAngkaKredit::find()
            ->from(['A' => RefPakAngkaKredit::tableName()])
            ->joinWith([
                'idUnsur' => function (ActiveQuery $q) {
                    return $q->from(['B' => RefPakUnsur::tableName()]);
                },
                'idSubUnsur' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefPakUnsurSub::tableName()]);
                },
                'idJenisPenelitian' => function (ActiveQuery $q) {
                    return $q->from(['D' => RefPakJenisKaryaIlmiah::tableName()]);
                },
                'idJenisKegiatan' => function (ActiveQuery $q) {
                    return $q->from(['E' => RefPakJenisKegiatan::tableName()]);
                },
            ]);
    }
}
