<?php

namespace common\models\referensi\searches;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\referensi\RefStatusDokumen;

/**
 * RefStatusDokumenSearch represents the model behind the search form about `common\models\referensi\RefStatusDokumen`.
 */
class RefStatusDokumenSearch extends RefStatusDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['STATUS_DOKUMEN', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'STATUS_DOKUMEN', $this->STATUS_DOKUMEN])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return RefStatusDokumen::find();
    }
}
