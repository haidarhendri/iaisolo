<?php

namespace common\models\referensi;

use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "REF_JENIS_UPLOAD".
 *
 * @property int $ID Primary key AI
 * @property string|null $JENIS_UPLOAD
 * @property string|null $CREATE_DATE
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_IP IPv4 or IPv6
 * @property string|null $UPDATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_IP IPv4 or IPv6
 */
class RefJenisUpload extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_JENIS_UPLOAD';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['JENIS_UPLOAD'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'JENIS_UPLOAD' => 'Jenis Upload',
            'CREATE_DATE' => 'Create Date',
            'CREATE_BY' => 'Create By',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_DATE' => 'Update Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public static function map($key = 'ID', $value = 'JENIS_UPLOAD', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, 'VALUE' => $value]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
