<?php

namespace common\models;

use common\models\referensi\RefAuthItem;
use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "APP_AUTH_ASSIGNMENT".
 *
 * @property string $ITEM_NAME
 * @property int $USER_ID
 * @property int|null $CREATED_AT
 *
 * @property APPUSER $uSER
 * @property APPAUTHITEM $iTEMNAME
 */
class AppAuthAssignment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'APP_AUTH_ASSIGNMENT';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ITEM_NAME', 'USER_ID'], 'required'],
            [['USER_ID', 'CREATED_AT'], 'integer'],
            [['ITEM_NAME'], 'string', 'max' => 64],
            [['ITEM_NAME', 'USER_ID'], 'unique', 'targetAttribute' => ['ITEM_NAME', 'USER_ID']],
            [['USER_ID'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['USER_ID' => 'ID']],
            // [['ITEM_NAME'], 'exist', 'skipOnError' => true, 'targetClass' => APPAUTHITEM::className(), 'targetAttribute' => ['ITEM_NAME' => 'NAME']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ITEM_NAME' => 'Item Name',
            'USER_ID' => 'User ID',
            'CREATED_AT' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'USER_ID']);
    }
    
    public function getAuthItem()
    {
        return $this->hasOne(RefAuthItem::className(), ['NAME' => 'ITEM_NAME']);
    }

    // /**
    //  * @return \yii\db\ActiveQuery
    //  */
    // public function getItemName()
    // {
    //     return $this->hasOne(APPAUTHITEM::className(), ['NAME' => 'ITEM_NAME']);
    // }
    // /**
    //  * {@inheritdoc}
    //  */
}
