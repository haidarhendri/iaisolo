<?php

namespace common\models;

use app\models\RefJenjang;
use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_RIWAYAT_PENDIDIKAN".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property string|null $JENJANG
 * @property string|null $NAMA_INSTITUSI
 * @property string|null $NOMOR_IJAZAH
 * @property string|null $TANGGAL_IJAZAH
 * @property string|null $FILE_IJAZAH
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiRiwayatPendidikan extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'PENDIDIKAN';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_RIWAYAT_PENDIDIKAN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'IS_UTAMA', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['TANGGAL_IJAZAH', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['JENJANG'], 'string', 'max' => 5],
            [['NAMA_INSTITUSI', 'NOMOR_IJAZAH', 'FILE_IJAZAH'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [
                [
                    'TANGGAL_IJAZAH',
                    'JENJANG',
                    'NAMA_INSTITUSI',
                    'NOMOR_IJAZAH',
                ],
                'required'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'ID TRANSAKSI',
            'JENJANG' => 'Jenjang',
            'NAMA_INSTITUSI' => 'Nama Institusi',
            'NOMOR_IJAZAH' => 'Nomor Ijazah',
            'TANGGAL_IJAZAH' => 'Tanggal Ijazah',
            'IS_UTAMA' => 'Status Pendidikan',
            'FILE_IJAZAH' => 'File Ijazah',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }

    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }
    
    public function getIdJenjang()
    {
        return $this->hasOne(RefJenjang::className(), ['ID' => 'JENJANG']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
