<?php

namespace common\models;

use app\models\RefKorwil;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "USER".
 *
 * @property int $ID
 * @property string $USERNAME
 * @property string $EMAIL
 * @property string $FULL_NAME
 * @property string $NO_KTA
 * @property string $DISPLAY_NAME
 * @property string $PASSWORD_HASH
 * @property string $PASSWORD_RESET_TOKEN
 * @property string $AUTH_KEY
 * @property int $STATUS
 * @property string $CREATED_AT
 * @property string $UPDATED_AT
 * @property string $LOGIN_AT
 * @property string|null $JENIS_USER
 * @property int|null $IS_SHOW
 * @property string|null $GELAR_DEPAN
 * @property string|null $GELAR_BELAKANG
 * @property string|null $TEMPAT_LAHIR
 * @property string|null $TANGGAL_LAHIR
 * @property string|null $JENIS_KELAMIN
 * @property string|null $GOL_DARAH
 * @property string|null $AGAMA
 * @property string|null $ALAMAT_KTP
 * @property string|null $NO_HP
 * @property string|null $KORWIL
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_REMOVED = 0;
    const STATUS_NOT_ACTIVE = 5;
    const STATUS_ACTIVE = 10;

    public $password;
    public $passwordOld;
    public $passwordRepeat;
    public $role;

    /**
     * @inheritdoc
     */
    // public function behaviors()
    // {
    //     return ['log' => ['class' => \common\classes\LogBehavior::className()]];
    // }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'APP_USER';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['USERNAME', 'EMAIL', 'FULL_NAME', 'DISPLAY_NAME', 'PASSWORD_HASH', 'PASSWORD_RESET_TOKEN',
                'GELAR_DEPAN', 'GELAR_BELAKANG', 'TEMPAT_LAHIR', 'TEMPAT_KERJA', 'JABATAN_KERJA', 'PERGURUAN_TINGGI',
                'NO_IJAZAH', 'NO_SERKOM', 'NO_SRTA', 'NO_SIPA'],
                'string',
                'max' => 255,
            ],
            [['NO_KTA'], 'string', 'max' => 18],
            [['JENIS_USER', 'JENIS_KELAMIN', 'ALAMAT_KTP', 'ALAMAT_DOMISILI'], 'string'],
            [['USERNAME', 'EMAIL'], 'required'],
            [['USERNAME', 'EMAIL'], 'unique'],
            [['USERNAME', 'EMAIL'], 'filter', 'filter' => 'trim'],
            [['USERNAME'], 'string', 'min' => 3, 'max' => 255],
            [['EMAIL'], 'email'],
            [['STATUS', 'ID_UNIT', 'ID_SUB_UNIT', 'IS_SHOW'], 'integer'],
            [['STATUS'], 'default', 'value' => self::STATUS_ACTIVE],
            [['STATUS'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_NOT_ACTIVE, self::STATUS_REMOVED]],
            [['AUTH_KEY'], 'string', 'max' => 32],
            [['GOL_DARAH'], 'string', 'max' => 2],
            [['AGAMA'], 'string', 'max' => 10],
            [['NO_HP'], 'string', 'max' => 25],
            [['KORWIL'], 'string', 'max' => 50],
            [['CREATED_AT', 'UPDATED_AT', 'LOGIN_AT', 'role', 'TANGGAL_LAHIR','TGL_AKHIR_SERKOM',
                'TGL_AKHIR_SRTA', 'TGL_AKHIR_SIPA'], 'safe'],
            [['USERNAME'], 'unique'],
            [['EMAIL'], 'unique'],
            [['PASSWORD_RESET_TOKEN'], 'unique'],
            [['password', 'passwordOld', 'passwordRepeat'], 'required', 'on' => 'resetPassword'],
            [['password'], 'required', 'on' => 'register'],
            [['password'], 'string', 'min' => 6],
            [['passwordOld'], 'passwordValidation'],
            [['passwordRepeat'], 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'USERNAME' => 'Username',
            'EMAIL' => 'Email',
            'NO_KTA' => 'No KTA',
            'ID_UNIT' => 'Fakultas',
            'ID_SUB_UNIT' => 'Prodi',
            'FULL_NAME' => 'Nama Lengkap',
            'DISPLAY_NAME' => 'Display Name',
            'PASSWORD_HASH' => 'Password Hash',
            'PASSWORD_RESET_TOKEN' => 'Password Reset Token',
            'AUTH_KEY' => 'Auth Key',
            'STATUS' => 'Status',
            'CREATED_AT' => 'Created At',
            'UPDATED_AT' => 'Updated At',
            'LOGIN_AT' => 'Login At',
            'ITEM_NAME' => 'Role',
            'JENIS_USER' => 'Jenis User',
            'IS_SHOW' => 'Is Show',
            'GELAR_DEPAN' => 'Gelar Depan',
            'GELAR_BELAKANG' => 'Gelar Belakang',
            'TEMPAT_LAHIR' => 'Tempat Lahir',
            'TANGGAL_LAHIR' => 'Tanggal Lahir',
            'JENIS_KELAMIN' => 'Jenis Kelamin',
            'GOL_DARAH' => 'Gol Darah',
            'AGAMA' => 'Agama',
            'ALAMAT_KTP' => 'Alamat KTP',
            'ALAMAT_DOMISILI' => 'Alamat Domisili',
            'NO_HP' => 'No HP',
            'KORWIL' => 'Korwil',
            'TEMPAT_KERJA' => 'Tempat Kerja',
            'JABATAN_KERJA' => 'Jabatan',
            'NO_IJAZAH' => 'Nomor Ijazah',
            'NO_SERKOM' => 'Nomor Sertifikasi',
            'TGL_AKHIR_SERKOM' => 'Tahun Kadaluarsa Sertifikasi',
            'NO_SRTA' => 'Nomor SRTA',
            'TGL_AKHIR_SRTA' => 'Tahun Kadaluarsa SRTA',
            'NO_SIPA' => 'Nomor SIPA',
            'TGL_AKHIR_SIPA' => 'Tahun Kadaluarsa SIPA',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['ID' => $id, 'STATUS' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['USERNAME' => $username, 'STATUS' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'PASSWORD_RESET_TOKEN' => $token,
            'STATUS' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->AUTH_KEY;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->PASSWORD_HASH);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->PASSWORD_HASH = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->AUTH_KEY = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->PASSWORD_RESET_TOKEN = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->PASSWORD_RESET_TOKEN = null;
    }

    /**
     * Validate password when resetting
     */
    public function passwordValidation()
    {
        $user = static::findOne(Yii::$app->user->id);

        if (!$user || !$user->validatePassword($this->passwordOld)) {
            $this->addError('passwordOld', 'Password lama tidak sesuai, silakan ulangi lagi.');
        }
    }

    /**
     * Get array of user status.
     *
     * @return array
     */
    public function getStatuses()
    {
        return [
            self::STATUS_ACTIVE => "Active",
            self::STATUS_NOT_ACTIVE => "Not Active",
            self::STATUS_REMOVED => "Removed",
        ];
    }

    /**
     * Get text from user status.
     *
     * @return string
     */
    public function getStatusText()
    {
        $status = $this->getStatuses();

        return isset($status[$this->STATUS]) ? $status[$this->STATUS] : null;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->CREATED_AT = new Expression('NOW()');
                $this->USERNAME = $this->EMAIL;
                if (!$this->STATUS) {
                    $this->STATUS = self::STATUS_NOT_ACTIVE;
                }
                if (!$this->DISPLAY_NAME) {
                    $this->DISPLAY_NAME = $this->FULL_NAME;
                }
            }
            $this->UPDATED_AT = new Expression('NOW()');

            return true;
        }

        return false;
    }
    
    public function getAppAuthAssignment()
    {
        return $this->hasMany(AppAuthAssignment::className(), ['USER_ID' => 'ID']);
    }
    
    public function getAuthItem()
    {
        return $this->hasMany(AppAuthAssignment::className(), ['USER_ID' => 'ID']);
    }

    public function getIdKorwil()
    {
        return $this->hasOne(RefKorwil::className(), ['ID' => 'KORWIL']);
    }
    
    public function getTransaksi()
    {
        return $this->hasMany(IaiTransaksi::className(), ['ID_USER' => 'ID']);
    }
}
