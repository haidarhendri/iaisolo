<?php

namespace common\models;

use simpak\models\RefIuran;
use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_RIWAYAT_IURAN".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property string|null $NOMINAL
 * @property string|null $TANGGAL_AWAL
 * @property string|null $TANGGAL_AKHIR
 * @property string|null $STATUS_BAYAR
 * @property string|null $TANGGAL_BAYAR
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiRiwayatIuran extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'IURAN';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_RIWAYAT_IURAN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'ID_JENIS_IURAN', 'STATUS_BAYAR', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['TANGGAL_AWAL', 'TANGGAL_AKHIR', 'TANGGAL_BAYAR', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['NOMINAL'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [['NOMINAL'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'Id TRANSAKSI',
            'ID_JENIS_IURAN' => 'Jenis Iuran',
            'NOMINAL' => 'Nominal',
            'TANGGAL_AWAL' => 'Tanggal Awal',
            'TANGGAL_AKHIR' => 'Tanggal Akhir',
            'STATUS_BAYAR' => 'Status Bayar',
            'TANGGAL_BAYAR' => 'Tanggal Bayar',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }

    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }

    public function getJenisIuran()
    {
        return $this->hasOne(RefIuran::className(), ['ID' => 'ID_JENIS_IURAN']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
