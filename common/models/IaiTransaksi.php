<?php

namespace common\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_TRANSAKSI".
 *
 * @property int $ID
 * @property int|null $ID_USER
 * @property string|null $URAIAN_KEGIATAN
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiTransaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_TRANSAKSI';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_USER', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['URAIAN_KEGIATAN'], 'string'],
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_USER' => 'Id User',
            'URAIAN_KEGIATAN' => 'Uraian Kegiatan',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
