<?php

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $USERNAME;
    public $EMAIL;
    public $PASSWORD;
    public $DISPLAY_NAME;
    public $STATUS;
    public $JENIS_USER;
    public $IS_SHOW;
    public $KORWIL;
    public $NO_KTA;
    public $GELAR_DEPAN;
    public $FULL_NAME;
    public $GELAR_BELAKANG;
    public $TEMPAT_LAHIR;
    public $TANGGAL_LAHIR;
    public $JENIS_KELAMIN;
    public $GOL_DARAH;
    public $AGAMA;
    public $ALAMAT_KTP;
    public $ALAMAT_DOMISILI;
    public $NO_HP;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['USERNAME', 'trim'],
            // ['USERNAME', 'required'],
            [
                'USERNAME',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => 'This username has already been taken.',
            ],
            ['USERNAME', 'string', 'min' => 2, 'max' => 255],

            ['EMAIL', 'trim'],
            ['EMAIL', 'required'],
            ['EMAIL', 'email'],
            [['EMAIL', 'FULL_NAME', 'GELAR_DEPAN', 'GELAR_BELAKANG', 'TEMPAT_LAHIR'], 'string', 'max' => 255],
            [
                'EMAIL',
                'unique',
                'targetClass' => '\common\models\User',
                'message' => Yii::t('app', 'This email address has already been taken.'),
            ],

            ['PASSWORD', 'required'],
            ['PASSWORD', 'string', 'min' => 6],
            [['NO_KTA'], 'string', 'max' => 18],
            [['JENIS_USER', 'JENIS_KELAMIN', 'ALAMAT_KTP', 'ALAMAT_DOMISILI'], 'string'],
            [['GOL_DARAH'], 'string', 'max' => 2],
            [['AGAMA'], 'string', 'max' => 10],
            [['NO_HP'], 'string', 'max' => 25],
            [['KORWIL'], 'string', 'max' => 50],
            ['TANGGAL_LAHIR', 'safe'],
            [
                [
                    'FULL_NAME', 'KORWIL', 'NO_HP', 'AGAMA', 'GOL_DARAH', 'JENIS_KELAMIN',
                    'ALAMAT_KTP', 'ALAMAT_DOMISILI','NO_KTA', 'GELAR_DEPAN', 'GELAR_BELAKANG',
                    'TEMPAT_LAHIR', 'TANGGAL_LAHIR'
                ],
                'required'
            ],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->USERNAME = $this->EMAIL;
        $user->EMAIL = $this->EMAIL;
        $user->setPassword($this->PASSWORD);
        $user->generateAuthKey();
        $user->DISPLAY_NAME = $this->GELAR_DEPAN . " " . $this->FULL_NAME . " " . $this->GELAR_BELAKANG;
        $user->STATUS = 10;
        $user->JENIS_USER = ($this->KORWIL != 5 ? '0' : '1');
        $user->IS_SHOW = 1;
        $user->KORWIL = $this->KORWIL;
        $user->NO_KTA = $this->NO_KTA;
        $user->NO_HP = $this->NO_HP;
        $user->GELAR_DEPAN = $this->GELAR_DEPAN;
        $user->FULL_NAME = $this->FULL_NAME;
        $user->GELAR_BELAKANG = $this->GELAR_BELAKANG;
        $user->TEMPAT_LAHIR = $this->TEMPAT_LAHIR;
        $user->TANGGAL_LAHIR = $this->TANGGAL_LAHIR;
        $user->JENIS_KELAMIN = $this->JENIS_KELAMIN;
        $user->GOL_DARAH = $this->GOL_DARAH;
        $user->AGAMA = $this->AGAMA;
        $user->ALAMAT_KTP = $this->ALAMAT_KTP;
        $user->ALAMAT_DOMISILI = $this->ALAMAT_DOMISILI;

        if ($user->save()) {
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole('role_anggota'), $user->ID);
            return $user;
        } else {
            return null;
        }
    }
}
