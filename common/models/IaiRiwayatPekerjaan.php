<?php

namespace common\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_RIWAYAT_PEKERJAAN".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property string|null $TEMPAT_KERJA
 * @property string|null $JABATAN
 * @property string|null $TANGGAL_AWAL
 * @property string|null $TANGGAL_AKHIR
 * @property int|null $IS_UTAMA
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiRiwayatPekerjaan extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'PEKERJAAN';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_RIWAYAT_PEKERJAAN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'IS_UTAMA', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['TANGGAL_AWAL', 'TANGGAL_AKHIR', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['TEMPAT_KERJA', 'JABATAN'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [
                [
                    'TANGGAL_AWAL',
                    'TANGGAL_AKHIR',
                    'TEMPAT_KERJA',
                    'JABATAN',
                ],
                'required'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'Id Transaksi',
            'TEMPAT_KERJA' => 'Tempat Kerja',
            'JABATAN' => 'Jabatan',
            'TANGGAL_AWAL' => 'Tanggal Awal',
            'TANGGAL_AKHIR' => 'Tanggal Akhir',
            'IS_UTAMA' => 'Status Pekerjaan',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
