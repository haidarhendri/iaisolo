<?php

namespace common\models\searches;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\iaiTransaksiDokumen;

/**
 * IaiTransaksiDokumenSearch represents the model behind the search form about `common\models\iaiTransaksiDokumen`.
 */
class IaiTransaksiDokumenSearch extends iaiTransaksiDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ID_USER', 'ID_JENIS_DOKUMEN', 'FILE_SIZE', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['KETERANGAN', 'FILE_NAME', 'FILE_URL', 'FILE_PATH', 'FILE_TYPE', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_USER' => $this->ID_USER,
            'ID_JENIS_DOKUMEN' => $this->ID_JENIS_DOKUMEN,
            'FILE_SIZE' => $this->FILE_SIZE,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'KETERANGAN', $this->KETERANGAN])
            ->andFilterWhere(['like', 'FILE_NAME', $this->FILE_NAME])
            ->andFilterWhere(['like', 'FILE_URL', $this->FILE_URL])
            ->andFilterWhere(['like', 'FILE_PATH', $this->FILE_PATH])
            ->andFilterWhere(['like', 'FILE_TYPE', $this->FILE_TYPE])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_USER' => $this->ID_USER,
            'ID_JENIS_DOKUMEN' => $this->ID_JENIS_DOKUMEN,
            'FILE_SIZE' => $this->FILE_SIZE,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'KETERANGAN', $this->KETERANGAN])
            ->andFilterWhere(['like', 'FILE_NAME', $this->FILE_NAME])
            ->andFilterWhere(['like', 'FILE_URL', $this->FILE_URL])
            ->andFilterWhere(['like', 'FILE_PATH', $this->FILE_PATH])
            ->andFilterWhere(['like', 'FILE_TYPE', $this->FILE_TYPE])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return iaiTransaksiDokumen::find();
    }
}
