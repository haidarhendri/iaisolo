<?php

namespace common\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\web\Request;

/**
 * This is the model class for table "IAI_RIWAYAT_SERTIF".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property int|null $ID_USER
 * @property int|null $JENIS_SERTIF
 * @property string|null $NOMOR_SERTIF
 * @property string|null $TGL_AWAL
 * @property string|null $TGL_AKHIR
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiRiwayatSertif extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'SERTIF';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_RIWAYAT_SERTIF';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'ID_USER', 'JENIS_SERTIF', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['TGL_AWAL', 'TGL_AKHIR', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['NOMOR_SERTIF'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [
                [
                    'TGL_AWAL',
                    'TGL_AKHIR',
                    'NOMOR_SERTIF',
                    'JENIS_SERTIF',
                ],
                'required'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'Id Transaksi',
            'ID_USER' => 'Id User',
            'JENIS_SERTIF' => 'Jenis Sertif',
            'NOMOR_SERTIF' => 'Nomor Sertif',
            'TGL_AWAL' => 'Tgl Awal',
            'TGL_AKHIR' => 'Tgl Akhir',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }

    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }
    
    public function getIdJenisSertif()
    {
        return $this->hasOne(RefJenisSertif::className(), ['ID' => 'JENIS_SERTIF']);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'ID_USER'=> $userId,
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
}
