<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "IAI_SETTING".
 *
 * @property string $SETTING_KEY
 * @property string $SETTING_VALUE
 */
class IaiSetting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_SETTING';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['SETTING_KEY', 'SETTING_VALUE'], 'required'],
            [['SETTING_KEY', 'SETTING_VALUE'], 'string', 'max' => 255],
            [['SETTING_KEY'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'SETTING_KEY' => 'Setting Key',
            'SETTING_VALUE' => 'Setting Value',
        ];
    }
}
