<?php

namespace common\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Request;

/**
 * This is the model class for table "IAI_AGENDA".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property string|null $AGENDA_NAMA
 * @property string|null $AGENDA_JENIS
 * @property string|null $AGENDA_DESKRIPSI
 * @property string|null $AGENDA_TEMPAT
 * @property string|null $AGENDA_TGL_AWAL
 * @property string|null $AGENDA_TGL_AKHIR
 * @property string|null $AGENDA_BIAYA
 * @property string|null $AGENDA_KUOTA
 * @property string|null $AGENDA_POIN
 * @property string|null $AGENDA_FOTO
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiAgenda extends \yii\db\ActiveRecord
{
    const URAIAN_KEGIATAN = 'AGENDA';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_AGENDA';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AGENDA_DESKRIPSI'], 'string'],
            [['AGENDA_TGL_AWAL', 'AGENDA_TGL_AKHIR', 'AGENDA_TGL_PELAKSANAAN','CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['ID_TRANSAKSI', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['AGENDA_NAMA', 'AGENDA_JENIS', 'AGENDA_TEMPAT', 'AGENDA_BIAYA', 'AGENDA_BIAYA_TERBILANG', 'AGENDA_KUOTA', 'AGENDA_POIN', 'AGENDA_FOTO'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [
                [
                    'AGENDA_NAMA',
                    'AGENDA_JENIS',
                    'AGENDA_TEMPAT',
                    'AGENDA_BIAYA',
                    'AGENDA_BIAYA_TERBILANG',
                    'AGENDA_KUOTA',
                    'AGENDA_POIN',
                    'AGENDA_TGL_AWAL',
                    'AGENDA_TGL_AKHIR',
                    'AGENDA_TGL_PELAKSANAAN',
                ],
                'required'
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'ID TRANSAKSI',
            'AGENDA_NAMA' => 'Nama Agenda',
            'AGENDA_JENIS' => 'Jenis Agenda',
            'AGENDA_DESKRIPSI' => 'Deskripsi',
            'AGENDA_TEMPAT' => 'Tempat Pelaksanaan',
            'AGENDA_TGL_AWAL' => 'Tanggal Awal',
            'AGENDA_TGL_AKHIR' => 'Tanggal Akhir',
            'AGENDA_TGL_PELAKSANAAN' => 'Tanggal Pelaksanaan',
            'AGENDA_BIAYA' => 'Biaya',
            'AGENDA_BIAYA_TERBILANG' => 'Biaya Terbilang',
            'AGENDA_KUOTA' => 'Kuota',
            'AGENDA_POIN' => 'Kredit Poin',
            'AGENDA_FOTO' => 'Foto Sampul',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(IaiTransaksi::className(), ['ID' => 'ID_TRANSAKSI']);
    }
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER'])->viaTable('IAI_TRANSAKSI', ['ID' => 'ID_TRANSAKSI']);
    }
    public function getIdDokumen()
    {
        return $this->hasOne(IaiTransaksiDokumen::className(), ['ID_TRANSAKSI' => 'ID_TRANSAKSI']);
    }
    
    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
    
    public static function map($key = 'ID', $value = 'AGENDA_NAMA', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, 'VALUE' => $value]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
