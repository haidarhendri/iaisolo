<?php

namespace common\models;

use common\models\PakPegawai as Pegawai;
use yii\helpers\Json;

/**
 * This is the model class for table "LOG_DATA".
 *
 * @property integer $ID
 * @property integer $ID_USER
 * @property integer $NO_KTA
 * @property integer $ID_JENIS_AKSI
 * @property string $IP_USER
 * @property string $TANGGAL
 * @property string $NAMA_APP
 * @property string $CONTROLLER
 * @property string $CONTROLLER_ID
 * @property string $CONTROLLER_ACTION_ID
 * @property string $TABEL
 * @property string $MODEL
 * @property string $MODEL_ID
 * @property string $DATA_LAMA
 * @property string $DATA_BARU
 * @property integer $IS_SINKRON
 * @property integer $IS_SINKRON_STATISTIK
 * @property integer $IS_ROLLBACK
 *
 * @property array $dataBeda
 * @property array $dataLama
 * @property array $dataBaru
 *
 * @property User $idUser
 * @property Pegawai $idPegawai
 * @property referensi\RefLogJenisAksi $idJenisAksi
 */
class LogData extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'LOG_DATA';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'ID_USER',
                    'NO_KTA',
                    'ID_JENIS_AKSI',
                    'IS_SINKRON',
                    'IS_SINKRON_STATISTIK',
                    'IS_ROLLBACK',
                ],
                'integer',
            ],
            [['TANGGAL'], 'safe'],
            [['DATA_LAMA', 'DATA_BARU'], 'string'],
            [['IP_USER'], 'string', 'max' => 100],
            [
                [
                    'NAMA_APP',
                    'CONTROLLER',
                    'CONTROLLER_ID',
                    'CONTROLLER_ACTION_ID',
                    'TABEL',
                    'MODEL',
                    'MODEL_ID',
                ],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_USER' => 'ID User',
            'NO_KTA' => 'No KTA',
            'ID_JENIS_AKSI' => 'ID Jenis Aksi',
            'IP_USER' => 'IP User',
            'TANGGAL' => 'Tanggal',
            'NAMA_APP' => 'Nama App',
            'CONTROLLER' => 'Controller',
            'CONTROLLER_ID' => 'Controller ID',
            'CONTROLLER_ACTION_ID' => 'Controller Action ID',
            'TABEL' => 'Tabel',
            'MODEL' => 'Model',
            'MODEL_ID' => 'Model ID',
            'DATA_LAMA' => 'Data Lama',
            'DATA_BARU' => 'Data Baru',
            'IS_SINKRON' => 'Is Sinkron',
            'IS_SINKRON_STATISTIK' => 'Is Sinkron Statistik',
            'IS_ROLLBACK' => 'Is Rollback',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUser()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_USER']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getIdPegawai()
    // {
    //     return $this->hasOne(Pegawai::className(), ['ID' => 'NO_KTA']);
    // }

    /**
     * @return \yii\db\ActiveQuery
     */
    // public function getIdJenisAksi()
    // {
    //     return $this->hasOne(referensi\RefLogJenisAksi::className(), ['ID' => 'ID_JENIS_AKSI']);
    // }

    /**
     * @return array
     */
    public function getDataBeda()
    {
        $dataLama = $this->getDataLama();
        $dataBaru = $this->getDataBaru();

        return (array_diff($dataBaru, $dataLama));
    }

    /**
     * @return mixed
     */
    public function getDataLama()
    {
        return Json::decode($this->DATA_LAMA);
    }

    /**
     * @return mixed
     */
    public function getDataBaru()
    {
        return Json::decode($this->DATA_BARU);
    }
}
