<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "REF_JENIS_SERTIF".
 *
 * @property int $ID
 * @property string|null $NAMA_SERTIF
 */
class RefJenisSertif extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_JENIS_SERTIF';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAMA_SERTIF'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NAMA_SERTIF' => 'Nama Sertif',
        ];
    }
}
