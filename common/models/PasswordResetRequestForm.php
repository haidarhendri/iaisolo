<?php
namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $EMAIL;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['EMAIL', 'trim'],
            ['EMAIL', 'required'],
            ['EMAIL', 'email'],
            ['EMAIL', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => Yii::t('app', 'There is no user with this email address.')
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'STATUS' => User::STATUS_ACTIVE,
            'EMAIL' => $this->EMAIL,
        ]);

        if (!$user) {
            return false;
        }
        
        if (!User::isPasswordResetTokenValid($user->PASSWORD_RESET_TOKEN)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
            ->setTo($this->EMAIL)
            ->setSubject('Setel ulang kata sandi')
            ->send();
    }
}
