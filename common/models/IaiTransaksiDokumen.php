<?php

namespace common\models;

use common\models\referensi\CfgJenisDokumen;
use common\models\referensi\RefJenisDokumen;
use Yii;
use yii\base\Application;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\Request;
use yii\web\UploadedFile;

/**
 * This is the model class for table "IAI_TRANSAKSI_DOKUMEN".
 *
 * @property int $ID
 * @property int|null $ID_TRANSAKSI
 * @property int|null $ID_JENIS_DOKUMEN
 * @property string|null $KETERANGAN
 * @property string|null $FILE_NAME
 * @property string|null $FILE_URL
 * @property string|null $FILE_PATH
 * @property int|null $FILE_SIZE
 * @property string|null $FILE_TYPE
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class IaiTransaksiDokumen extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'IAI_TRANSAKSI_DOKUMEN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_TRANSAKSI', 'ID_JENIS_DOKUMEN', 'FILE_SIZE', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['KETERANGAN', 'FILE_NAME', 'FILE_URL', 'FILE_PATH', 'FILE_TYPE'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
            [
                'file',
                'file',
                'extensions' => ['png', 'jpg', 'jpeg', 'gif', 'doc', 'docx', 'pdf'],
                'maxSize' => 5 * 1024 * 1024,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_TRANSAKSI' => 'Id User',
            'ID_JENIS_DOKUMEN' => 'Id Jenis Dokumen',
            'KETERANGAN' => 'Keterangan',
            'FILE_NAME' => 'File Name',
            'FILE_URL' => 'File Url',
            'FILE_PATH' => 'File Path',
            'FILE_SIZE' => 'File Size',
            'FILE_TYPE' => 'File Type',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    // 'ID_USER' => $userId,
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                // 'ID_USER' => $userId,
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }

    public function getIdTransaksi()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_TRANSAKSI']);
    }

    public function getIdJenisDokumen()
    {
        return $this->hasOne(User::className(), ['ID' => 'ID_JENIS_DOKUMEN']);
    }

    public function getCfgDokumen($idJenisUpload)
    {
        $query = CfgJenisDokumen::find()
            ->where([
                'ID_JENIS_UPLOAD' => $idJenisUpload,
                'IS_SHOW' => 1
            ])
            ->indexBy('ID');

        return $query->asArray()->all();
    }

    public function getUploadDokumen($idJenisUpload)
    {
        $query = RefJenisDokumen::find()
            ->select(['A.ID', 'A.JENIS_DOKUMEN', 'B.IS_REQUIRED'])
            ->from(['A' => RefJenisDokumen::tableName()])
            ->innerJoinWith([
                'dataConfig' => function (ActiveQuery $q) use ($idJenisUpload) {
                    return $q->from(['B' => CfgJenisDokumen::tableName()])
                        ->andOnCondition(['B.ID_JENIS_UPLOAD' => $idJenisUpload]);
                },
            ], false)
            ->where(['B.IS_SHOW' => 1])
            ->indexBy('ID');

        return $query->asArray()->all();
    }

    /**
     * @return array
     */
    public function getRecordDokumen($idTransaksi, $idJenisUpload)
    {
        $upload = $this->getUploadDokumen($idJenisUpload);
        $query = IaiTransaksiDokumen::find()
            ->select(['A.ID', 'A.ID_JENIS_DOKUMEN', 'B.JENIS_DOKUMEN', 'A.FILE_URL', 'A.FILE_PATH'])
            ->from(['A' => IaiTransaksiDokumen::tableName()])
            ->joinWith([
                'idJenisDokumen' => function (ActiveQuery $q) {
                    return $q->from(['B' => RefJenisDokumen::tableName()]);
                },
            ], false)
            ->andWhere([
                'AND',
                ['IN', 'A.ID_JENIS_DOKUMEN', array_keys($upload)],
                ['A.ID_TRANSAKSI' => $idTransaksi],
            ])
            ->orderBy(['A.CREATE_DATE' => SORT_DESC])
            ->indexBy('ID_JENIS_DOKUMEN');
        $models = $query->asArray()->all();
        return $models;
    }

    public function uploadDokumen(array $dokumen)
    {
        /* @var $form PakTransaksiDokumen[] */
        /* @var $upload array */
        $success = true;
        $uploads = $dokumen['uploads'];
        $records = $dokumen['records'];
        $form = $dokumen['form'];
        $dokumen['deleted'] = [];
        $model = $dokumen['model'] ? $dokumen['model'] : null;
        $idTransaksi = $dokumen['idTransaksi'] ? $dokumen['idTransaksi'] : null;
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userName = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->identity->FULL_NAME : null;
        $uploadPath = Yii::getAlias('@uploadPath/dokumen' . date('/Y/m'));

        if (!is_dir($uploadPath)) {
            FileHelper::createDirectory($uploadPath);
        }

        foreach ($uploads as $i => $f) {
            $file = $form[$i];

            if ($exist = ArrayHelper::getValue($records, $i)) {
                $file = IaiTransaksiDokumen::findOne($exist['ID']);
            }

            if ($file->file = UploadedFile::getInstance($file, "[{$i}]file")) {
                $file->FILE_NAME = date('Y/m/') . Yii::$app->user->id . '-' . Yii::$app->security->generateRandomString() . ".{$file->file->extension}";
                $file->setAttributes([
                    'ID_TRANSAKSI' => $idTransaksi,
                    'ID_JENIS_DOKUMEN' => $i,
                    'KETERANGAN' => ArrayHelper::getValue($f, 'JENIS_DOKUMEN') . ' a.n. ' . $userName,
                    'FILE_PATH' => Yii::getAlias("@uploadPath/dokumen/{$file->FILE_NAME}"),
                    'FILE_URL' => Yii::getAlias("@uploadUrl/dokumen/{$file->FILE_NAME}"),
                    'FILE_TYPE' => $file->file->type,
                    'FILE_SIZE' => $file->file->size,
                ]);

                foreach ($dokumen['cfgdokumen'] as $cfg) {
                    if ($i == $cfg['ID_JENIS_DOKUMEN']) {
                        $file->ID_CFG_JENIS_DOKUMEN = $cfg['ID'];
                    }
                }
                $file->file->saveAs($file->FILE_PATH);
                $file->file = 'Hello';

                if ($file->save()) {
                    if ($exist && is_file($exist['FILE_PATH'])) {
                        $dokumen['deleted'][] = $exist['FILE_PATH'];
                    }
                } else {
                    $success = false;
                }
            } elseif (ArrayHelper::getValue($f, 'IS_REQUIRED') && !ArrayHelper::getValue($dokumen['records'], $i)) {
                $file->addError('file', 'File harus diupload');
                $success = false;
            }
        }

        $dokumen['form'] = $form;
        $dokumen['success'] = $success;

        return $dokumen;
    }
}
