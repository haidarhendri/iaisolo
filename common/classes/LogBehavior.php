<?php
/**
 * @author Haidar H. Setyawan <13nightevil@gmail.com>
 * @copyright Copyright (c) 2016 NEXPLAN
 */

namespace common\classes;

use common\models\LogData;
use common\models\User;
use Yii;
use yii\base\Application;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\Json;

/**
 * Class LogBehavior
 * @package common\behaviors
 *
 * @property \yii\db\ActiveRecord $owner
 */
class LogBehavior extends \yii\base\Behavior
{
    const INSERT = 1;

    const UPDATE = 2;

    const DELETE = 3;

    /**
     * Array with fields to save
     * You don't need to configure both `allowed` and `ignored`
     * @var array
     */
    public $allowed = [];

    /**
     * Array with fields to ignore
     * You don't need to configure both `allowed` and `ignored`
     * @var array
     */
    public $ignored = [
        'CREATE_DATE',
        'CREATE_BY',
        'CREATE_IP',
        'UPDATE_DATE',
        'UPDATE_BY',
        'UPDATE_IP',
        'IS_SINKRON',
        'ID_APPROVE',
    ];

    /**
     * Array with app_id to ignore
     * example:
     * [
     *      'class' => LogBehavior::className(),
     *      'ignoredApp' => ['app-console']
     * ]
     * @var array
     */
    public $ignoredApps = ['app-console', 'app-frontend'];

    /**
     * Array with classes to ignore
     * @var array
     */
    public $ignoredClasses = [];

    /**
     * Is the behavior is active or not
     * @var boolean
     */
    public $active = true;

    /**
     * Date format to use in stamp - set to "Y-m-d H:i:s" for datetime or "U" for timestamp
     * @var string
     */
    public $dateFormat = 'Y-m-d H:i:s';

    /**
     * @var array
     */
    private $_oldAttributes = [];

    /**
     * {@inheritdoc}
     */
    public function events()
    {
        if ($this->isIgnore()) {
            return [];
        }

        if (!$this->active) {
            return [];
        }

        if (sizeof($this->ignoredClasses) > 0 && array_search(get_class($this->owner), $this->ignoredClasses) !== false) {
            return [];
        }

        return [
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }

    /**
     * Set old attributes ActiveRecord::EVENT_AFTER_FIND
     */
    public function afterFind()
    {
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * Save log on create new record
     */
    public function afterInsert()
    {
        $this->saveLog(self::INSERT);
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * Save log on update record
     */
    public function afterUpdate()
    {
        $this->saveLog(self::UPDATE);
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * Save log on delete record
     */
    public function afterDelete()
    {
        $this->saveLog(self::DELETE);
        $this->setOldAttributes($this->owner->getAttributes());
    }

    /**
     * Update attributes before save
     */
    public function beforeInsert()
    {
        $this->owner->setAttributes([
            'IS_SINKRON' => 0,
            'CREATE_DATE' => new Expression('LOCALTIMESTAMP(0)'),
            'CREATE_BY' => $this->getUserId(),
            'CREATE_IP' => Yii::$app->request->userIP,
        ]);
    }

    /**
     * Update attributes before save
     */
    public function beforeUpdate()
    {
        $this->owner->setAttributes([
            'IS_SINKRON' => 0,
            'UPDATE_DATE' => new Expression('LOCALTIMESTAMP(0)'),
            'UPDATE_BY' => $this->getUserId(),
            'UPDATE_IP' => Yii::$app->request->userIP,
        ]);
    }

    /**
     * Clean attributes of fields that are not allowed or ignored.
     *
     * @param $attributes
     * @return mixed
     */
    protected function cleanAttributes($attributes)
    {
        $attributes = $this->cleanAttributesAllowed($attributes);
        $attributes = $this->cleanAttributesIgnored($attributes);

        return $attributes;
    }

    /**
     * Unset attributes which are not allowed
     *
     * @param $attributes
     * @return mixed
     */
    protected function cleanAttributesAllowed($attributes)
    {
        if (sizeof($this->allowed) > 0) {
            foreach ($attributes as $index => $value) {
                if (array_search($index, $this->allowed) === false) {
                    unset($attributes[$index]);
                }
            }
        }

        return $attributes;
    }

    /**
     * Unset attributes which are ignored
     *
     * @param $attributes
     * @return mixed
     */
    protected function cleanAttributesIgnored($attributes)
    {
        if (sizeof($this->ignored) > 0) {
            foreach ($attributes as $index => $value) {
                if (array_search($index, $this->ignored) !== false) {
                    unset($attributes[$index]);
                }
            }
        }
        return $attributes;
    }

    /**
     * Save the audit trails for a create or update action
     * @param $action
     * @return bool
     */
    protected function saveLog($action)
    {
        $oldAttributes = $this->cleanAttributes($this->getOldAttributes());
        $newAttributes = $this->cleanAttributes($this->owner->getAttributes());

        if ($action !== self::DELETE && count(array_diff($newAttributes, $oldAttributes)) <= 0) {
            return false;
        }

        $logReferensi = new LogData([
            'ID_USER' => $this->getUserId(),
            'NO_KTA' => $this->getIdPegawai(),
            'ID_JENIS_AKSI' => $action,
            'IP_USER' => Yii::$app->request->userIP,
            'TANGGAL' => date($this->dateFormat),
            'NAMA_APP' => $this->getAppName(),
            'CONTROLLER' => Yii::$app->controller->className(),
            'CONTROLLER_ID' => Yii::$app->controller->id,
            'CONTROLLER_ACTION_ID' => Yii::$app->controller->action->id,
            'TABEL' => $this->owner->tableName(),
            'MODEL' => $this->owner->className(),
            'MODEL_ID' => $this->getNormalizedPk(),
            'DATA_LAMA' => Json::encode($oldAttributes),
            'DATA_BARU' => $action === self::DELETE ? Json::encode([]) : Json::encode($newAttributes),
        ]);

        return $logReferensi->save(false);
    }

    /**
     * @return array
     */
    public function getOldAttributes()
    {
        return $this->_oldAttributes;
    }

    /**
     * @param $value
     */
    public function setOldAttributes($value)
    {
        $this->_oldAttributes = $value;
    }

    /**
     * @return string
     */
    protected function getNormalizedPk()
    {
        $pk = $this->owner->getPrimaryKey();
        return is_array($pk) ? json_encode($pk) : $pk;
    }

    /**
     * @return int|null
     */
    protected function getUserId()
    {
        return (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
    }

    /**
     * @return int|null
     */
    protected function getIdPegawai()
    {
        /* @var $user User */

        if (Yii::$app instanceof Application && ($user = Yii::$app->user->identity)) {
            return $user->NO_KTA;
        }

        return null;
    }

    /**
     * @return int|null|string
     */
    protected function getAppName()
    {
        return (Yii::$app instanceof Application && Yii::$app->id) ? Yii::$app->id : null;
    }

    /**
     * @return array
     */
    protected function isIgnore()
    {
        return in_array($this->getAppName(), $this->ignoredApps);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreateBy()
    {
        return $this->owner->hasOne(User::class, ['ID' => 'CREATE_BY']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdateBy()
    {
        return $this->owner->hasOne(User::class, ['ID' => 'UPDATE_BY']);
    }
}
