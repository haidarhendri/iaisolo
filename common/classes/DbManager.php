<?php
/**
 * @link http://simpeg.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 NEXPLAN
 */

namespace common\classes;

use Yii;
use yii\caching\Cache;
use yii\db\Query;
use yii\db\Expression;
use yii\base\InvalidCallException;
use yii\base\InvalidParamException;
use yii\rbac\Assignment;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;

/**
 * @inheritdoc
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Alexander Kochetov <creocoder@gmail.com>
 * @since 2.0
 */
class DbManager extends \yii\rbac\DbManager
{
    /**
     * @inheritdoc
     */
    protected function checkAccessRecursive($user, $itemName, $params, $assignments)
    {
        if (($item = $this->getItem($itemName)) === null) {
            return false;
        }

        Yii::trace($item instanceof Role ? "Checking role: $itemName" : "Checking permission: $itemName", __METHOD__);

        if (!$this->executeRule($user, $item, $params)) {
            return false;
        }

        if (isset($assignments[$itemName]) || in_array($itemName, $this->defaultRoles)) {
            return true;
        }

        $query = new Query;
        $parents = $query->select(['PARENT'])
            ->from($this->itemChildTable)
            ->where(['CHILD' => $itemName])
            ->column($this->db);
        foreach ($parents as $parent) {
            if ($this->checkAccessRecursive($user, $parent, $params, $assignments)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    protected function getItem($name)
    {
        if (empty($name)) {
            return null;
        }

        if (!empty($this->items[$name])) {
            return $this->items[$name];
        }

        $row = (new Query)->from($this->itemTable)
            ->where(['NAME' => $name])
            ->one($this->db);

        if ($row === false) {
            return null;
        }

        return $this->populateItem($row);
    }

    /**
     * @inheritdoc
     */
    protected function addItem($item)
    {
        $time = time();
        if ($item->createdAt === null) {
            $item->createdAt = $time;
        }
        if ($item->updatedAt === null) {
            $item->updatedAt = $time;
        }
        $this->db->createCommand()
            ->insert($this->itemTable, [
                'NAME' => $item->name,
                'TYPE' => $item->type,
                'DESCRIPTION' => $item->description,
                'RULE_NAME' => $item->ruleName,
                'DATA' => $item->data === null ? null : serialize($item->data),
                'CREATED_AT' => $item->createdAt,
                'UPDATED_AT' => $item->updatedAt,
            ])->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function removeItem($item)
    {
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->delete($this->itemChildTable, ['OR', '[[PARENT]]=:NAME', '[[CHILD]]=:NAME'], [':NAME' => $item->name])
                ->execute();
            $this->db->createCommand()
                ->delete($this->assignmentTable, ['ITEM_NAME' => $item->name])
                ->execute();
        }

        $this->db->createCommand()
            ->delete($this->itemTable, ['NAME' => $item->name])
            ->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function updateItem($name, $item)
    {
        if ($item->name !== $name && !$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemChildTable, ['PARENT' => $item->name], ['PARENT' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->itemChildTable, ['CHILD' => $item->name], ['CHILD' => $name])
                ->execute();
            $this->db->createCommand()
                ->update($this->assignmentTable, ['ITEM_NAME' => $item->name], ['ITEM_NAME' => $name])
                ->execute();
        }

        $item->updatedAt = time();

        $this->db->createCommand()
            ->update($this->itemTable, [
                'NAME' => $item->name,
                'DESCRIPTION' => $item->description,
                'RULE_NAME' => $item->ruleName,
                'DATA' => $item->data === null ? null : serialize($item->data),
                'UPDATED_AT' => $item->updatedAt,
            ], [
                'NAME' => $name,
            ])->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function addRule($rule)
    {
        $time = time();
        if ($rule->createdAt === null) {
            $rule->createdAt = $time;
        }
        if ($rule->updatedAt === null) {
            $rule->updatedAt = $time;
        }
        $this->db->createCommand()
            ->insert($this->ruleTable, [
                'NAME' => $rule->name,
                'DATA' => serialize($rule),
                'CREATED_AT' => $rule->createdAt,
                'UPDATED_AT' => $rule->updatedAt,
            ])->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function updateRule($name, $rule)
    {
        if ($rule->name !== $name && !$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['RULE_NAME' => $rule->name], ['RULE_NAME' => $name])
                ->execute();
        }

        $rule->updatedAt = time();

        $this->db->createCommand()
            ->update($this->ruleTable, [
                'NAME' => $rule->name,
                'DATA' => serialize($rule),
                'UPDATED_AT' => $rule->updatedAt,
            ], [
                'NAME' => $name,
            ])->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function removeRule($rule)
    {
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['RULE_NAME' => null], ['RULE_NAME' => $rule->name])
                ->execute();
        }

        $this->db->createCommand()
            ->delete($this->ruleTable, ['NAME' => $rule->name])
            ->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    protected function getItems($type)
    {
        $query = (new Query)
            ->from($this->itemTable)
            ->where(['TYPE' => $type]);

        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['NAME']] = $this->populateItem($row);
        }

        return $items;
    }

    /**
     * @inheritdoc
     */
    protected function populateItem($row)
    {
        $class = $row['TYPE'] == Item::TYPE_PERMISSION ? Permission::className() : Role::className();

        if (!isset($row['DATA']) || ($data = @unserialize($row['DATA'])) === false) {
            $data = null;
        }

        return new $class([
            'name' => $row['NAME'],
            'type' => $row['TYPE'],
            'description' => $row['DESCRIPTION'],
            'ruleName' => $row['RULE_NAME'],
            'data' => $data,
            'createdAt' => $row['CREATED_AT'],
            'updatedAt' => $row['UPDATED_AT'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getRolesByUser($userId)
    {
        if (!isset($userId) || $userId === '') {
            return [];
        }

        $query = (new Query)->select('B.*')
            ->from(['A' => $this->assignmentTable, 'B' => $this->itemTable])
            ->where('{{A}}.[[ITEM_NAME]]={{B}}.[[NAME]]')
            ->andWhere(['A.USER_ID' => (string) $userId])
            ->andWhere(['B.TYPE' => Item::TYPE_ROLE]);

        $roles = [];
        foreach ($query->all($this->db) as $row) {
            $roles[$row['NAME']] = $this->populateItem($row);
        }
        return $roles;
    }

    /**
     * @inheritdoc
     */
    public function getPermissionsByRole($roleName)
    {
        $childrenList = $this->getChildrenList();
        $result = [];
        $this->getChildrenRecursive($roleName, $childrenList, $result);
        if (empty($result)) {
            return [];
        }
        $query = (new Query)->from($this->itemTable)->where([
            'TYPE' => Item::TYPE_PERMISSION,
            'NAME' => array_keys($result),
        ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['NAME']] = $this->populateItem($row);
        }
        return $permissions;
    }

    /**
     * @inheritdoc
     */
    protected function getDirectPermissionsByUser($userId)
    {
        $query = (new Query)->select('B.*')
            ->from(['A' => $this->assignmentTable, 'B' => $this->itemTable])
            ->where('{{A}}.[[ITEM_NAME]]={{B}}.[[NAME]]')
            ->andWhere(['A.USER_ID' => (string) $userId])
            ->andWhere(['B.TYPE' => Item::TYPE_PERMISSION]);

        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['NAME']] = $this->populateItem($row);
        }
        return $permissions;
    }

    /**
     * @inheritdoc
     */
    protected function getInheritedPermissionsByUser($userId)
    {
        $query = (new Query)->select('ITEM_NAME')
            ->from($this->assignmentTable)
            ->where(['USER_ID' => (string) $userId]);

        $childrenList = $this->getChildrenList();
        $result = [];
        foreach ($query->column($this->db) as $roleName) {
            $this->getChildrenRecursive($roleName, $childrenList, $result);
        }

        if (empty($result)) {
            return [];
        }

        $query = (new Query)->from($this->itemTable)->where([
            'TYPE' => Item::TYPE_PERMISSION,
            'NAME' => array_keys($result),
        ]);
        $permissions = [];
        foreach ($query->all($this->db) as $row) {
            $permissions[$row['NAME']] = $this->populateItem($row);
        }
        return $permissions;
    }

    /**
     * @inheritdoc
     */
    protected function getChildrenList()
    {
        $query = (new Query)->from($this->itemChildTable);
        $parents = [];
        foreach ($query->all($this->db) as $row) {
            $parents[$row['PARENT']][] = $row['CHILD'];
        }
        return $parents;
    }

    /**
     * @inheritdoc
     */
    public function getRule($name)
    {
        if ($this->rules !== null) {
            return isset($this->rules[$name]) ? $this->rules[$name] : null;
        }

        $row = (new Query)->select(['DATA'])
            ->from($this->ruleTable)
            ->where(['NAME' => $name])
            ->one($this->db);
        return $row === false ? null : unserialize($row['DATA']);
    }

    /**
     * @inheritdoc
     */
    public function getRules()
    {
        if ($this->rules !== null) {
            return $this->rules;
        }

        $query = (new Query)->from($this->ruleTable);

        $rules = [];
        foreach ($query->all($this->db) as $row) {
            $rules[$row['NAME']] = unserialize($row['DATA']);
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function getAssignment($roleName, $userId)
    {
        if (empty($userId)) {
            return null;
        }

        $row = (new Query)->from($this->assignmentTable)
            ->where(['USER_ID' => (string) $userId, 'ITEM_NAME' => $roleName])
            ->one($this->db);

        if ($row === false) {
            return null;
        }

        return new Assignment([
            'userId' => $row['USER_ID'],
            'roleName' => $row['ITEM_NAME'],
            'createdAt' => $row['CREATED_AT'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getAssignments($userId)
    {
        if (empty($userId)) {
            return [];
        }

        $query = (new Query)
            ->from($this->assignmentTable)
            ->where(['USER_ID' => (string) $userId]);

        $assignments = [];
        foreach ($query->all($this->db) as $row) {
            $assignments[$row['ITEM_NAME']] = new Assignment([
                'userId' => $row['USER_ID'],
                'roleName' => $row['ITEM_NAME'],
                'createdAt' => $row['CREATED_AT'],
            ]);
        }

        return $assignments;
    }

    /**
     * @inheritdoc
     */
    public function addChild($parent, $child)
    {
        if ($parent->name === $child->name) {
            throw new InvalidParamException("Cannot add '{$parent->name}' as a child of itself.");
        }

        if ($parent instanceof Permission && $child instanceof Role) {
            throw new InvalidParamException('Cannot add a role as a child of a permission.');
        }

        if ($this->detectLoop($parent, $child)) {
            throw new InvalidCallException("Cannot add '{$child->name}' as a child of '{$parent->name}'. A loop has been detected.");
        }

        $this->db->createCommand()
            ->insert($this->itemChildTable, ['PARENT' => $parent->name, 'CHILD' => $child->name])
            ->execute();

        $this->invalidateCache();

        return true;
    }

    /**
     * @inheritdoc
     */
    public function removeChild($parent, $child)
    {
        $result = $this->db->createCommand()
                ->delete($this->itemChildTable, ['PARENT' => $parent->name, 'CHILD' => $child->name])
                ->execute() > 0;

        $this->invalidateCache();

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function removeChildren($parent)
    {
        $result = $this->db->createCommand()
                ->delete($this->itemChildTable, ['PARENT' => $parent->name])
                ->execute() > 0;

        $this->invalidateCache();

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function hasChild($parent, $child)
    {
        return (new Query)
            ->from($this->itemChildTable)
            ->where(['PARENT' => $parent->name, 'CHILD' => $child->name])
            ->one($this->db) !== false;
    }

    /**
     * @inheritdoc
     */
    public function getChildren($name)
    {
        $query = (new Query)
            ->select(['NAME', 'TYPE', 'DESCRIPTION', 'RULE_NAME', 'DATA', 'CREATED_AT', 'UPDATED_AT'])
            ->from([$this->itemTable, $this->itemChildTable])
            ->where(['PARENT' => $name, 'NAME' => new Expression('[[CHILD]]')]);

        $children = [];
        foreach ($query->all($this->db) as $row) {
            $children[$row['NAME']] = $this->populateItem($row);
        }

        return $children;
    }

    /**
     * @inheritdoc
     */
    public function assign($role, $userId)
    {
        $assignment = new Assignment([
            'userId' => $userId,
            'roleName' => $role->name,
            'createdAt' => time(),
        ]);

        $this->db->createCommand()
            ->insert($this->assignmentTable, [
                'USER_ID' => $assignment->userId,
                'ITEM_NAME' => $assignment->roleName,
                'CREATED_AT' => $assignment->createdAt,
            ])->execute();

        return $assignment;
    }

    /**
     * @inheritdoc
     */
    public function revoke($role, $userId)
    {
        if (empty($userId)) {
            return false;
        }

        return $this->db->createCommand()
            ->delete($this->assignmentTable, ['USER_ID' => (string) $userId, 'ITEM_NAME' => $role->name])
            ->execute() > 0;
    }

    /**
     * @inheritdoc
     */
    public function revokeAll($userId)
    {
        if (empty($userId)) {
            return false;
        }

        return $this->db->createCommand()
            ->delete($this->assignmentTable, ['USER_ID' => (string) $userId])
            ->execute() > 0;
    }

    /**
     * @inheritdoc
     */
    protected function removeAllItems($type)
    {
        if (!$this->supportsCascadeUpdate()) {
            $names = (new Query)
                ->select(['NAME'])
                ->from($this->itemTable)
                ->where(['TYPE' => $type])
                ->column($this->db);
            if (empty($names)) {
                return;
            }
            $key = $type == Item::TYPE_PERMISSION ? 'CHILD' : 'PARENT';
            $this->db->createCommand()
                ->delete($this->itemChildTable, [$key => $names])
                ->execute();
            $this->db->createCommand()
                ->delete($this->assignmentTable, ['ITEM_NAME' => $names])
                ->execute();
        }
        $this->db->createCommand()
            ->delete($this->itemTable, ['TYPE' => $type])
            ->execute();

        $this->invalidateCache();
    }

    /**
     * @inheritdoc
     */
    public function removeAllRules()
    {
        if (!$this->supportsCascadeUpdate()) {
            $this->db->createCommand()
                ->update($this->itemTable, ['RULE_NAME' => null])
                ->execute();
        }

        $this->db->createCommand()->delete($this->ruleTable)->execute();

        $this->invalidateCache();
    }

    public function loadFromCache()
    {
        if ($this->items !== null || !$this->cache instanceof Cache) {
            return;
        }

        $data = $this->cache->get($this->cacheKey);
        if (is_array($data) && isset($data[0], $data[1], $data[2])) {
            list($this->items, $this->rules, $this->parents) = $data;
            return;
        }

        $query = (new Query)->from($this->itemTable);
        $this->items = [];
        foreach ($query->all($this->db) as $row) {
            $this->items[$row['NAME']] = $this->populateItem($row);
        }

        $query = (new Query)->from($this->ruleTable);
        $this->rules = [];
        foreach ($query->all($this->db) as $row) {
            $this->rules[$row['NAME']] = unserialize($row['DATA']);
        }

        $query = (new Query)->from($this->itemChildTable);
        $this->parents = [];
        foreach ($query->all($this->db) as $row) {
            if (isset($this->items[$row['CHILD']])) {
                $this->parents[$row['CHILD']][] = $row['PARENT'];
            }
        }

        $this->cache->set($this->cacheKey, [$this->items, $this->rules, $this->parents]);
    }

    /**
     * @inheritdoc
     */
    public function getUserIdsByRole($roleName)
    {
        if (empty($roleName)) {
            return [];
        }

        return (new Query)->select('[[USER_ID]]')
            ->from($this->assignmentTable)
            ->where(['ITEM_NAME' => $roleName])->column($this->db);
    }
}
