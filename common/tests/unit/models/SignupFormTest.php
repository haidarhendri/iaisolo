<?php
namespace frontend\tests\unit\models;

use common\fixtures\User as UserFixture;
use common\models\SignupForm;

class SignupFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => codecept_data_dir() . 'user.php'
            ]
        ]);
    }

    public function testCorrectSignup()
    {
        $model = new SignupForm([
            'USERNAME' => 'some_username',
            'EMAIL' => 'some_email@example.com',
            'PASSWORD' => 'some_password',
        ]);

        $user = $model->signup();

        expect($user)->isInstanceOf('common\models\User');

        expect($user->USERNAME)->equals('some_username');
        expect($user->EMAIL)->equals('some_email@example.com');
        expect($user->validatePassword('some_password'))->true();
    }

    public function testNotCorrectSignup()
    {
        $model = new SignupForm([
            'USERNAME' => 'troy.becker',
            'EMAIL' => 'nicolas.dianna@hotmail.com',
            'PASSWORD' => 'some_password',
        ]);

        expect_not($model->signup());
        expect_that($model->getErrors('USERNAME'));
        expect_that($model->getErrors('EMAIL'));

        expect($model->getFirstError('USERNAME'))
            ->equals('This username has already been taken.');
        expect($model->getFirstError('EMAIL'))
            ->equals('This email address has already been taken.');
    }
}
