<?php

namespace common\traits;

/**
 * @link https://simpak.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2017 NEXPLAN
 *
 * @property \common\models\kinerja\KinerjaBukuAuthor[] $dataKinerjaBukuAuthor
 * @property \common\models\kinerja\KinerjaBuku[] $dataKinerjaBuku
 * @property \common\models\kinerja\KinerjaJurnalAuthor[] $dataKinerjaJurnalAuthor
 * @property \common\models\kinerja\KinerjaJurnal[] $dataKinerjaJurnal
 * @property \common\models\kinerja\KinerjaKaryaSeniAuthor[] $dataKinerjaKaryaSeniAuthor
 * @property \common\models\kinerja\KinerjaKaryaSeni[] $dataKinerjaKaryaSeni
 * @property \common\models\kinerja\KinerjaKoranAuthor[] $dataKinerjaKoranAuthor
 * @property \common\models\kinerja\KinerjaKoran[] $dataKinerjaKoran
 */
trait PakKinerjaRelations
{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaBukuAuthor()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaBukuAuthor::className(), ['ID_DOSEN' => 'PENGUSUL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaBuku()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaBuku::className(), ['ID_BUKU' => 'ID'])
            ->via('dataKinerjaBukuAuthor');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaJurnalAuthor()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaJurnalAuthor::className(), ['ID_DOSEN' => 'PENGUSUL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaJurnal()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaJurnal::className(), ['ID_JURNAL' => 'ID'])
            ->via('dataKinerjaJurnalAuthor');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaKaryaSeniAuthor()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaKaryaSeniAuthor::className(), ['ID_DOSEN' => 'PENGUSUL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaKaryaSeni()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this
            ->hasMany(\common\models\kinerja\KinerjaKaryaSeni::className(), ['ID_KARYASENI' => 'ID'])
            ->via('dataKinerjaKaryaSeniAuthor');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaKoranAuthor()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this->hasMany(\common\models\kinerja\KinerjaKoranAuthor::className(), ['ID_DOSEN' => 'PENGUSUL_ID']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDataKinerjaKoran()
    {
        /* @var $this \common\models\pak\PakUtama */

        return $this
            ->hasMany(\common\models\kinerja\KinerjaKoran::className(), ['ID_KORAN' => 'ID'])
            ->via('dataKinerjaKoranAuthor');
    }

}