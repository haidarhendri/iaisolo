<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search" class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search collapse" style="margin-top: 15px">
    <?= "<?php " ?>$form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
<?php if ($generator->enablePjax): ?>
        'options' => [
            'data-pjax' => 1
        ],
<?php endif; ?>
    ]); ?>

<?php
$count = 0;
foreach ($generator->getColumnNames() as $attribute) {
    if (++$count < 6) {
        echo "    <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    } else {
        echo "    <?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    }
}
?>
    <div class="form-group">
        <?= '<?= '?>Html::submitButton('<i class="fa fa-cog"></i> Proses', ['class' => 'btn btn-primary']);<?= ' ?>' . "\n" ?>

        <?= '<?= '?>Html::a(
            '<i class="glyphicon glyphicon-repeat"></i> Reset',
            ['index'],
            ['class' => 'btn btn-primary']
        );<?= ' ?>' . "\n" ?>

    </div>
    <?= '<?php '?>ActiveForm::end();<?= ' ?>' . "\n" ?>

</div>
