<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
echo "<?php\n";
?>

use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '<?= Inflector::camel2words(StringHelper::basename($generator->modelClass)) ?>';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <div class="box box-danger">
        <div class="box-body">
            <?= '<?= ' ?>ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dataProvider,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => 10,
                'target' => '_blank',
                'stream' => true,
                
                'deleteAfterSave' => true,
            ]);<?= ' ?>' . "\n" ?>

            <?= '<?= ' ?>Html::button(
                '<i class="glyphicon fa fa-search"></i> Pencarian Lanjut',
                [
                    'class' => 'btn btn-info search-button pull-right',
                    'data-toggle' => 'collapse',
                    'data-target' => '#<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search',
                ]
            );<?= ' ?>' . "\n" ?>

            <?= '<?= ' ?>$this->render('_search', ['model' => $searchModel]);<?= ' ?>' . "\n" ?>
        </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?="<?= "?>GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'toolbar' => [
                [
                    'content' => Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        ['index'],
                        [
                            'data-pjax' => 1,
                            'class' => 'btn btn-default',
                            'title' => 'Reset Grid',
                        ]
                    ) . '{toggleData} {export}',
                ],
            ],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i>',
                'before' => Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> Tambah Data',
                    ['create'],
                    ['role' => 'modal-remote', 'title' => 'Tambah Data', 'class' => 'btn btn-success']
                ),
                'after' => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                    ['index'],
                    ['class' => 'btn btn-primary btn-xs']
                )
                . '<div class="clearfix"></div>',
            ],
        ])<?=" ?>\n"?>
    </div>
</div>
<?= '<?php' . "\n" ?>
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",
    "size"=> "modal-lg",
]);
Modal::end();
<?= '?>' ?>

