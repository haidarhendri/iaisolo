<?php

use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$actionParams = $generator->generateActionParams();

echo "<?php\n";
?>

return [
<?php foreach ($generator->getColumnNames() as $name) {
    echo "    [\n";
    echo "        'class' => '\kartik\grid\DataColumn',\n";
    echo "        'attribute' => '" . $name . "',\n";
    echo "    ],\n";
} ?>
];
