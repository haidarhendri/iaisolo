<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@simpak', dirname(dirname(__DIR__)) . '/simpak');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@themes', dirname(dirname(__DIR__)) . '/themes');

Yii::setAlias('@uploadPath', 'uploads/');
Yii::setAlias('@uploadUrl', 'https://user.iaisolo.id/uploads/');
