<?php
return [
    'adminEmail' => 'apoteker.solo@gmail.com',
    'supportEmail' => 'apoteker.solo@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
