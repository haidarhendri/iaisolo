<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <div class="box box-danger">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <p><?= 'Silkan isi form berikut untuk me-reset password:'; ?></p>
            <?= $form->field($model, 'passwordOld')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('password_old'),
            ]) ?>

            <?= $form->field($model, 'password')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('password'),
            ]) ?>

            <?= $form->field($model, 'passwordRepeat')->passwordInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel('passwordRepeat'),
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Reset Password', ['class' => 'btn btn-primary']) ?>

            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
