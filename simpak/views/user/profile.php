<?php

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Profil Saya';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('_profile', [
        'model' => $model,
    ]) ?>
</div>
