<?php

use common\models\referensi\RefAuthItem;
use common\models\referensi\RefUnit;
use common\models\referensi\RefUnitFakultas;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use simpak\models\UserSearch;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this View */
/* @var $model UserSearch */
/* @var $form ActiveForm */
?>

<div id="user-search" class="user-search">
    <?php
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    ?>

    <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'ITEM_NAME')->widget(Select2::class, [
                'data' => RefAuthItem::map(),
                'options' => ['placeholder' => '--Pilih Role--'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('Role')
            ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'STATUS')->dropDownList($model->getStatuses(), ['prompt' => '']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-search"></i> Cari', ['class' => 'btn btn-primary']) ?>
        <?=
        Html::a(
            '<i class="glyphicon glyphicon-repeat"></i> Reset',
            ['index'],
            ['class' => 'btn btn-primary']
        )
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>