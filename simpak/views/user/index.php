<?php
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel simpak\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Manajemen Pengguna';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);
?>
<div class="user-index">
    <div class="box box-danger">
        <div class="box-body">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dataProvider,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => 10,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
            ]); ?>

            <?= Html::button(
                '<i class="glyphicon fa fa-search"></i> Pencarian Lanjut',
                [
                    'class' => 'btn btn-info search-button pull-right',
                    'data-toggle' => 'collapse',
                    'data-target' => '#user-search',
                ]
            ) ?>

            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                [
                    'content' => Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            [''],
                            [
                                'data-pjax' => 1,
                                'class' => 'btn btn-default',
                                'title' => 'Reset Grid',
                            ]
                        ) . '{toggleData} {export}',
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i>',
                'before' => Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> Tambah Data',
                    ['create'],
                    ['role' => 'modal-remote', 'title' => 'Tambah Data', 'class' => 'btn btn-success']
                ),
                'after' => Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                        ['index'],
                        ['class' => 'btn btn-primary btn-xs']
                    )
                    . '<div class="clearfix"></div>',
            ],
        ]) ?>
    </div>
</div>
<?php
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",
    "size"=> "modal-lg",// always need it for jquery plugin
]);
Modal::end();
?>
