<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'User #' . $model->USERNAME;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="box box-danger">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'USERNAME',
                'EMAIL:email',
                'FULL_NAME',
                'DISPLAY_NAME',
                'AUTH_KEY',
                [
                    'attribute' => 'STATUS',
                    'value' => $model->getStatusText(),
                ],
                'CREATED_AT',
                'UPDATED_AT',
                'LOGIN_AT',
            ],
        ]) ?>

    </div>
</div>
