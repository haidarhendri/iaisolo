<?php

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <?= $this->render('_reset-password', [
        'model' => $model,
    ]) ?>
</div>
