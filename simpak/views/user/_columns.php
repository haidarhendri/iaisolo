<?php
/* @var $this yii\web\View */
/* @var $searchModel simpak\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to(['/rbac/assignment/view', 'id' => $key]);
        },
        'template' => '{assign}',
        'buttons' => [
            'assign' => function ($url, $model, $key) {
                return Html::a('Assign', $url, [
                    'data-pjax' => 0,
                    'data-toggle' => 'tooltip',
                    'title' => 'Assign User',
                    'class' => 'btn btn-sm btn-success',
                ]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
        ],
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Role',
        'value' => function ($model, $key, $index, $column) {
            $role = '';
            foreach ($model->appAuthAssignment as $assignment) {
                $role .= $assignment->authItem['DESCRIPTION'] . ', <br>';
            }
            return $role;
        },
        'format' => 'raw',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'USERNAME',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'EMAIL',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'STATUS',
        'value' => 'statusText',
        'filter' => $searchModel->getStatuses()
    ],

];
