<?php
use yii\helpers\Url;

return [
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'USERNAME',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'EMAIL',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'DISPLAY_NAME',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'STATUS',
        'value' => 'statusText'
    ],
];
