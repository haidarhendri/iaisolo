<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-form">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <h2 class="box-title text-uppercase"><?= 'Form User' ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'EMAIL')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'NO_HP')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'USERNAME')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'KORWIL')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'NO_KTA')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'GELAR_DEPAN')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'FULL_NAME')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'GELAR_BELAKANG')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'TEMPAT_LAHIR')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'TANGGAL_LAHIR')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'JENIS_KELAMIN')->dropDownList(['L' => 'L', 'P' => 'P',], ['prompt' => '']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'GOL_DARAH')->dropDownList([
                        'A' => 'A',
                        'B' => 'B',
                        'O' => 'O',
                        'AB' => 'AB',
                    ], ['prompt' => '']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'AGAMA')->dropDownList([
                        'ISLAM' => 'ISLAM',
                        'KRISTEN' => 'KRISTEN',
                        'KATHOLIK' => 'KATHOLIK',
                        'HINDU' => 'HINDU',
                        'BUDHA' => 'BUDHA',
                        'KONGHUCHU' => 'KONGHUCHU',
                        'LAINNYA' => 'LAINNYA',
                    ], ['prompt' => '']) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'ALAMAT_KTP')->textarea(['rows' => 6]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'role')->dropDownList(\yii\helpers\ArrayHelper::map(
                        Yii::$app->authManager->getRoles(),
                        'name',
                        'name'
                    )); ?>
                </div>
            </div>



            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton(
                        $model->isNewRecord ? 'Tambah' : 'Update',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    )
                    ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>