<?php

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'User #' . $model->USERNAME;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
