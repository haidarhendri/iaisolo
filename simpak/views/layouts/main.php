<?php

use themes\adminlte\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $content string */

$this->beginContent('@app/views/layouts/blank.php');
?>
<div class="wrapper">
    <?= $this->render('header'); ?>
    <?= $this->render('sidebar'); ?>
    <div class="content-wrapper">
        <section class="content-header">
            <h1><?= $this->title; ?></h1>
            <?= Breadcrumbs::widget([
                'homeLink' => [
                    'label' => Html::a(
                        '<i class="fa fa-dashboard"></i>' . Yii::t('app', 'Beranda'),
                        Yii::$app->homeUrl
                    ),
                ],
                'encodeLabels' => false,
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>
        <section class="content clearfix">
            <?= Alert::widget() ?>
            <?= $content; ?>
        </section>
    </div>
    <?= $this->render('footer'); ?>
</div>
<?php $this->endContent(); ?>
