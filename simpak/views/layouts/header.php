<?php

use cebe\gravatar\Gravatar;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $identity common\models\User */
/* @var $user yii\web\User */

$user = Yii::$app->user;
$identity = $user->identity;
?>
<header class="main-header">
    <?= Html::a(
        Html::tag('span', Html::img(Yii::getAlias('@web/img/iai-logo-mini.jpg')), ['class' => 'logo-mini']) .
        Html::tag('span', 'IAI-<b>SOLO</b>', ['class' => 'logo-lg']), ['/site/index'], ['class' => 'logo']
    ); ?>

    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                
                <?php if (!$user->isGuest): ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?= Gravatar::widget([
                                'email' => $identity->EMAIL,
                                'options' => [
                                    'alt' => $identity->USERNAME,
                                    'class' => 'user-image',
                                ],
                                'size' => 25,
                            ]); ?>
                            <span class="hidden-xs"><?= $identity->FULL_NAME; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <?= Gravatar::widget([
                                    'email' => $identity->EMAIL,
                                    'options' => [
                                        'alt' => $identity->USERNAME,
                                        'class' => 'img-circle',
                                    ],
                                    'size' => 84,
                                ]); ?>

                                <p><?= $identity->FULL_NAME; ?></p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-right">
                                    <?= Html::a(
                                        'Keluar',
                                        ['/site/logout'],
                                        ['class' => 'btn btn-default btn-flat', 'data-method' => 'post']
                                    ); ?>

                                </div>
                            </li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li>
                        <a href="<?= Url::to(['/site/login']); ?>">
                            <i class="fa fa-sign-in"></i> <span>Login</span>
                        </a>
                    </li>
                <?php endif; ?>

            </ul>
        </div>
    </nav>
</header>
