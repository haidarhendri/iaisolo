<?php

use cebe\gravatar\Gravatar;
use themes\adminlte\widgets\Menu;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $identity \common\models\User */

$identity = Yii::$app->user->identity;
?>
<aside class="main-sidebar">
    <section class="sidebar">

        <?php if (!Yii::$app->user->isGuest) : ?>
            <div class="user-panel">
                <div class="pull-left image">
                    <?=
                    Gravatar::widget([
                        'email' => $identity->EMAIL,
                        'options' => [
                            'alt' => $identity->USERNAME,
                            'class' => 'img-circle',
                        ],
                        'size' => 45,
                    ])
                    ?>
                </div>
                <div class="pull-left info">
                    <p><?= $identity->FULL_NAME; ?></p>
                    <?=
                    Html::a(
                        '<i class="circle text-success"></i>'
                            . 'Online',
                        ['/user/profile']
                    )
                    ?>
                </div>
            </div>
        <?php endif ?>

        <?php
        $adminMenu[] = [
            'label' => Yii::t('app', 'MENU UTAMA'),
            'options' => ['class' => 'header'],
            'template' => '{label}',
        ];

        $adminMenu[] = [
            'icon' => 'home',
            'label' => Yii::t('app', 'Home'),
            'url' => ['/site/index'],
        ];

        $adminMenu[] = [
            'label' => 'Anggota',
            'icon' => 'address-book',
            'items' => [
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Data Utama'),
                    'url' => ['/anggota/site/index'],
                    'visible' => Yii::$app->user->can('perm_anggota'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Pendidikan'),
                    'url' => ['/anggota/pendidikan/index'],
                    'visible' => Yii::$app->user->can('perm_anggota'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Pekerjaan'),
                    'url' => ['/anggota/pekerjaan/index'],
                    'visible' => Yii::$app->user->can('perm_anggota'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Iuran'),
                    'url' => ['/anggota/iuran/index'],
                    'visible' => Yii::$app->user->can('perm_anggota'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Sertifikat'),
                    'url' => ['/anggota/sertif/index'],
                    'visible' => Yii::$app->user->can('perm_anggota'),
                ],
            ],
            'visible' => Yii::$app->user->can('perm_anggota'),
        ];

        $adminMenu[] = [
            'label' => 'Sekretaris',
            'icon' => 'address-book',
            'items' => [
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Daftar Anggota'),
                    'url' => ['/sekretaris/site/index'],
                    'visible' => Yii::$app->user->can('perm_sekretaris'),
                ],
            ],
            'visible' => Yii::$app->user->can('perm_sekretaris'),
        ];

        $adminMenu[] = [
            'label' => 'Bendahara',
            'icon' => 'calculator',
            'items' => [
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Iuran Anggota'),
                    'url' => ['/bendahara/iuran/index'],
                    'visible' => Yii::$app->user->can('perm_bendahara'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Referensi Iuran'),
                    'url' => ['/bendahara/ref-iuran/index'],
                    'visible' => Yii::$app->user->can('perm_bendahara'),
                ],
            ],
            'visible' => Yii::$app->user->can('perm_bendahara'),
        ];

        $adminMenu[] = [
            'label' => 'Agenda',
            'icon' => 'calculator',
            'items' => [
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Manajemen Agenda'),
                    'url' => ['/agenda/site/index'],
                    'visible' => Yii::$app->user->can('perm_agenda_manajemen'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Riwayat Agenda'),
                    'url' => ['/agenda/riwayat/index'],
                    'visible' => Yii::$app->user->can('perm_agenda_riwayat'),
                ],
                [
                    'icon' => 'circle-o',
                    'label' => Yii::t('app', 'Riwayat Pembayaran Agenda'),
                    'url' => ['/agenda/pembayaran/index'],
                    'visible' => Yii::$app->user->can('perm_agenda_pembayaran'),
                ],
            ],
            'visible' => Yii::$app->user->can('perm_agenda_riwayat'),
        ];

        $adminMenu[] = [
            'label' => 'Administrator System',
            'icon' => 'users',
            'items' => [
                [
                    'label' => Yii::t('app', 'Pengaturan User'),
                    'icon' => 'users',
                    'items' => [
                        [
                            'icon' => 'circle-o',
                            'label' => Yii::t('app', 'Semua User'),
                            'url' => ['/user/index'],
                            'visible' => Yii::$app->user->can('role_superuser'),
                        ],
                        [
                            'icon' => 'circle-o',
                            'label' => Yii::t('app', 'Tambah User'),
                            'url' => ['/user/create'],
                            'visible' => Yii::$app->user->can('role_superuser'),
                        ],
                        [
                            'icon' => 'circle-o',
                            'label' => Yii::t('app', 'Profil Saya'),
                            'url' => ['/user/profile'],
                            'visible' => Yii::$app->user->can('role_superuser'),
                        ],
                        [
                            'icon' => 'circle-o',
                            'label' => Yii::t('app', 'Ubah Kata Sandi'),
                            'url' => ['/user/reset-password'],
                            'visible' => Yii::$app->user->can('role_superuser'),
                        ],
                        [
                            'icon' => 'circle-o',
                            'label' => Yii::t('app', 'RBAC'),
                            'url' => ['/rbac/assignment/index'],
                            'visible' => Yii::$app->user->can('role_superuser'),
                        ],
                    ],
                ],
                [
                    'label' => 'Referensi',
                    'icon' => 'list',
                    'url' => ['/referensi/default/index'],
                    'visible' => Yii::$app->user->can('role_superuser'),
                ],
                [
                    'label' => 'Setting',
                    'icon' => 'list',
                    'url' => ['/referensi/setting/index'],
                    'visible' => Yii::$app->user->can('role_superuser'),
                ],
            ],
            'visible' => Yii::$app->user->can('role_superuser'),
        ];

        $adminMenu[] = [
            'icon' => 'sign-in',
            'label' => Yii::t('app', 'Login'),
            'url' => ['/site/login'],
            'visible' => Yii::$app->user->isGuest,
        ];

        $adminMenu[] = [
            'icon' => 'sign-out',
            'label' => Yii::t('app', 'Logout'),
            'url' => ['/site/logout'],
            'linkOptions' => ['data-method' => 'post'],
            'visible' => !Yii::$app->user->isGuest,
        ];

        if (isset(Yii::$app->params['adminMenu']) && is_array(Yii::$app->params['adminMenu'])) {
            $adminMenu = ArrayHelper::merge($adminMenu, Yii::$app->params['adminMenu']);
        }

        echo Menu::widget([
            'items' => $adminMenu,
        ]);
        ?>
    </section>
</aside>