<?php
/* @var $this yii\web\View */
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versi</b> 0.1.0
    </div>
    <strong>
        Copyright &copy; <?= date('Y'); ?> <a href="http://www.puskom.uns.ac.id">NEXPLAN</a>.
    </strong> All rights reserved.
</footer>
