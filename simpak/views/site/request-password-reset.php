<?php
/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */

use common\models\PasswordResetRequestForm;
use themes\adminlte\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model PasswordResetRequestForm */

$this->title = Yii::t('app', 'Ubah Kata Sandi');
?>
<div class="login-box">
    <div class="login-logo">
        <h1>
            <a href="https://iaisolo.id/">
                <img src="<?= Yii::getAlias('@web/img/iai-logo-mini.jpg') ?>" alt="IAI-SOLO">
                IKATAN APOTEKER INDONESIA - SOLO
            </a>
        </h1>
    </div>

    <?= Alert::widget() ?>

    <div class="login-box-body">
        <p class="login-box-msg">
            <?= Yii::t('app', 'Silakan isi email anda. Sebuah link akan dikirimkan ke email tersebut.') ?>
        </p>

        <?php $form = ActiveForm::begin(['id' => 'request-password-token-form']) ?>

        <?= $form->field($model, 'EMAIL', [
            'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>{error}',
        ])->textInput(['placeholder' => $model->getAttributeLabel('EMAIL')]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Kirim'), ['class' => 'btn btn-flat btn-primary form-control']) ?>

        </div>
        <?php ActiveForm::end() ?>

    </div>
    <br/>
    <?= Html::a(
        '<i class="fa fa-home"></i> ' . Yii::t(
            'app', 'Kembali {sitetitle}',
            ['sitetitle' => 'IAI-SOLO']
        ),
        Url::to(['/site/index']),
        ['class' => 'btn btn-block btn-success btn-flat']
    ) ?>

</div>
