<?php
/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */

use common\models\ResetPasswordForm;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model ResetPasswordForm */

$this->title = Yii::t('app', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <h1>
            <a href="https://iaisolo.id/">
                <img src="<?= Yii::getAlias('@web/img/iai-logo-mini.jpg') ?>" alt="UNS">
                IKATAN APOTEKER INDONESIA - SOLO
            </a>
        </h1>
    </div>

    <?= Alert::widget() ?>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('app', 'Silakan memilih password baru anda.') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']) ?>

        <?= $form->field($model, 'password', [
            'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span></div>{error}',
        ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Simpan'), ['class' => 'btn btn-flat btn-primary form-control']) ?>

        </div>
        <?php ActiveForm::end() ?>

    </div>
    <?= Html::a(
        '<i class="fa fa-home"></i> ' . Yii::t(
            'app', 'Kembali {sitetitle}',
            ['sitetitle' => 'IAI-SOLO']
        ),
        Url::to(['/site/index']),
        ['class' => 'btn btn-block btn-success btn-flat']
    ) ?>
    
</div>
