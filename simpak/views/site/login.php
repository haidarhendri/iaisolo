<?php

/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */

use themes\adminlte\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\LoginForm */

$this->title = Yii::t('app', 'Sign In');
?>
<div class="login-box">
    <div class="login-logo">
        <h1>
            <a href="https://iaisolo.id/">
                IKATAN APOTEKER INDONESIA - SOLO
            </a>
        </h1>
    </div>

    <?= Alert::widget() ?>

    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('app', 'Login Keanggotaan IAI Solo') ?></p>
        <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>

        <?= $form->field($model, 'username', [
            'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-user form-control-feedback"></span></div>{error}',
        ])->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form->field($model, 'password', [
            'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span></div>{error}',
        ])->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>

            </div>
            <div class="col-xs-4">
                <?= Html::submitButton(Yii::t('app', 'Sign In'), [
                    'class' => 'btn btn-primary btn-block btn-flat',
                    'name' => 'signin-button',
                ]) ?>

            </div>
        </div>
        <?php ActiveForm::end() ?>

        <?= Html::a(Yii::t('app', 'Ubah kata sandi'), ['request-password-reset']) ?><br />

        Belum mempunyai akun? <?= Html::a(Yii::t('app', 'Daftar'), ['signup'], ['class' => 'btn btn-xs btn-success']) ?><br />

    </div>
    <br />
    
    <br />
    <?php
    // Html::a(
    // '<i class="fa fa-sign-in"></i> ' . Yii::t(
    //     'app', 'SSO UNS',
    //     ['sitetitle' => 'Login Menggunakan SSO UNS']
    // ),
    // Url::to(['/sso/login']),
    // ['class' => 'btn btn-block btn-primary btn-flat']) 
    ?>

    <?=
    Html::a(
    '<i class="fa fa-home"></i> ' . Yii::t(
        'app', 'Kembali {sitetitle}',
        ['sitetitle' => 'IAI-SOLO']
    ),
    Url::to(['/site/index']),
    ['class' => 'btn btn-block btn-success btn-flat']) 
    ?>

</div>