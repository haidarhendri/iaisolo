<?php
/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */
/* @var $this yii\web\View */

// $this->title = Yii::t('app', 'Sistem Informasi Manajemen Keanggotaan ');

$this->registerCss('
.features-box { padding: 0 10px 10px;}
.features-box .fa{ color:#3C8DBC;font-size: 16px;padding: 5px;}
.features-box ul { list-style: none;padding-left:0;}
.features-box li { font-size:18px;}
');
?>
<div class="site-index">
    <div class="box box-danger no-border">
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-sm-6 hidden-xs">
                    <img src="<?= Yii::getAlias('@web/img/banner-image.png') ?>" height="200" style="float: right;" alt="IAI-SOLO - Sistem Informasi Manajemen Keanggotaan ">
                </div>
                <div class="col-md-8 col-sm-6" style="margin-top:20px;">
                    <h4 class="h4" style="margin-bottom:0;font-size: 30px;color: #F90;">SISTEM INFORMASI MANAJEMEN</h4>
                    <h3 class="h3" style="margin-top: 0;font-weight: bold;font-size: 50px;color: #3C8DBC;">PENILAIAN </h3>
                </div>
            </div>
            <hr style="border-width: 3px 0 0;border-color: #FF8700;margin: 10px 0 0 0;" />
            <hr style="border-width: 3px 0 0;border-color: #3C8DBC;margin: 0;" />
            <div class="row">
                <div class="col-lg-4">
                    <div class="features-box">
                        <h4 class="h2 text-bold text-primary"> <i class="fa fa-calculator fa-5x"></i> Kalkulasi</h4>
                        <hr style="margin-top:0;border-bottom:1px dotted #3C8DBC;"/>
                        <p style="font-size:18px;">Dosen dapat melakukan manajemen data kenaikan angka kredit <b>secara mandiri</b>, dengan fitur sebagai berikut:
                        </p>
                        <ul>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Manajemen Data Kalkulasi Kenaikan Angka Kredit
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Data Usul Penilaian Angka Kredit(DUPAK)
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Form Dupak (Bidang A,B,C,D, dan E)
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Peer Review Bidang Penelitian (Bidang C)
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Dokumen Pengesahan Kaprodi, Dekan, Rektor
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Pengesahan Karya Ilmiah 
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="features-box">
                        <h4 class="h2 text-bold text-primary"><i class="fa fa-file-text-o fa-5x"></i> Administrasi</h4>
                        <hr style="margin-top:0;border-bottom:1px dotted #3C8DBC;"/>
                        <p style="font-size:18px;">
                            Dokumen-dokumen <b>administrasi pendukung</b> penilaian angka kredit dapat dicetak oleh operator PAK, yaitu:
                        </p>
                        <ul>
                            <li style="font-size:16px"> 
                                <i class="fa fa-check-circle"></i> Surat Pengantar ke Kaprodi, ke Fakultas, dan ke Rektor
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Undangan ke Tim PAK
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Undangan Rapat Senat
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Form Review DUPAK
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Surat Pengantar Revisi atau Penolakan
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Form Rapat Pleno Senat Akademik Fakultas
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Cetak Form Syarat Administrasi
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="features-box">
                        <h4 class="h2 text-bold text-primary"><i class="fa fa-balance-scale fa-5x"></i>Penilaian</h4>
                        <hr style="margin-top:0;border-bottom:1px dotted #3C8DBC;"/>
                        <p style="font-size:18px;">
                            Proses Penilaian Angka Kredit Usulan Kenaikan Pangkat/Jabatan yang sebelumnya dilakukan secara manual (menggunakan dokumen fisik) <b>seluruhnya akan dilakukan secara online/tanpa dokumen fisik (paperless)</b>, yaitu :
                        </p>
                        <ul>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Penilaian Per-Review Karya Ilmiah oleh Rekan Sejawat
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Penilaian Angka Kredit Bidang A (Pendidikan), Bidang B (Pengajaran), Bidang C (Penelitian), Bidang D (Pengabdian) dan Bidang E (Unsur Penunjang)
                            </li>
                            <li style="font-size:16px">
                                <i class="fa fa-check-circle"></i> Penilaian Karya Ilmiah Oleh Komisi F Universitas
                            </li>
                        </ul>
                    </div>
                </div>                            
            </div>
        </div>
    </div>
</div>
