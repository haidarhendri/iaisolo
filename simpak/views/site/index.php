<?php

/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */
/* @var $this yii\web\View */

// $this->title = Yii::t('app', 'Sistem Informasi Manajemen Keanggotaan ');

use yii\helpers\Html;
use kv4nt\owlcarousel\OwlCarouselWidget;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\base\Application;

$userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

$this->registerCss('
.features-box { padding: 0 10px 10px;}
.features-box .fa{ color:#3C8DBC;font-size: 16px;padding: 5px;}
.features-box ul { list-style: none;padding-left:0;}
.features-box li { font-size:18px;}
');
$this->registerCss($this->render('_style.css'));
?>

<div class="site-index">
    <div class="box box-danger no-border">
        <div class="box-body">
            <div class="row">
                <br>
                <div class="col-md-4">
                    <img src="<?= Yii::getAlias('@web/img/iai-logo.png') ?>" height="150" style="float: right;" alt="IAI-SOLO - Sistem Informasi Manajemen Keanggotaan ">
                </div>
                <div class="col-md-8" style="margin-top:20px;">
                    <h3 class="h3" style="margin-top:0;margin-bottom:0;font-size: 25px;color: #000000;">Sistem Informasi Manajemen Keanggotaan</h3>
                    <h2 class="h2" style="margin-top:0;font-weight: bold;font-size: 40px;color: #ea4141;">Ikatan Apoteker Indonesia</h2>
                    <h4 class="h4" style="margin-top:0;font-weight: bold;font-size: 20px;color: #ea4141;">Cabang Solo</h4>
                </div>
            </div>
            <br>
        </div>
    </div>
    <?php
    if ($userId) {
        echo $this->render('@common/widgets/Tagihan');
    }

    foreach ($models as $model) { ?>
        <div class="col-md-6">
            <div class="box box-danger">
                <div class="box-body">
                    <article class="post vt-post">
                        <div class="row">
                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                                <div class="post-type post-img">
                                    <a href="<?= $model->idDokumen->FILE_URL ?>"><img src="<?= $model->idDokumen->FILE_URL ?>" class="img-responsive" alt="Gambar Agenda"></a>
                                </div>
                                <div class="author-info author-info-2">
                                    <ul class="list-inline">
                                        <li>
                                            <div class="info">
                                                <p>Diterbitkan pada :</p>
                                                <strong><?= Yii::$app->formatter->asDate($model->CREATE_DATE, 'php:d F Y'); ?></strong>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8">
                                <div class="caption" maxlength="50">
                                    <h3 class="md-heading"><a href="#"><?= $model->AGENDA_NAMA; ?></a></h3>
                                    <hr>
                                    <?= substr($model->AGENDA_DESKRIPSI, 0, 450) ?><br><br>
                                    <a class="btn btn-danger" href="<?= Url::to(['/agenda/site/lihat', 'id' => $model->ID]); ?>" role="button">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    <?php
    }

    echo LinkPager::widget([
        'pagination' => $pages,
    ]);
    ?>
</div>