<?php

/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */

use app\models\RefKorwil;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form ActiveForm */
/* @var $model SignupForm */

$this->title = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="register-box" style="width:50%">
    <div class="login-logo">
        <h1>
            <a href="https://iaisolo.id/">
                IKATAN APOTEKER INDONESIA - SOLO
            </a>
        </h1>
    </div>

    <?= Alert::widget() ?>

    <div class="register-box-body">
        <p class="login-box-msg"><?= Yii::t('app', 'Pendaftaran anggota baru') ?></p>

        <?php $form = ActiveForm::begin(['id' => 'signup-form']) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'EMAIL', [
                    'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-envelope form-control-feedback"></span></div>{error}',
                ])->textInput(['placeholder' => $model->getAttributeLabel('EMAIL')]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'PASSWORD', [
                    'template' => '<div class="form-group has-feedback">{input}<span class="glyphicon glyphicon-lock form-control-feedback"></span></div>{error}',
                ])->passwordInput(['placeholder' => $model->getAttributeLabel('PASSWORD')]) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'KORWIL')->widget(Select2::className(), [
                    'data' => ArrayHelper::map(
                        RefKorwil::find()
                            ->select(['ID', 'NAMA_WILAYAH',])
                            ->asArray()
                            ->all(),
                        'ID',
                        'NAMA_WILAYAH'
                    ),
                    'options' => ['placeholder' => '-- Pilih --'],
                    'pluginOptions' => ['allowClear' => true],
                ]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'NO_KTA')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'NO_HP')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'GELAR_DEPAN')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'FULL_NAME')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'GELAR_BELAKANG')->textInput(['maxlength' => true]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'TEMPAT_LAHIR')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'TANGGAL_LAHIR')->widget(DatePicker::className(), [
                    'layout' => '{input}{picker}',
                    'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                ]); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'JENIS_KELAMIN')->dropDownList(['L' => 'L', 'P' => 'P',], ['prompt' => '-- Pilih Jenis Kelamin --']) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'GOL_DARAH')->dropDownList([
                    'A' => 'A',
                    'B' => 'B',
                    'O' => 'O',
                    'AB' => 'AB',
                ], ['prompt' => '-- Pilih Golongan Darah --']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'AGAMA')->dropDownList([
                    'ISLAM' => 'ISLAM',
                    'KRISTEN' => 'KRISTEN',
                    'KATHOLIK' => 'KATHOLIK',
                    'HINDU' => 'HINDU',
                    'BUDHA' => 'BUDHA',
                    'KONGHUCHU' => 'KONGHUCHU',
                    'LAINNYA' => 'LAINNYA',
                ], ['prompt' => '-- Pilih Agama --']) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'ALAMAT_KTP')->textarea(['rows' => 6]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'ALAMAT_DOMISILI')->textarea(['rows' => 6]) ?>
            </div>
        </div>

        <div class="form-group">
            <?= Html::submitButton('Daftar', [
                'class' => 'btn btn-primary btn-block btn-flat',
                'name' => 'signup-button',
            ]) ?>

        </div>
        <?php ActiveForm::end() ?>

        <?= Html::a(Yii::t('app', 'Saya sudah mempunyai keanggotaan.'), ['login']) ?>

    </div>
    <br />
    <?= Html::a(
        '<i class="fa fa-home"></i> ' . Yii::t(
            'app',
            'Kembali {sitetitle}',
            ['sitetitle' => 'IAI-SOLO']
        ),
        Url::to(['/site/index']),
        ['class' => 'btn btn-block btn-success']
    ) ?>

</div>