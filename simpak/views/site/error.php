<?php
/**
 * @link http://app.uns.ac.id/
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2016 UNS
 */

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

$this->title = $name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-error">
    <div class="alert alert-danger"><?= $message ?></div>
    <p>The above error occurred while the Web server was processing your request.</p>
    <p>Please contact us if you think this is a server error. Thank you.</p>
</div>
