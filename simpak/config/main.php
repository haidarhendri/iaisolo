<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$baseUrlFront = str_replace('/admin', '', (new \yii\web\Request())->getBaseUrl());

return [
    'id' => 'IAI-Solo',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'simpak\controllers',
    'bootstrap' => ['log', 'rbac'],
    'modules' => [
        'rbac' => [
            'class' => 'mdm\admin\Module',
            'mainLayout' => '@simpak/views/layouts/rbac.php',
            'layout' => 'left-menu',
            'controllerMap' => [
                'assignment' => [
                    'class' => 'mdm\admin\controllers\AssignmentController',
                    'searchClass' => 'simpak\models\UserSearch',
                    'idField' => 'ID',
                    'usernameField' => 'USERNAME',
                ],
            ],
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'referensi' => [
            'class' => 'simpak\modules\referensi\Module',
        ],
        'sekretaris' => [
            'class' => 'simpak\modules\sekretaris\Module',
        ],
        'bendahara' => [
            'class' => 'simpak\modules\bendahara\Module',
        ],
        'verifikator' => [
            'class' => 'simpak\modules\verifikator\Module',
        ],
        'anggota' => [
            'class' => 'simpak\modules\anggota\Module',
        ],
        'agenda' => [
            'class' => 'simpak\modules\agenda\Module',
        ],
    ],
    'components' => [
        'saml' => [
            'class' => 'asasmoyo\yii2saml\Saml',
            'configFileName' => '@simpak/config/saml.php',
        ],
        'request' => [
            'csrfParam' => '_csrf-simpak',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-simpak', 'httpOnly' => true],
        ],
        'session' => [
            'name' => 'PHPPAKSESSID',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'umSimpeg' => [
            'class' => 'yii\web\UrlManager',
            'baseUrl' => 'https://simpeg.uns.ac.id',
            'scriptUrl' => 'https://simpeg.uns.ac.id/index.php',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'rules' => array(
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ],
        'authManager' => [
            'class' => 'common\classes\DbManager',
            // 'db' => 'dbsimpeg',
            'itemTable' => '{{%APP_AUTH_ITEM}}',
            'itemChildTable' => '{{%APP_AUTH_ITEM_CHILD}}',
            'assignmentTable' => '{{%APP_AUTH_ASSIGNMENT}}',
            'ruleTable' => '{{%APP_AUTH_RULE}}',
        ],
    ],
    'params' => $params,
];
