<?php
/**
 * @link https://iaisolo.id/admin
 * @author Haidar H. Setyawan <agielkurniawns@gmail.com>
 * @copyright Copyright (c) 2019 NEXPLAN
 */

namespace simpak\assets;

use yii\web\AssetBundle;

/**
 * Asset bundle for backend.
 *
 * @package backend\assets
 * @author  Haidar H. Setyawan <13nightevil@gmail.com>
 * @since   0.1.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'rmrevin\yii\fontawesome\AssetBundle',
        'themes\adminlte\assets\FastClickAsset',
        'themes\adminlte\assets\SlimScrollAsset',
        'themes\adminlte\assets\ThemeAsset',
        'simpak\assets\AppAssetIe9',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        if (YII_DEBUG) {
            $this->css = ['css/site.css'];
            $this->js = ['js/match.height.js'];
        } else {
            $this->css = ['css/site.min.css'];
            $this->js = ['js/match.height.min.js'];
        }
    }
}
