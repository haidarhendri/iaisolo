<?php
/**
 * @link https://iaisolo.id/admin
 * @author Haidar H. Setyawan <agielkurniawns@gmail.com>
 * @copyright Copyright (c) 2019 NEXPLAN
 */

namespace simpak\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Register html5shiv.js and respond.min.js when browser is Internet Explorer 9
 *
 * @package backend\assets
 * @author  Haidar H. Setyawan <13nightevil@gmail.com>
 * @since   0.1.0
 */
class AppAssetIe9 extends AssetBundle
{
    /**
     * @var array
     */
    public $js = [
        '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
        '//oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
    ];
    /**
     * @var array
     */
    public $jsOptions = [
        'condition' => 'lt IE 9',
        'position' => View::POS_HEAD,
    ];
}
