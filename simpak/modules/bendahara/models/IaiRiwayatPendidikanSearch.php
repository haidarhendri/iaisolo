<?php

namespace simpak\modules\bendahara\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatPendidikan;
use common\models\User;
use Yii;

/**
 * IaiRiwayatPendidikanSearch represents the model behind the search form about `common\models\IaiRiwayatPendidikan`.
 */
class IaiRiwayatPendidikanSearch extends IaiRiwayatPendidikan
{
    public $FULL_NAME;
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['FULL_NAME']);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['JENJANG', 'NAMA_INSTITUSI', 'NOMOR_IJAZAH', 'TANGGAL_IJAZAH', 'FILE_IJAZAH', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP', 'FULL_NAME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {        
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.FULL_NAME'
            ])
            ->from(['A' => IaiRiwayatPendidikan::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
            ], false)
            ->orderBy(['A.JENJANG' => SORT_DESC]);

        // if ($user->can('role_bendahara') && !$user->can('role_superuser')) {
        //     $query->andWhere(['A.ID_TRANSAKSI' => $identity->ID]);
        // }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_IJAZAH' => $this->TANGGAL_IJAZAH,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'JENJANG', $this->JENJANG])
            ->andFilterWhere(['like', 'NAMA_INSTITUSI', $this->NAMA_INSTITUSI])
            ->andFilterWhere(['like', 'NOMOR_IJAZAH', $this->NOMOR_IJAZAH])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'FILE_IJAZAH', $this->FILE_IJAZAH])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_IJAZAH' => $this->TANGGAL_IJAZAH,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'JENJANG', $this->JENJANG])
            ->andFilterWhere(['like', 'NAMA_INSTITUSI', $this->NAMA_INSTITUSI])
            ->andFilterWhere(['like', 'NOMOR_IJAZAH', $this->NOMOR_IJAZAH])
            ->andFilterWhere(['like', 'FILE_IJAZAH', $this->FILE_IJAZAH])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatPendidikan::find();
    }
}
