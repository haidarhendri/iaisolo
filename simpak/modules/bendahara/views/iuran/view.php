<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatIuran */

$this->title = 'Riwayat Iuran';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pembayaran Iuran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-iuran-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Riwayat Iuran' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'NOMINAL',
                    'TANGGAL_AWAL',
                    'TANGGAL_AKHIR',
                    'STATUS_BAYAR',
                    'TANGGAL_BAYAR',
                ],
            ]) ?>

        </div>
    </div>
</div>
