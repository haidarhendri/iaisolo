<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatIuran */

$this->title = 'Update Riwayat Iuran';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pembayaran Iuran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-iuran-update">
    <?= $this->render('_form-edit', [
        'model' => $model,
        'dokumen' => $dokumen,
        'modelDok' => $modelDok,
        'type' => $type,
    ]) ?>
</div>
