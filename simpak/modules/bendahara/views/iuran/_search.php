<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use simpak\models\RefIuran;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\bendahara\models\IaiRiwayatIuranSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="iai-riwayat-iuran-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'FULL_NAME')->widget(Select2::classname(), [
                'options' => ['multiple' => false, 'placeholder' => 'Cari nama anggota ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 3,
                    'language' => [
                        'errorLoading' => new JsExpression("function () { return 'Loading...'; }"),
                        'noResults' => new JsExpression("function () { 
                                    return 'Data tidak ditemukan !'
                                }")
                    ],
                    'ajax' => [
                        'url' => Url::to(['ajax-cari-anggota-nama']),
                        'dataType' => 'JSON',
                        'delay' => 500,
                        'data' => new JsExpression('function(params) { return {q:params.term}; }'),
                        'cache' => true
                    ],
                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                    'templateResult' => new JsExpression('function(response) { return response.text; }'),
                    'templateSelection' => new JsExpression('function (response) {return response.text;}'),
                ],
            ])->label('Nama Anggota') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'NAMA_IURAN')->widget(Select2::class, [
                'data' => RefIuran::map(
                    'NAMA_IURAN',
                    'NAMA_IURAN',
                ),
                'options' => ['placeholder' => '-- Pilih Jenis Iuran --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('Jenis Iuran') ?>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>