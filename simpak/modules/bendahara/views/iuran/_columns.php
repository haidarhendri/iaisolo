<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel simpak\modules\bendahara\models\IaiRiwayatIuranSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Lengkap',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NAMA_IURAN',
        'label' => $searchModel->getAttributeLabel('ID_JENIS_IURAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMINAL',
        'label' => $searchModel->getAttributeLabel('NOMINAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_BAYAR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_BAYAR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'STATUS_BAYAR',
        'format' => 'raw',
        'value' => function ($model) {
            switch ($model['STATUS_BAYAR']) {
                case 0:
                    return '<span class="label label-danger">Belum Terkonfirmasi Bayar</span>';
                case 1:
                    return '<span class="label label-success">Sudah Terkonfirmasi Bayar</span>';
            }
        },
        'label' => $searchModel->getAttributeLabel('STATUS_BAYAR'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Lihat',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $model['ID_TRANSAKSI']]);
        },
        'template' => '{file-preview}',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'buttons' => [
            'file-preview' => function ($url) {
                return Html::a('<i class="fa fa-eye"></i> Lihat File', $url, [
                    'id' => 'button-file-preview',
                    'role' => 'modal-remote',
                    'title' => 'Lihat',
                    'class' => 'btn btn-xs btn-info',
                    'data-toggle' => 'tooltip',
                    'data-pjax' => 0,
                ]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'visibleButtons' => [
            'update' => function ($model) {
                return ($model['STATUS_BAYAR'] == 0) ? true : false;
            },
            'delete' => function ($model) {
                return ($model['STATUS_BAYAR'] != 1) ? true : false;
            },
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-warning'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
