<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\bendahara\models\IaiRiwayatIuranSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Dashboard Pembayaran Iuran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-iuran-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-IAI-RIWAYAT-IURAN-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . 'Pencarian',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
        <div class="box-body search-collapse collapse in">
            <?= $this->render('_search', [
                'model' => $searchModel
            ]); ?>
        </div>
    </div>
    <!-- <div class="row">
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php // $jmlData['SETUJU'] ?></h3>
                    <p>Sudah Bayar</p>
                </div>
                <a href="?IaiRiwayatIuranSearch%5BSTATUS_BAYAR%5D=1" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php // $jmlData['TOLAK'] ?></h3>
                    <p>Belum Bayar</p>
                </div>
                <a href="?IaiRiwayatIuranSearch%5BSTATUS_BAYAR%5D=0" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div> -->
    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dpSearch,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    ['index'],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                )
                    . '{toggleData}'
                    . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar Riwayat Iuran',
            'before' => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . 'Tambah',
                ['create'],
                ['role' => 'modal-remote', 'class' => 'btn btn-success']
            ) . (empty(Yii::$app->request->queryParams) ? '   <span class="label label-danger">Data tidak ditemukan. Nama atau Jenis Iuran belum dipilih!</span>' : ''),
        ],
        'rowOptions' => function ($model) {
            if ($model['STATUS_BAYAR'] == 1) {
                return ['class' => 'success'];
            }
            if ($model['STATUS_BAYAR'] == 0) {
                return ['class' => 'danger'];
            }

            return [];
        },
    ]) ?>

</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    "size" => "modal-lg",
    'footer' => '',
]);
Modal::end();
?>