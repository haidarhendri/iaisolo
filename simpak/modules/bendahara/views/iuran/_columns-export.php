<?php

/* @var $searchModel simpak\modules\bendahara\models\IaiRiwayatIuranSearch */

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Lengkap',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMINAL',
        'label' => $searchModel->getAttributeLabel('NOMINAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_AWAL',
        'label' => $searchModel->getAttributeLabel('TANGGAL_AWAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_AKHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'STATUS_BAYAR',
        'label' => $searchModel->getAttributeLabel('STATUS_BAYAR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_BAYAR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_BAYAR'),
    ],
];
