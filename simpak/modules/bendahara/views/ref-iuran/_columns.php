<?php
use yii\helpers\Url;

/* @var $searchModel simpak\modules\bendahara\models\RefIuranSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NAMA_IURAN',
        'label' => $searchModel->getAttributeLabel('NAMA_IURAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('TGL_AWAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMINAL',
        'label' => $searchModel->getAttributeLabel('NOMINAL'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '100px',
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'visibleButtons' => [
            // 'update' => function ($model) {
            //     return false;
            // },
            'delete' => function ($model) {
                return false;
            },
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-warning'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
