<?php

/* @var $searchModel simpak\modules\bendahara\models\RefIuranSearch */

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NAMA_IURAN',
        'label' => $searchModel->getAttributeLabel('NAMA_IURAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('TGL_AWAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMINAL',
        'label' => $searchModel->getAttributeLabel('NOMINAL'),
    ],
];
