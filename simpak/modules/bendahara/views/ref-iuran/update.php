<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model simpak\models\RefIuran */

$this->title = 'Update Ref Iuran';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Iuran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-iuran-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
