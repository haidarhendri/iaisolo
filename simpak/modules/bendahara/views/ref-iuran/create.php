<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model simpak\models\RefIuran */

$this->title = 'Tambah Ref Iuran';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Iuran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-iuran-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
