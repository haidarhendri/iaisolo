<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model simpak\models\RefIuran */

$this->title = 'Ref Iuran';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Iuran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-iuran-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Ref Iuran' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID',
                    'NAMA_IURAN',
                    'TGL_AWAL',
                    'TGL_AKHIR',
                    'NOMINAL',
                ],
            ]) ?>

        </div>
    </div>
</div>
