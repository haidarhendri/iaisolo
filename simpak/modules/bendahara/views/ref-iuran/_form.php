<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\models\RefIuran */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="ref-iuran-form">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <h2 class="box-title text-uppercase"><?= 'Form Ref Iuran' ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'NAMA_IURAN')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'NOMINAL')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'TGL_AWAL')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'TGL_AKHIR')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
            </div>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton(
                        $model->isNewRecord ? 'Tambah' : 'Update',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    )
                    ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>