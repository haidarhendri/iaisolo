<?php

namespace simpak\modules\bendahara\controllers;

use Yii;
use simpak\models\RefIuran;
use simpak\modules\bendahara\models\RefIuranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * RefIuranController implements the CRUD actions for RefIuran model.
 */
class RefIuranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefIuran models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new RefIuranSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
        ]);
    }

    /**
     * Displays a single RefIuran model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Ref Iuran',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::a(
                        'Update',
                        ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                    ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new RefIuran model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new RefIuran();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Tambah Ref Iuran',
                    'content' => '<span class="text-success">' . 'Tambah ref iuran berhasil' . '</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            'Tambah Lagi',
                            ['create'],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => 'Tambah Ref Iuran',
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
            ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Tambah ref iuran berhasil.');
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RefIuran model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Update Ref Iuran',
                    'content' => '<span class="text-success">' . 'Update ref iuran berhasil' . '</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            'Update',
                            ['update', 'id' => $id],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => 'Update Ref Iuran',
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
                ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Update ref iuran berhasil');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Delete an existing RefIuran model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus ref iuranberhasil');
        return $this->redirect(['index']);
    }

     /**
     * Delete multiple existing RefIuran model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the RefIuran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefIuran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefIuran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
