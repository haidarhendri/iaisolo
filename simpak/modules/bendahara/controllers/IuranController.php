<?php

namespace simpak\modules\bendahara\controllers;

use Yii;
use common\models\IaiRiwayatIuran;
use common\models\IaiTransaksi;
use common\models\IaiTransaksiDokumen;
use common\models\User;
use Exception;
use simpak\modules\bendahara\models\IaiRiwayatIuranSearch;
use yii\base\Application;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * IuranController implements the CRUD actions for IaiRiwayatIuran model.
 */
class IuranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IaiRiwayatIuran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IaiRiwayatIuranSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);
        $jmlData = $searchModel->searchJumlah();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
            'jmlData' => $jmlData[0],
        ]);
    }

    /**
     * Displays a single IaiRiwayatIuran model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Riwayat Iuran',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                Html::button(
                    'Tutup',
                    ['class' => 'btn btn-default pull-right', 'data-dismiss' => 'modal']
                ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new IaiRiwayatIuran model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new IaiRiwayatIuran();
        $detail = new IaiTransaksiDokumen();
        // $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

        $transaksi = new IaiTransaksi([
            // 'ID_USER' => $userId,
            'URAIAN_KEGIATAN' => IaiRiwayatIuran::URAIAN_KEGIATAN,
        ]);

        $idJenisUpload = 5;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_PENDIDIKAN',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $transaksi->ID_USER = $model->ID_TRANSAKSI;
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $model->STATUS_BAYAR = 0;
                        $model->TANGGAL_BAYAR = date("Y-m-d");
                        $success = $model->save();
                    }
                    $dokumen = $detail->uploadDokumen($dokumen);
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Tambah Riwayat Iuran',
                            'content' => '<span class="text-success">' . 'Tambah Riwayat Iuran berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Tambah Lagi',
                                    ['create'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Tambah Riwayat Iuran',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Tambah Riwayat Iuran',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $transaksi->ID_USER = $model->ID_TRANSAKSI;
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $model->STATUS_BAYAR = 0;
                    $model->TANGGAL_BAYAR = date("Y-m-d");
                    $success = $model->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Tambah Data Iuran berhasil.');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Updates an existing IaiRiwayatIuran model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $transaksi = $model->idTransaksi;
        $detail = new IaiTransaksiDokumen();
        $idJenisUpload = 5;

        $modelDok = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $transaksi->ID]);
        $ext = ($modelDok->FILE_NAME) ? substr($modelDok->FILE_NAME, strrpos($modelDok->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_PENDIDIKAN',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            if (isset($dokumen['records'][$index])) {
                $form->setAttributes($dokumen['records'][$index]);
            }
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $success = $model->save();
                    }
                    $dokumen = $detail->uploadDokumen($dokumen);
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Update Riwayat Iuran',
                            'content' => '<span class="text-success">' . 'Update Riwayat Iuran berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Update',
                                    ['update', 'id' => $id],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Update Riwayat Iuran',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                            'modelDok' => $modelDok,
                            'type' => $type,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Update Riwayat Iuran',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                        'modelDok' => $modelDok,
                        'type' => $type,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $success = $model->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Update Riwayat Iuran berhasil');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('update', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                    'modelDok' => $modelDok,
                    'type' => $type,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dokumen' => $dokumen,
                'modelDok' => $modelDok,
                'type' => $type,
            ]);
        }
    }

    /**
     * Delete an existing IaiRiwayatIuran model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus Riwayat Iuranberhasil');
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple existing IaiRiwayatIuran model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the IaiRiwayatIuran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IaiRiwayatIuran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IaiRiwayatIuran::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFilePreview($id)
    {
        $request = Yii::$app->request;
        $model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $id]);

        $ext = ($model->FILE_NAME) ? substr($model->FILE_NAME, strrpos($model->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Preview File Upload',
                'content' => $this->renderAjax('file-preview', [
                    'model' => $model,
                    'type' => $type,
                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']),
            ];
        } else {
            return $this->render('file-preview', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }

    public function actionAjaxCariAnggota($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => [['id' => '', 'text' => '']]];

        if ($q !== null) {
            $models = User::find()
                ->select([
                    'ID AS id',
                    "CONCAT_WS(' - ', ID, FULL_NAME, NO_KTA) AS text",
                ])
                ->where([
                    'OR',
                    ['like', 'ID', $q],
                    ['like', 'FULL_NAME', $q],
                    ['like', 'NO_KTA', $q],
                ])
                ->limit(20)
                ->asArray()
                ->all();
            $out['results'] = $models;
        }

        return $out;
    }
    
    public function actionAjaxCariAnggotaNama($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => [['id' => '', 'text' => '']]];

        if ($q !== null) {
            $models = User::find()
                ->select([
                    'FULL_NAME AS id',
                    "CONCAT_WS(' - ', ID, FULL_NAME, NO_KTA) AS text",
                ])
                ->where([
                    'OR',
                    ['like', 'ID', $q],
                    ['like', 'FULL_NAME', $q],
                    ['like', 'NO_KTA', $q],
                ])
                ->limit(20)
                ->asArray()
                ->all();
            $out['results'] = $models;
        }

        return $out;
    }
}
