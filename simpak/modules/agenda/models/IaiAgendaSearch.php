<?php

namespace simpak\modules\agenda\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiAgenda;
use common\models\IaiTransaksiDokumen;

/**
 * IaiAgendaSearch represents the model behind the search form about `common\models\IaiAgenda`.
 */
class IaiAgendaSearch extends IaiAgenda
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [[
                'AGENDA_NAMA', 'AGENDA_JENIS', 'AGENDA_DESKRIPSI', 'AGENDA_TEMPAT', 'AGENDA_TGL_AWAL',
                'AGENDA_TGL_AKHIR', 'AGENDA_TGL_PELAKSANAAN', 'AGENDA_BIAYA', 'AGENDA_BIAYA_TERBILANG', 'AGENDA_KUOTA',
                'AGENDA_POIN', 'AGENDA_FOTO', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchPost($id)
    {
        $query = IaiAgenda::find()
            ->select([
                'A.*',
                'B.FILE_URL'
            ])
            ->from(['A' => IaiAgenda::tableName()])
            ->joinWith([
                'idDokumen' => function (ActiveQuery $q) {
                    return $q->from(['B' => IaiTransaksiDokumen::tableName()]);
                },
            ], false)
            ->where(['A.ID' => $id])->one();

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'AGENDA_TGL_AWAL' => $this->AGENDA_TGL_AWAL,
            'AGENDA_TGL_AKHIR' => $this->AGENDA_TGL_AKHIR,
            'AGENDA_TGL_PELAKSANAAN' => $this->AGENDA_TGL_PELAKSANAAN,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'AGENDA_NAMA', $this->AGENDA_NAMA])
            ->andFilterWhere(['like', 'AGENDA_JENIS', $this->AGENDA_JENIS])
            ->andFilterWhere(['like', 'AGENDA_DESKRIPSI', $this->AGENDA_DESKRIPSI])
            ->andFilterWhere(['like', 'AGENDA_TEMPAT', $this->AGENDA_TEMPAT])
            ->andFilterWhere(['like', 'AGENDA_BIAYA', $this->AGENDA_BIAYA])
            ->andFilterWhere(['like', 'AGENDA_BIAYA_TERBILANG', $this->AGENDA_BIAYA_TERBILANG])
            ->andFilterWhere(['like', 'AGENDA_KUOTA', $this->AGENDA_KUOTA])
            ->andFilterWhere(['like', 'AGENDA_POIN', $this->AGENDA_POIN])
            ->andFilterWhere(['like', 'AGENDA_FOTO', $this->AGENDA_FOTO])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'AGENDA_TGL_AWAL' => $this->AGENDA_TGL_AWAL,
            'AGENDA_TGL_AKHIR' => $this->AGENDA_TGL_AKHIR,
            'AGENDA_TGL_PELAKSANAAN' => $this->AGENDA_TGL_PELAKSANAAN,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'AGENDA_NAMA', $this->AGENDA_NAMA])
            ->andFilterWhere(['like', 'AGENDA_JENIS', $this->AGENDA_JENIS])
            ->andFilterWhere(['like', 'AGENDA_DESKRIPSI', $this->AGENDA_DESKRIPSI])
            ->andFilterWhere(['like', 'AGENDA_TEMPAT', $this->AGENDA_TEMPAT])
            ->andFilterWhere(['like', 'AGENDA_BIAYA', $this->AGENDA_BIAYA])
            ->andFilterWhere(['like', 'AGENDA_BIAYA_TERBILANG', $this->AGENDA_BIAYA_TERBILANG])
            ->andFilterWhere(['like', 'AGENDA_KUOTA', $this->AGENDA_KUOTA])
            ->andFilterWhere(['like', 'AGENDA_POIN', $this->AGENDA_POIN])
            ->andFilterWhere(['like', 'AGENDA_FOTO', $this->AGENDA_FOTO])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiAgenda::find();
    }
}
