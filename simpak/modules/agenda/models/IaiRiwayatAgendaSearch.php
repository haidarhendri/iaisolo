<?php

namespace simpak\modules\agenda\models;

use app\models\RefKorwil;
use common\models\IaiAgenda;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatAgenda;
use common\models\User;
use Yii;

/**
 * IaiRiwayatAgendaSearch represents the model behind the search form about `common\models\IaiRiwayatAgenda`.
 */
class IaiRiwayatAgendaSearch extends IaiRiwayatAgenda
{
    public $AGENDA_NAMA;
    public $AGENDA_JENIS;
    public $AGENDA_TEMPAT;
    public $AGENDA_TGL_PELAKSANAAN;
    public $AGENDA_TGL_AWAL;
    public $AGENDA_TGL_AKHIR;
    public $AGENDA_POIN;
    public $AGENDA_BIAYA;
    public $AGENDA_BIAYA_TERBILANG;
    public $FULL_NAME;
    public $KORWIL;

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'AGENDA_NAMA', 'AGENDA_JENIS', 'AGENDA_TEMPAT', 'FULL_NAME', 'AGENDA_TGL_PELAKSANAAN',
            'AGENDA_TGL_AWAL', 'AGENDA_TGL_AKHIR', 'AGENDA_POIN', 'AGENDA_BIAYA', 'KORWIL'
        ]);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'ID_KEGIATAN', 'IS_BAYAR', 'IS_KLAIM', 'IS_HADIR', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [[
                'AGENDA_NAMA', 'AGENDA_JENIS', 'AGENDA_TEMPAT', 'AGENDA_TGL_PELAKSANAAN', 'KORWIL',
                'AGENDA_TGL_AWAL', 'AGENDA_TGL_AKHIR', 'FULL_NAME', 'AGENDA_POIN', 'AGENDA_BIAYA',
                'AGENDA_BIAYA_TERBILANG', 'FILE_URL', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP'
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function searchOne($idKegiatan)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $query = static::query()
            ->select([
                'A.*',
                'B.AGENDA_NAMA',
                'B.AGENDA_JENIS',
                'B.AGENDA_TEMPAT',
                'B.AGENDA_TGL_PELAKSANAAN',
                'B.AGENDA_TGL_AWAL',
                'B.AGENDA_TGL_AKHIR',
                'B.AGENDA_POIN',
                'B.AGENDA_BIAYA',
                'B.AGENDA_BIAYA_TERBILANG',
                'B.ID AS ID_KEG',
                'C.ID AS ID_USER',
                'C.FULL_NAME'
            ])
            ->from(['A' => IaiRiwayatAgenda::tableName()])
            ->joinWith([
                'idAgenda' => function (ActiveQuery $q) {
                    return $q->from(['B' => IaiAgenda::tableName()]);
                },
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['C' => User::tableName()]);
                },
            ], false)
            ->where([
                'C.ID' => $identity->ID,
                'B.ID' => $idKegiatan
            ])->one();

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $query = static::query()
            ->select([
                'A.*',
                'B.AGENDA_NAMA',
                'B.AGENDA_JENIS',
                'B.AGENDA_TEMPAT',
                'B.AGENDA_TGL_PELAKSANAAN',
                'B.AGENDA_TGL_AWAL',
                'B.AGENDA_TGL_AKHIR',
                'B.AGENDA_POIN',
                'B.AGENDA_BIAYA',
                'B.AGENDA_BIAYA_TERBILANG',
                'B.ID AS ID_KEG',
                'C.ID AS ID_USER',
                'C.FULL_NAME',
                'C.NO_KTA',
                'C.GELAR_DEPAN',
                'C.GELAR_BELAKANG',
                'D.NAMA_WILAYAH AS KORWIL'
            ])
            ->from(['A' => IaiRiwayatAgenda::tableName()])
            ->joinWith([
                'idAgenda' => function (ActiveQuery $q) {
                    return $q->from(['B' => IaiAgenda::tableName()]);
                },
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['C' => User::tableName()]);
                },
                'idUser.idKorwil' => function (ActiveQuery $q) {
                    return $q->from(['D' => RefKorwil::tableName()]);
                },
            ], false)
            ->orderBy([
                'B.AGENDA_TGL_PELAKSANAAN' => SORT_DESC,
                'A.IS_BAYAR' => SORT_DESC,
                ]);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['C.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $this->load($params);

        if (empty($params)) {
            $query->andWhere('0 = 1');
        }
        
        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'A.ID_KEGIATAN' => $this->ID_KEGIATAN,
            'A.IS_BAYAR' => $this->IS_BAYAR,
            'A.IS_KLAIM' => $this->IS_KLAIM,
            'A.IS_HADIR' => $this->IS_HADIR,
            'C.KORWIL' => $this->KORWIL,
            'A.CREATE_BY' => $this->CREATE_BY,
            'A.CREATE_DATE' => $this->CREATE_DATE,
            'A.UPDATE_BY' => $this->UPDATE_BY,
            'A.UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'A.FILE_URL', $this->FILE_URL])
            ->andFilterWhere(['like', 'B.AGENDA_NAMA', $this->AGENDA_NAMA])
            ->andFilterWhere(['like', 'B.AGENDA_JENIS', $this->AGENDA_JENIS])
            ->andFilterWhere(['like', 'B.AGENDA_TEMPAT', $this->AGENDA_TEMPAT])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_PELAKSANAAN', $this->AGENDA_TGL_PELAKSANAAN])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_AWAL', $this->AGENDA_TGL_AWAL])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_AKHIR', $this->AGENDA_TGL_AKHIR])
            ->andFilterWhere(['like', 'B.AGENDA_POIN', $this->AGENDA_POIN])
            ->andFilterWhere(['like', 'B.AGENDA_BIAYA', $this->AGENDA_BIAYA])
            ->andFilterWhere(['like', 'B.AGENDA_BIAYA_TERBILANG', $this->AGENDA_BIAYA_TERBILANG])
            ->andFilterWhere(['like', 'C.FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'C.KORWIL', $this->KORWIL])
            ->andFilterWhere(['like', 'A.CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'A.UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.AGENDA_NAMA',
                'B.AGENDA_JENIS',
                'B.AGENDA_TEMPAT',
                'B.AGENDA_TGL_PELAKSANAAN',
                'B.AGENDA_TGL_AWAL',
                'B.AGENDA_TGL_AKHIR',
                'B.AGENDA_POIN',
                'B.AGENDA_BIAYA',
                'B.AGENDA_BIAYA_TERBILANG',
                'B.ID AS ID_KEG',
                'C.ID AS ID_USER',
                'C.FULL_NAME',
                'C.NO_KTA',
                'C.GELAR_DEPAN',
                'C.GELAR_BELAKANG',
                'D.NAMA_WILAYAH AS KORWIL'
            ])
            ->from(['A' => IaiRiwayatAgenda::tableName()])
            ->joinWith([
                'idAgenda' => function (ActiveQuery $q) {
                    return $q->from(['B' => IaiAgenda::tableName()]);
                },
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['C' => User::tableName()]);
                },
                'idUser.idKorwil' => function (ActiveQuery $q) {
                    return $q->from(['D' => RefKorwil::tableName()]);
                },
            ], false)
            ->orderBy(['B.AGENDA_TGL_PELAKSANAAN' => SORT_DESC]);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['C.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'A.ID_KEGIATAN' => $this->ID_KEGIATAN,
            'A.IS_BAYAR' => $this->IS_BAYAR,
            'A.IS_KLAIM' => $this->IS_KLAIM,
            'A.IS_HADIR' => $this->IS_HADIR,
            'C.KORWIL' => $this->KORWIL,
            'A.CREATE_BY' => $this->CREATE_BY,
            'A.CREATE_DATE' => $this->CREATE_DATE,
            'A.UPDATE_BY' => $this->UPDATE_BY,
            'A.UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'A.FILE_URL', $this->FILE_URL])
            ->andFilterWhere(['like', 'B.AGENDA_NAMA', $this->AGENDA_NAMA])
            ->andFilterWhere(['like', 'B.AGENDA_JENIS', $this->AGENDA_JENIS])
            ->andFilterWhere(['like', 'B.AGENDA_TEMPAT', $this->AGENDA_TEMPAT])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_PELAKSANAAN', $this->AGENDA_TGL_PELAKSANAAN])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_AWAL', $this->AGENDA_TGL_AWAL])
            ->andFilterWhere(['like', 'B.AGENDA_TGL_AKHIR', $this->AGENDA_TGL_AKHIR])
            ->andFilterWhere(['like', 'B.AGENDA_POIN', $this->AGENDA_POIN])
            ->andFilterWhere(['like', 'B.AGENDA_BIAYA', $this->AGENDA_BIAYA])
            ->andFilterWhere(['like', 'B.AGENDA_BIAYA_TERBILANG', $this->AGENDA_BIAYA_TERBILANG])
            ->andFilterWhere(['like', 'C.FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'C.KORWIL', $this->KORWIL])
            ->andFilterWhere(['like', 'A.CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'A.UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatAgenda::find();
    }

    public function searchCetakBukti($idTransaksi)
    {
        $query = static::query()
            ->select([
                'A.*',
                'B.AGENDA_NAMA',
                'B.AGENDA_JENIS',
                'B.AGENDA_TEMPAT',
                'B.AGENDA_TGL_PELAKSANAAN',
                'B.AGENDA_TGL_AWAL',
                'B.AGENDA_TGL_AKHIR',
                'B.AGENDA_POIN',
                'B.AGENDA_BIAYA',
                'B.AGENDA_BIAYA_TERBILANG',
                'B.ID AS ID_KEG',
                'B.UPDATE_DATE',
                'C.FULL_NAME'
            ])
            ->from(['A' => IaiRiwayatAgenda::tableName()])
            ->joinWith([
                'idAgenda' => function (ActiveQuery $q) {
                    return $q->from(['B' => IaiAgenda::tableName()]);
                },
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['C' => User::tableName()]);
                },
            ], false)
            ->where(['A.ID_TRANSAKSI' => $idTransaksi]);

        return $query->asArray()->one();
    }
}
