<?php

namespace simpak\modules\agenda\controllers;

use Yii;
use common\models\IaiAgenda;
use common\models\IaiRiwayatAgenda;
use common\models\IaiTransaksi;
use common\models\IaiTransaksiDokumen;
use Exception;
use simpak\modules\agenda\models\IaiAgendaSearch;
use simpak\modules\agenda\models\IaiRiwayatAgendaSearch;
use yii\base\Application;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * SiteController implements the CRUD actions for IaiAgenda model.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IaiAgenda models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IaiAgendaSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
        ]);
    }

    /**
     * Displays a single IaiAgenda model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Iai Agenda',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                Html::button(
                    'Tutup',
                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new IaiAgenda model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new IaiAgenda();
        $detail = new IaiTransaksiDokumen();
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

        $transaksi = new IaiTransaksi([
            'ID_USER' => $userId,
            'URAIAN_KEGIATAN' => IaiAgenda::URAIAN_KEGIATAN,
        ]);

        $idJenisUpload = 6;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_AGENDA',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $dokumen = $detail->uploadDokumen($dokumen);
                    }
                    if ($dokumen['success']) {
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $success = $model->save();
                    }
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Tambah Iai Agenda',
                            'content' => '<span class="text-success">' . 'Tambah iai agenda berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Tambah Lagi',
                                    ['create'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Tambah Iai Agenda',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Tambah IAI Agenda',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $success = $model->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Tambah IAI Agenda berhasil.');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Updates an existing IaiAgenda model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $transaksi = $model->idTransaksi;
        $detail = new IaiTransaksiDokumen();
        $idJenisUpload = 6;

        $modelDok = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $transaksi->ID]);
        $ext = ($modelDok->FILE_NAME) ? substr($modelDok->FILE_NAME, strrpos($modelDok->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_AGENDA',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            if (isset($dokumen['records'][$index])) {
                $form->setAttributes($dokumen['records'][$index]);
            }
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $success = $model->save();
                    }
                    $dokumen = $detail->uploadDokumen($dokumen);
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Update Iai Agenda',
                            'content' => '<span class="text-success">' . 'Update iai agenda berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Update',
                                    ['update', 'id' => $id],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Update Iai Agenda',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                            'modelDok' => $modelDok,
                            'type' => $type,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Update Iai Agenda',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                        'modelDok' => $modelDok,
                        'type' => $type,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $success = $model->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Update IAI Agenda berhasil');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('update', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Delete an existing IaiAgenda model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus iai agendaberhasil');
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple existing IaiAgenda model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the IaiAgenda model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IaiAgenda the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IaiAgenda::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionFilePreview($id)
    {
        $request = Yii::$app->request;
        $model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $id]);

        $ext = ($model->FILE_NAME) ? substr($model->FILE_NAME, strrpos($model->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Preview File Upload',
                'content' => $this->renderAjax('file-preview', [
                    'model' => $model,
                    'type' => $type,
                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']),
            ];
        } else {
            return $this->render('file-preview', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }

    public function actionLihat($id)
    {
        $searchModel = new IaiAgendaSearch();
        $model = $searchModel->searchPost($id);

        return $this->render('lihat', [
            'model' => $model,
        ]);
    }

    public function actionDaftar($id)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

        if(!$userId) {
            return $this->redirect(['/site/login']);
        }

        $searchModel = new IaiRiwayatAgendaSearch();
        $exist = $searchModel->searchOne($id);

        if (!$exist) {
            $transaksi = new IaiTransaksi([
                'ID_USER' => $userId,
                'URAIAN_KEGIATAN' => IaiRiwayatAgenda::URAIAN_KEGIATAN,
            ]);

            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($transaksi->save()) {
                    $model = new IaiRiwayatAgenda();
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $model->ID_KEGIATAN = $id;
                    $model->IS_BAYAR = 0;
                    $model->IS_KLAIM = 0;
                }
                if ($model->save()) {
                    $transaction->commit();
                    Yii::$app->session->setFlash('success', 'Pendaftaran Agenda berhasil.');
                    return $this->redirect(['/agenda/riwayat']);
                }
                throw new Exception('Error ketika minyimpan data. Hubungi Admin website!.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
            }
        }

        Yii::$app->session->setFlash('danger', 'Anda sudah terdaftar pada agenda ini.');
        return $this->redirect(['/agenda/riwayat']);
    }
}
