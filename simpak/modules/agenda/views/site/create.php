<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IaiAgenda */

$this->title = 'Tambah Iai Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-agenda-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
