<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->registerCss($this->render('_style.css'));
?>

<div class="box box-danger">
    <div class="box-body">
        <article class="post vt-post">
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4">
                    <div class="post-type post-img">
                        <a href="<?= $model->idDokumen->FILE_URL ?>"><img src="<?= $model->idDokumen->FILE_URL ?>" class="img-responsive" alt="Gambar Agenda"></a>
                    </div>
                    <div class="author-info author-info-2">
                        <ul class="list-inline">
                            <li>
                                <div class="info">
                                    <p>Diterbitkan pada :</p>
                                    <strong><?= Yii::$app->formatter->asDate($model->CREATE_DATE, 'php:d F Y'); ?></strong>
                                </div>
                            </li>
                            <li>
                                <!-- <a href="<?php Url::to(['/agenda/site/daftar', 'id' => $model->ID]); ?>"> -->
                                <button type="button" class="btn btn-block btn-lg btn-danger" data-toggle="modal" data-target="#modal-default" style="color:#ffffff">
                                    Daftar
                                </button>
                                <!-- </a> -->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8">
                    <div class="caption" maxlength="50">
                        <h3 class="md-heading"><b><?= $model->AGENDA_NAMA; ?></b></h3>
                        <hr>
                        <div class="box box-danger box-solid">
                            <div class="box-header with-border">
                                <h4 class="box-title">Detil Agenda</h4>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="box-body">
                                <dl class="dl-horizontal" style="font-size: 16px;">
                                    <dt>Tempat Pelaksanaan</dt>
                                    <dd><?= $model->AGENDA_TEMPAT; ?></dd><br>
                                    <dt>Tanggal Pelaksanaan</dt>
                                    <dd><?= Yii::$app->formatter->asDate($model->AGENDA_TGL_PELAKSANAAN, 'php:l, d F Y') ?></dd><br>
                                    <dt>Biaya</dt>
                                    <dd><?= Yii::$app->formatter->asCurrency($model->AGENDA_BIAYA, 'Rp ') ?></dd><br>
                                    <dt>Kuota</dt>
                                    <dd><?= $model->AGENDA_KUOTA; ?></dd><br>
                                    <dt>Poin</dt>
                                    <dd><?= $model->AGENDA_POIN; ?></dd>
                                </dl>
                            </div>
                        </div>
                        <hr>
                        <?= $model->AGENDA_DESKRIPSI ?>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div>

<div class="modal fade" id="modal-default" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Pendaftaran <?= $model->AGENDA_NAMA; ?></h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <dl class="dl-horizontal" style="font-size: 16px;">
                        <dt>Tempat Pelaksanaan</dt>
                        <dd><?= $model->AGENDA_TEMPAT; ?></dd><br>
                        <dt>Tanggal Pelaksanaan</dt>
                        <dd><?= Yii::$app->formatter->asDate($model->AGENDA_TGL_PELAKSANAAN, 'php:l, d F Y') ?></dd><br>
                        <dt>Biaya</dt>
                        <dd><?= Yii::$app->formatter->asCurrency($model->AGENDA_BIAYA, 'Rp ') ?></dd><br>
                        <dt>Kuota</dt>
                        <dd><?= $model->AGENDA_KUOTA; ?></dd><br>
                        <dt>Poin</dt>
                        <dd><?= $model->AGENDA_POIN; ?></dd>
                    </dl>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                <a class="btn btn-success" href="<?= Url::to(['/agenda/site/daftar', 'id' => $model->ID]); ?>" role="button">Daftar</a>
            </div>
        </div>
    </div>
</div>