<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\agenda\models\IaiAgendaSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="iai-agenda-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_TGL_AWAL')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_TGL_AKHIR')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_TEMPAT') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_NAMA') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_JENIS') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'AGENDA_BIAYA') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'AGENDA_KUOTA') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'AGENDA_POIN') ?>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>