<?php

/* @var $searchModel simpak\modules\agenda\models\IaiAgendaSearch */

return [
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'ID',
    // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_NAMA',
        'label' => $searchModel->getAttributeLabel('AGENDA_NAMA'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_JENIS',
        'label' => $searchModel->getAttributeLabel('AGENDA_JENIS'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_DESKRIPSI',
        'label' => $searchModel->getAttributeLabel('AGENDA_DESKRIPSI'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TEMPAT',
        'label' => $searchModel->getAttributeLabel('AGENDA_TEMPAT'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_PELAKSANAAN',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_PELAKSANAAN'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AWAL'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AKHIR'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_BIAYA',
        'label' => $searchModel->getAttributeLabel('AGENDA_BIAYA'),
        'value' => function ($model) {
            return Yii::$app->formatter->asCurrency($model['AGENDA_BIAYA'], 'Rp ');
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_KUOTA',
        'label' => $searchModel->getAttributeLabel('AGENDA_KUOTA'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_POIN',
        'label' => $searchModel->getAttributeLabel('AGENDA_POIN'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_FOTO',
        'label' => $searchModel->getAttributeLabel('AGENDA_FOTO'),
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'CREATE_BY',
    // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'CREATE_DATE',
    // 'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'UPDATE_BY',
    // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'UPDATE_DATE',
    // 'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'CREATE_IP',
    // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'UPDATE_IP',
    // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
