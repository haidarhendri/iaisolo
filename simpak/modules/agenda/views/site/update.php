<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiAgenda */

$this->title = 'Update Iai Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-agenda-update">
    <?= $this->render('_form-edit', [
        'model' => $model,
        'dokumen' => $dokumen,
        'modelDok' => $modelDok,
        'type' => $type,
    ]) ?>
</div>
