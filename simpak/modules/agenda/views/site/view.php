<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiAgenda */

$this->title = 'Iai Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-agenda-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Iai Agenda' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID',
                    'AGENDA_NAMA',
                    'AGENDA_JENIS',
                    'AGENDA_DESKRIPSI:ntext',
                    'AGENDA_TEMPAT',
                    'AGENDA_TGL_AWAL',
                    'AGENDA_TGL_AKHIR',
                    'AGENDA_BIAYA',
                    'AGENDA_KUOTA',
                    'AGENDA_POIN',
                    'AGENDA_FOTO',
                    'CREATE_BY',
                    'CREATE_DATE',
                    'UPDATE_BY',
                    'UPDATE_DATE',
                    'CREATE_IP',
                    'UPDATE_IP',
                ],
            ]) ?>

        </div>
    </div>
</div>
