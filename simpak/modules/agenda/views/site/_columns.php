<?php

use yii\helpers\Url;

/* @var $searchModel simpak\modules\agenda\models\IaiAgendaSearch */

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_NAMA',
        'label' => $searchModel->getAttributeLabel('AGENDA_NAMA'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_JENIS',
        'label' => $searchModel->getAttributeLabel('AGENDA_JENIS'),
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'AGENDA_DESKRIPSI',
    //     'format' => 'raw',
    //     'label' => $searchModel->getAttributeLabel('AGENDA_DESKRIPSI'),
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TEMPAT',
        'label' => $searchModel->getAttributeLabel('AGENDA_TEMPAT'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_PELAKSANAAN',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_PELAKSANAAN'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AWAL'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AKHIR'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_BIAYA',
        'label' => $searchModel->getAttributeLabel('AGENDA_BIAYA'),
        'value' => function ($model) {
            return Yii::$app->formatter->asCurrency($model['AGENDA_BIAYA'], 'Rp ');
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_KUOTA',
        'label' => $searchModel->getAttributeLabel('AGENDA_KUOTA'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_POIN',
        'label' => $searchModel->getAttributeLabel('AGENDA_POIN'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '70px',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'visibleButtons' => [
            'view' => function () {
                return true;
            },
            'update' => function () {
                return true;
            },
            'delete' => function () {
                return false;
            },
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-warning'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
