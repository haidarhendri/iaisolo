<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel simpak\modules\agenda\models\IaiRiwayatAgendaSearch */

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute' => 'ID',
    // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Peserta',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_NAMA',
        'label' => $searchModel->getAttributeLabel('AGENDA_NAMA'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_JENIS',
        'label' => $searchModel->getAttributeLabel('AGENDA_JENIS'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TEMPAT',
        'label' => $searchModel->getAttributeLabel('AGENDA_TEMPAT'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_PELAKSANAAN',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_PELAKSANAAN'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AWAL'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('AGENDA_TGL_AKHIR'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'KORWIL',
        'label' => $searchModel->getAttributeLabel('KORWIL'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'AGENDA_BIAYA',
        'label' => $searchModel->getAttributeLabel('AGENDA_BIAYA'),
        'value' => function ($model) {
            return Yii::$app->formatter->asCurrency($model['AGENDA_BIAYA'], 'Rp ');
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'IS_BAYAR',
        'format' => 'raw',
        'value' => function ($model) {
            switch ($model['IS_BAYAR']) {
                case 0:
                    return '<span class="label label-default">Belum Bayar</span>';
                case 1:
                    return '<span class="label label-warning">Menunggu Konfirmasi</span>';
                case 2:
                    return '<span class="label label-success">Sudah Bayar</span>';
                case 3:
                    return '<span class="label label-danger">Upload Ulang</span>';
            }
        },
        'label' => $searchModel->getAttributeLabel('IS_BAYAR'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'IS_HADIR',
        'format' => 'raw',
        'value' => function ($model) {
            switch ($model['IS_HADIR']) {
                case 0:
                    return '<span class="label label-default">Tidak Hadir</span>';
                case 1:
                    return '<span class="label label-success">Hadir</span>';
            }
        },
        'label' => $searchModel->getAttributeLabel('IS_HADIR'),
    ],
    // [
    //     'class' => '\kartik\grid\DataColumn',
    //     'attribute' => 'IS_KLAIM',
    //     'format' => 'raw',
    //     'value' => function ($model) {
    //         switch ($model['IS_KLAIM']) {
    //             case 0:
    //                 return '<span class="label label-default">Belum Diklaim</span>';
    //             case 1:
    //                 return '<span class="label label-warning">Menunggu Konfirmasi</span>';
    //             case 2:
    //                 return '<span class="label label-success">Sudah Diklaim</span>';
    //             case 3:
    //                 return '<span class="label label-success">Klaim Ditolak</span>';
    //         }
    //     },
    //     'label' => $searchModel->getAttributeLabel('IS_KLAIM'),
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Bukti Pembayaran',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $model['ID_TRANSAKSI']]);
        },
        'template' => '{file-preview}',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'buttons' => [
            'file-preview' => function ($url, $model) {
                if ($model['IS_BAYAR'] >= 1) {
                    return Html::a('<i class="fa fa-eye"></i> Lihat File', $url, [
                        'id' => 'button-file-preview',
                        'role' => 'modal-remote',
                        'title' => 'Lihat',
                        'class' => 'btn btn-xs btn-info',
                        'data-toggle' => 'tooltip',
                        'data-pjax' => 0,
                    ]);
                } else {
                    return Html::a('<i class="fa fa-eye"></i> Lihat File', $url, [
                        'id' => 'button-file-preview',
                        'role' => 'modal-remote',
                        'title' => 'Lihat',
                        'class' => 'btn btn-xs btn-default disabled',
                        'data-toggle' => 'tooltip',
                        'data-pjax' => 0,
                    ]);
                }
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'visibleButtons' => [
            'view' => function () {
                return false;
            },
            'update' => function ($model) {
                if ($model['IS_BAYAR'] == 2) {
                    return false;
                } else {
                    return true;
                }
            },
            'delete' => function () {
                return false;
            },
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'label' => 'Verifikasi <br> Bukti Pembayaran',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
