<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatAgenda */

$this->title = 'Pembayaran Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pembayaran Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-Pembayaran-agenda-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Pembayaran Agenda' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'AGENDA_NAMA',
                    'AGENDA_JENIS',
                    'AGENDA_TEMPAT',
                    'AGENDA_TGL_PELAKSANAAN',
                    'AGENDA_TGL_AWAL',
                    'AGENDA_TGL_AKHIR',
                    'AGENDA_POIN',
                    'AGENDA_BIAYA',
                ],
            ]) ?>

        </div>
    </div>
</div>
