<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IaiPembayaranAgenda */

$this->title = 'Tambah Iai Pembayaran Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pembayaran Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-Pembayaran-agenda-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
