<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\agenda\models\IaiRiwayatAgendaSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Dashboard Pembayaran Agenda';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-Pembayaran-agenda-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-IAI-Pembayaran-AGENDA-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
    </div>
    
    <?= $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dpSearch,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    ['index'],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                )
                    . '{toggleData}'
                    . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar Pembayaran Agenda',
            'before' => empty(Yii::$app->request->queryParams) ? '<span class="label label-danger">Data tidak ditemukan. Agenda belum dipilih!</span>' : '',
            'after' => BulkButtonWidget::widget([
                'buttons' => Html::a(
                    '<i class="glyphicon glyphicon-check"></i> ' . 'Tandai sebagai hadir',
                    ['bulk-delete'],
                    [
                        'class' => 'btn btn-success btn-xs',
                        'role' => 'modal-remote-bulk',
                        'data-confirm' => false,
                        'data-method' => false,
                        'data-request-method' => 'post',
                        'data-confirm-title' => 'Apakah Anda Yakin?',
                        'data-confirm-message' => 'Apakah anda yakin untuk menandai kehadiran ini',
                    ]
                ),
            ]),
        ],
        'rowOptions' => function ($model) {
            if ($model['IS_BAYAR'] == 2) {
            //     return [];
            // } 
            // elseif ($model['IS_BAYAR'] == 1) {
            //     return ['class' => 'warning'];
            // } elseif ($model['IS_BAYAR'] == 2) {
                return ['class' => 'success'];
            // } elseif ($model['IS_BAYAR'] == 3) {
            //     return ['class' => 'danger'];
            }

            return [];
        },
    ]) ?>

</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    "size" => "modal-lg",
    'footer' => '',
]);
Modal::end();
?>