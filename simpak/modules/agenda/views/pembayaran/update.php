<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatAgenda */

$this->title = 'Update Pembayaran Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pembayaran Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-Pembayaran-agenda-update">
    <?= $this->render('_form-edit', [
        'model' => $model,
        'dokumen' => $dokumen,
        'modelDok' => $modelDok,
        'type' => $type,
    ]) ?>
</div>
