<div class="file-preview">
    <?php if ($type === 'image') : ?>
        <img src="<?= $model->FILE_URL ?>" class="img-responsive" alt="File Tidak Tersedia">
    <?php else : ?>
        <iframe src="<?= $model->FILE_URL ?>" style="width: 100%; height:700px; border:0;" allowfullscreen>
        <?php endif; ?>
</div>