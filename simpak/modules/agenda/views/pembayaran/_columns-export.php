<?php

/* @var $searchModel simpak\modules\agenda\models\IaiRiwayatAgendaSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'NO_KTA',
        'label' => 'Nomor Anggota',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Lengkap',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'GELAR_DEPAN',
        'label' => 'Gelar Depan',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'GELAR_BELAKANG',
        'label' => 'Gelar Belakang',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'KORWIL',
        'label' => 'Asal PC',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'IS_BAYAR',
        'label' => 'Status Bayar',
        'value' => function ($model) {
            switch ($model['IS_BAYAR']) {
                case 0:
                    return 'Belum Bayar';
                case 1:
                    return 'Menunggu Konfirmasi';
                case 2:
                    return 'Sudah Bayar';
                case 2:
                    return 'Upload Ulang';
            }
        },
    ],
];
