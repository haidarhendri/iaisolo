<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatAgenda */

$this->title = 'Update Iai Riwayat Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-agenda-update">
    <?= $this->render('_form-edit', [
        'model' => $model,
        'dokumen' => $dokumen,
        // 'modelDok' => $modelDok,
        // 'type' => $type,
    ]) ?>
</div>
