<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <table style="width: 100%">
        <tr style="border: 4px solid black;">
            <td style="padding: 15px; border-left:4px solid black;border-top:4px solid black;border-bottom:4px solid black;">
                <img src="<?= Yii::getAlias('@web/img/iai-logo-mini.jpg') ?>" height="100" style="
                    display: block;
                    margin-left: auto;
                    margin-right: auto;" alt="IAI-SOLO - Sistem Informasi Manajemen Keanggotaan ">
            </td>
            <td valign="top" style="padding: 15px;border-right:4px solid black;border-top:4px solid black;border-bottom:4px solid black;">
                <h4 style="font-family:Arial;"><b>Pengurus Cabang Surakarta</b></h4>
                <h3 style="font-family:Arial;"><b>IKATAN APOTEKER INDONESIA</b></h3>
                <h5 style="font-family:Arial;"><b>Sekretariat :</b><?= $setting['ALAMAT_SEKRETARIAT'] ?></h5>
                <h5 style="font-family:Arial;">
                    Email : <a href="mailto:iai.pcsurakarta@gmail.com"><?= $setting['EMAIL'] ?></a>
                    web: <a href="https://www.iaisolo.id"><?= $setting['WEB'] ?></a>
                </h5>
            </td>
        </tr>
    </table>
    <br>
    <table style="width: 100%">
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 20%; font-size:10pt;"><p>Telah terima dari</p></td>
            <td valign="top" style="width: 1%;"><p>:</p></td>
            <td valign="top" style="width: 74%; font-size:10pt;"><p><?= $model['FULL_NAME'] ?></p></td>
        </tr>
        <tr>
            <td valign="top"></td>
            <td valign="top" style="font-size:10pt;"><p>Sejumlah</p></td>
            <td valign="top"><p>:</p></td>
            <td valign="top" style="font-size:10pt;"><p><?= $terbilang ?></p></td>
        </tr>
        <tr>
            <td valign="top"></td>
            <td valign="top" style="font-size:10pt;"><p>Untuk pembayaran</p></td>
            <td valign="top"><p>:</p></td>
            <td valign="top" style="font-size:10pt;"><p><?= $model['AGENDA_NAMA'] ?></p></td>
        </tr>
    </table>
    <br>
    <table style="width: 100%">
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 28%;"></td>
            <td valign="top" style="width: 33%;"></td>
            <td valign="top" style="width: 34%;text-align: center;font-size:10pt;">Surakarta, <?= Yii::$app->formatter->asDate($model['UPDATE_DATE'], 'php:d F Y') ?></td>
        </tr>
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 28%;"></td>
            <td valign="top" style="width: 33%;"></td>
            <td valign="top" style="width: 34%;text-align: center;font-size:10pt;"><b>Bendahara</b></td>
        </tr>
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 28%;"></td>
            <td valign="top" style="width: 33%;"></td>
            <td valign="top" style="width: 34%;text-align: center;font-size:10pt;"><b>IAI PC SURAKARTA</b></td>
        </tr>
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 28%; border: 2px solid black; padding: 5px;font-size:10pt;">
            <b><?= Yii::$app->formatter->asCurrency($model['AGENDA_BIAYA'], 'Rp ') ?></b></td>
            <td valign="top" style="width: 33%;"></td>
            <td valign="top" style="width: 34%;"></td>
        </tr>
        <tr>
            <td valign="top" style="width: 5%;"></td>
            <td valign="top" style="width: 28%;"></td>
            <td valign="top" style="width: 33%;"></td>
            <td valign="top" style="width: 34%;text-align: center;font-size:10pt;"><?= $setting['NAMA_BENDAHARA'] ?></td>
        </tr>
    </table>
</body>

</html>