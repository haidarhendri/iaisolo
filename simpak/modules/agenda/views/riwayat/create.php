<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatAgenda */

$this->title = 'Tambah Iai Riwayat Agenda';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Agenda', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-agenda-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
