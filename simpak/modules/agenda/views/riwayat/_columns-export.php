<?php

/* @var $searchModel simpak\modules\agenda\models\IaiRiwayatAgendaSearch */

return [
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_TRANSAKSI',
        'label' => $searchModel->getAttributeLabel('ID_TRANSAKSI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_KEGIATAN',
        'label' => $searchModel->getAttributeLabel('ID_KEGIATAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'IS_KLAIM',
        'label' => $searchModel->getAttributeLabel('IS_KLAIM'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FILE_URL',
        'label' => $searchModel->getAttributeLabel('FILE_URL'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
