<?php

use app\models\RefKorwil;
use common\models\IaiAgenda;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $searchModel simpak\modules\kalkulasi\models\PakTransaksiSearch */
/* @var $user common\models\User */
?>
<div class="box box-danger">
    <div class="box-header" style="padding-bottom: 0">
        <i class="fa fa-search"></i> Filter Data Agenda :
    </div>
    <div class="box-body">
        <div class="pembayaran-agenda-search">
            <?php $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); ?>

            <div class="row">
                <div class="col-md-9">
                    <?= $form->field($model, 'ID_KEGIATAN')->widget(Select2::class, [
                        'data' => IaiAgenda::map(
                            'ID',
                            'AGENDA_NAMA',
                        ),
                        'options' => ['placeholder' => '-- Pilih Agenda --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Agenda') ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'KORWIL')->widget(Select2::class, [
                        'data' => RefKorwil::map(
                            'ID',
                            'NAMA_WILAYAH',
                        ),
                        'options' => ['placeholder' => '-- Pilih Korwil --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Korwil') ?>
                </div>
            </div>

            <?= Html::submitButton('<i class="fa fa-cog"></i> Proses', ['class' => 'btn btn-primary']) ?>

            <?= Html::a(
                '<i class="glyphicon glyphicon-repeat"></i> Reset',
                [''],
                ['class' => 'btn btn-primary']
            ) ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>