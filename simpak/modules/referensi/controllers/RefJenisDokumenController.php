<?php

namespace simpak\modules\referensi\controllers;

use Yii;
use common\models\referensi\RefJenisDokumen;
use simpak\modules\referensi\models\RefJenisDokumenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * RefJenisDokumenController implements the CRUD actions for RefJenisDokumen model.
 */
class RefJenisDokumenController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefJenisDokumen models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new RefJenisDokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single RefJenisDokumen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Referensi Jenis Dokumen #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    ) . Html::a(
                        'Edit',
                        ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role'=>'modal-remote']
                    ),
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new RefJenisDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new RefJenisDokumen();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $model->CREATE_DATE = new \yii\db\Expression('NOW()');
                $model->CREATE_BY = Yii::$app->user->id;
                $model->CREATE_IP = Yii::$app->getRequest()->getUserIP();
                $model->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Tambah Referensi Jenis Dokumen',
                    'content' => '<span class="text-success">Data Berhasil Ditambahkan...</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        ) . Html::a(
                            'Tambah Kembali',
                            ['create'],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];         
            } else {
                return [
                    'title' => 'Tambah Referensi Jenis Dokumen',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left','data-dismiss' => 'modal']
                        ) . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];         
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $model->CREATE_DATE = new \yii\db\Expression('NOW()');
                $model->CREATE_BY = Yii::$app->user->id;
                $model->CREATE_IP = Yii::$app->getRequest()->getUserIP();
                $model->save();
                Yii::$app->session->setFlash('success', 'Data Berhasil Ditambahkan...');
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing RefJenisDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->load($request->post()) && $model->save()) {
                $model->UPDATE_DATE = new \yii\db\Expression('NOW()');
                $model->UPDATE_BY = Yii::$app->user->id;
                $model->UPDATE_IP = Yii::$app->getRequest()->getUserIP();
                $model->save();
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title' => 'Update Referensi Jenis Dokumen #' . $id,
                    'content' => '<span class="text-success">Data Berhasil Diupdate...</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        ) . Html::a(
                            'Edit',
                            ['update', 'id' => $id],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];    
            } else {
                return [
                    'title' => 'Update Referensi Jenis Dokumen #' . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        ) . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];        
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                $model->UPDATE_DATE = new \yii\db\Expression('NOW()');
                $model->UPDATE_BY = Yii::$app->user->id;
                $model->UPDATE_IP = Yii::$app->getRequest()->getUserIP();
                $model->save();
                Yii::$app->session->setFlash('success', 'Data Berhasil Diupdate...');
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing RefJenisDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }


    /**
     * Finds the RefJenisDokumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefJenisDokumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefJenisDokumen::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Halaman yang diminta tidak tersedia.');
        }
    }
}
