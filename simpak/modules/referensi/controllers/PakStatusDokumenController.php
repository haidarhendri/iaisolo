<?php

namespace simpak\modules\referensi\controllers;

use Yii;
use common\models\referensi\RefStatusDokumen;
use common\models\referensi\searches\RefStatusDokumenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * PakStatusDokumenController implements the CRUD actions for RefStatusDokumen model.
 */
class PakStatusDokumenController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefStatusDokumen models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RefStatusDokumenSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        // $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dataProvider' => $dataProvider,
            // 'dpExport' => $dpExport,
        ]);
    }

    /**
     * Displays a single RefStatusDokumen model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Ref Status Dokumen',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::a(
                        'Update',
                        ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                    ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new RefStatusDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new RefStatusDokumen();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Tambah Ref Status Dokumen',
                    'content' => '<span class="text-success">' . 'Tambah ref status dokuman berhasil' . '</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            'Tambah Lagi',
                            ['create'],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => 'Tambah Ref Status Dokumen',
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
            ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Tambah ref status dokuman berhasil.');
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefStatusDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => 'Update Ref Status Dokumen',
                    'content' => '<span class="text-success">' . 'Update ref status dokuman berhasil' . '</span>',
                    'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            'Update',
                            ['update', 'id' => $id],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => 'Update Ref Status Dokumen',
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
            ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Update ref status dokuman berhasil');
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Delete an existing RefStatusDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus ref status dokumanberhasil');
        return $this->redirect(['index']);
    }

     /**
     * Delete multiple existing RefStatusDokumen model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the RefStatusDokumen model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefStatusDokumen the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefStatusDokumen::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
