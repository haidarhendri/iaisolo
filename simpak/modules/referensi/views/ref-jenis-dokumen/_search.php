<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\referensi\models\RefPakUnsurSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="ref-jenis-dokumen-search" class="ref-jenis-dokumen-search collapse" style="margin-top: 15px">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'ID') ?>

        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'JENIS_DOKUMEN') ?>

        </div>
    </div>
    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-cog"></i> Proses', ['class' => 'btn btn-primary']); ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-repeat"></i> Reset',
            ['index'],
            ['class' => 'btn btn-primary']
        ); ?>

    </div>
    <?php ActiveForm::end(); ?>

</div>
