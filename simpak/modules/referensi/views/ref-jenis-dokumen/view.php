<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefJenisDokumen */
$this->title = 'Detil Referensi Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Referensi Jenis Dokumen', 'url' => ['index']];
?>
<div class="ref-jenis-dokumen-view">
    <div class="box box-danger">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'JENIS_DOKUMEN',
                'CREATE_DATE',
                'UPDATE_DATE'
            ],
        ]) ?>

    </div>
</div>
