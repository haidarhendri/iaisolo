<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefJenisDokumen */
$this->title = 'Update Referensi Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Referensi Jenis Dokumen', 'url' => ['index']];
?>
<div class="ref-jenis-dokumen-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
