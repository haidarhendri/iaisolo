<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\referensi\models\RefJenisDokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Referensi Jenis Dokumen';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="ref-jenis-dokumen-index">
    <div class="box box-danger">
        <div class="box-body">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dataProvider,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dataProvider->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-REF-JENIS-DOKUMEN-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> Pencarian Lanjut',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '#ref-jenis-dokumen-search',
                    ]
                ); ?>

                <?= Html::a(
                    '<i class="glyphicon fa fa-list-ul"></i> Referensi',
                    ['/referensi/default/index'],
                    ['class' => 'btn btn-warning back-button']
                ); ?>

            </div>

            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'toolbar' => [
                [
                    'content' => Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index'],
                            [
                                'data-pjax' => 1,
                                'class' => 'btn btn-default',
                                'title' => 'Reset Grid',
                            ]
                        ) . '{toggleData} {export}',
                ],
            ],
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i>',
                'before' => Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> Tambah Data',
                    ['create'],
                    ['role' => 'modal-remote', 'title' => 'Tambah Data', 'class' => 'btn btn-success']
                ),
                'after' => Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                        ['index'],
                        ['class' => 'btn btn-primary btn-xs']
                    )
                    . '<div class="clearfix"></div>',
            ],
        ]) ?>
    </div>
</div>
<?php 
Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",
    "size"=> "modal-lg",
]);
Modal::end();
?>
