<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->title = 'Dashboard Pengaturan Kode Referensi';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('.box-body .wrap{margin-top: -15px} .box-body .btn-social{margin-top: 15px}')
?>
<div class="box box-solid box-danger">
    <div class="box-header box-solid with-border">
        <h3 class="box-title">Pengaturan Kode Referensi Dokumen</h3>
    </div>
    <div class="box-body">
        <div class="wrap">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <?= Html::a(
                        '<i class="fa fa-bars text-white"></i> Referensi Jenis Upload',
                        ['/referensi/ref-jenis-upload'],
                        ['class' => 'btn btn-block btn-social btn-danger']
                    ) ?>
                    <?= Html::a(
                        '<i class="fa fa-bars text-white"></i> Config Jenis Dokumen',
                        ['/referensi/cfg-jenis-dokumen'],
                        ['class' => 'btn btn-block btn-social btn-danger']
                    ) ?>
                </div>
                <div class="col-md-6 col-sm-6">
                    <?= Html::a(
                        '<i class="fa fa-bars text-white"></i> Referensi Jenis Dokumen',
                        ['/referensi/ref-jenis-dokumen'],
                        ['class' => 'btn btn-block btn-social btn-danger']
                    ) ?>
                    <?= Html::a(
                        '<i class="fa fa-bars text-white"></i> Referensi Status Dokumen',
                        ['/referensi/pak-status-dokumen'],
                        ['class' => 'btn btn-block btn-social btn-danger']
                    ) ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>