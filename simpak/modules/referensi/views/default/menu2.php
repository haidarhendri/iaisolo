<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->title = 'Dashboard Pengaturan Kode Referensi';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss($this->render('style.css'))
?>

<div class="box box-solid box-default">
    <div class="box-body" style="padding-bottom:30px;">
          <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-pills nav-justified">
				<li class="tab-1 active"><a aria-expanded="false" href="#tab_1" data-toggle="tab">FAKULTAS</a></li>
				<li class="tab-1"><a aria-expanded="true" href="#tab_2" data-toggle="tab">UNIVERSITAS</a></li>
            </ul>
            <div class="tab-content">
				<div class="active tab-pane " id="tab_1">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>1.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Form DUPAK</h4>
									<span>Mengisi & Membuat Form DUPAK</span>
								</div>
								<div class="media-right"><i class="fa fa-calculator"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>2.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Surat Pengantar Kaprodi</h4>
									<span>Membuat Surat Pengantar ke Kaprodi</span>
								</div>
								<div class="media-right"><i class="fa fa-graduation-cap"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>3.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Surat Pengantar Fakultas</h4>
									<span>Membuat surat pengantar ke fakultas</span>
								</div>
								<div class="media-right"><i class="fa fa-pencil"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>4.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Undangan Tim PAK</h4>
									<span>Membuat undangan kepada tim PAK</span>
								</div>
								<div class="media-right"><i class="fa fa-group"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>5.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Review DUPAK</h4>
									<span>Form review DUPAK dosen</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>6.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Undangan Rapat Senat</h4>
									<span>Membuat undangan rapat senat</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>7.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Surat Pengantar Dosen</h4>
									<span>Membuat surat pengantar ke dosen terkait penolakan/revisi</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>8.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Form Rapat Pleno Senat</h4>
									<span>Form rapat pleno senat fakultas</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>9.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Syarat Administrasi</h4>
									<span>Form ceklist syarat administrasi</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>10.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Syarat Administrasi</h4>
									<span>Form ceklist syarat administrasi</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>11.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Syarat Administrasi</h4>
									<span>Form ceklist syarat administrasi</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="tab_2">
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>1.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Undangan Rapat Tim PAK</h4>
									<span>Undangan rapat tim PAK Universitas</span>
								</div>
								<div class="media-right"><i class="fa fa-calculator"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>2.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Blangko Penilaian Angka Kredit</h4>
									<span>Membuat blangko penilaian angka kredit</span>
								</div>
								<div class="media-right"><i class="fa fa-graduation-cap"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>3.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Form Pengembalian Berkas</h4>
									<span>Cetak form berkas pengembalian</span>
								</div>
								<div class="media-right"><i class="fa fa-pencil"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>4.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Draf SK PAK Jabatan</h4>
									<span>Membuat draf SK PAK jabatan</span>
								</div>
								<div class="media-right"><i class="fa fa-group"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>5.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Hasil Rapat Pleno</h4>
									<span>Cetak Hasil rapat pleno ke kepegawaian pusat</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>6.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Form Berkas Kemenristek Dikti</h4>
									<span>Cetak form berkas ke Kemenristek Dikti</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<a href="#">
							<div class="media reg-box">
								<div class="media-left"><span>7.</span></div>
								<div class="media-body">
									<h4 class="media-heading">Form Penerbitan SK Kenaikan Jabatan</h4>
									<span>Cetak form penerbitan SK kenaikan jabatan lektor kepala/guru besar</span>
								</div>
								<div class="media-right"><i class="fa fa-briefcase"></i> </div>
							</div>
						</a>
					</div>
				</div>
            </div>
            <!-- /.tab-content -->
        </div>
    </div>
</div>
