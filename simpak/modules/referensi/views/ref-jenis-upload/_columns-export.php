<?php

/* @var $searchModel simpak\modules\referensi\models\RefJenisUploadSearch */

return [
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENIS_UPLOAD',
        'label' => $searchModel->getAttributeLabel('JENIS_UPLOAD'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'CREATE_DATE',
        'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'UPDATE_DATE',
        'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
