<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefJenisUpload */

$this->title = 'Ref Jenis Upload';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Jenis Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-jenis-upload-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Ref Jenis Upload' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'ID',
                    'JENIS_UPLOAD',
                    'CREATE_DATE',
                    // 'CREATE_BY',
                    // 'CREATE_IP',
                    'UPDATE_DATE',
                    // 'UPDATE_BY',
                    // 'UPDATE_IP',
                ],
            ]) ?>

        </div>
    </div>
</div>
