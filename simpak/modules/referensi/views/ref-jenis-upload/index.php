<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\referensi\models\RefJenisUploadSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Dashboard Referensi Jenis Upload';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-jenis-upload-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-REF-JENIS-UPLOAD-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . 'Pencarian Lanjut',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= Html::a(
                    '<i class="glyphicon fa fa-list-ul"></i> Referensi',
                    ['/referensi/default/index'],
                    ['class' => 'btn btn-warning back-button']
                ); ?>

            </div>
            <?= $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    </div>
    <!-- <div class="box box-danger"> -->
    <div class="ajaxCrudDatatable">
        <?= GridView::widget([
            'id' => 'crud-datatable',
            'dataProvider' => $dpSearch,
            'pjax' => true,
            'columns' => require(__DIR__ . '/_columns.php'),
            'toolbar' => [
                [
                    'content' =>
                    Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        ['index'],
                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                    )
                        . '{toggleData}'
                        . '{export}',
                ],
            ],
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => 'default',
                'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar Ref Jenis Upload',
                'before' =>
                Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> ' . 'Tambah Data',
                    ['create'],
                    ['role' => 'modal-remote', 'class' => 'btn btn-success']
                ),
                'after' => BulkButtonWidget::widget([
                    'buttons' => Html::a(
                        '<i class="glyphicon glyphicon-trash"></i> ' . 'Delete All',
                        ['bulk-delete'],
                        [
                            'class' => 'btn btn-danger btn-xs',
                            'role' => 'modal-remote-bulk',
                            'data-confirm' => false,
                            'data-method' => false,
                            'data-request-method' => 'post',
                            'data-confirm-title' => 'Apakah Anda Yakin?',
                            'data-confirm-message' => 'Apakah anda yakin untuk menghapus item ini',
                        ]
                    ),
                ])
                    . '<div class="clearfix"></div>',
            ],
        ]) ?>

    </div>
    <!-- </div> -->
</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    'footer' => '',
]);
Modal::end();
?>