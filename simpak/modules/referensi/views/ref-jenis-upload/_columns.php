<?php
use yii\helpers\Url;

/* @var $searchModel simpak\modules\referensi\models\RefJenisUploadSearch */

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENIS_UPLOAD',
        'label' => $searchModel->getAttributeLabel('JENIS_UPLOAD'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'CREATE_DATE',
        'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'UPDATE_DATE',
        'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '60px',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Detail', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
        ],
    ],
];
