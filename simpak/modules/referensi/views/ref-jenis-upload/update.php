<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefJenisUpload */

$this->title = 'Update Ref Jenis Upload';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Jenis Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-jenis-upload-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
