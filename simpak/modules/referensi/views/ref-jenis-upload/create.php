<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefJenisUpload */

$this->title = 'Tambah Ref Jenis Upload';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Referensi Jenis Upload', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-jenis-upload-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
