<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\referensi\models\RefJenisUploadSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss('.search-form{margin-bottom: 20px; padding-bottom: 5px; border-bottom: 1px solid #f4f4f4;}');
?>
<div class="ref-jenis-upload-search search-form search-collapse collapse">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'JENIS_UPLOAD') ?>

    <div class="form-group">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
