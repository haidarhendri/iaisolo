<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\CfgJenisDokumen */

$this->title = 'Konfig Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Konfig Jenis Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cfg-jenis-dokumen-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Cfg Jenis Dokumen' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'idJenisUpload.JENIS_UPLOAD',
                    'idJenisDokumen.JENIS_DOKUMEN',
                    'ID_TIPE_DOKUMEN',
                    'IS_REQUIRED',
                ],
            ]) ?>

        </div>
    </div>
</div>
