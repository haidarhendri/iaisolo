<?php

/* @var $searchModel simpak\modules\referensi\models\CfgJenisDokumenSearch */

return [
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_JENIS_UPLOAD',
        'label' => $searchModel->getAttributeLabel('ID_JENIS_UPLOAD'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_JENIS_DOKUMEN',
        'label' => $searchModel->getAttributeLabel('ID_JENIS_DOKUMEN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_TIPE_DOKUMEN',
        'label' => $searchModel->getAttributeLabel('ID_TIPE_DOKUMEN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'IS_REQUIRED',
        'label' => $searchModel->getAttributeLabel('IS_REQUIRED'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
