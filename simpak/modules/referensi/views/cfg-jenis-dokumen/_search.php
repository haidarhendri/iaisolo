<?php

use common\models\referensi\RefJenisDokumen;
use common\models\referensi\RefJenisUpload;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\referensi\models\CfgJenisDokumenSearch */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss('.search-form{margin-bottom: 20px; padding-bottom: 5px; border-bottom: 1px solid #f4f4f4;}');
?>
<div class="cfg-jenis-dokumen-search search-form search-collapse collapse">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <div class="row">
        <div class="col-sm-6">
            <?= $form->field($model, 'ID_JENIS_UPLOAD')->widget(Select2::className(), [
                'data' => RefJenisUpload::map(),
                'options' => ['id' => 'fi-id_jenis_upload_search', 'placeholder' => '-- Pilih --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('Jenis Upload') ?>

        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'ID_JENIS_DOKUMEN')->widget(Select2::className(), [
                'data' => RefJenisDokumen::map(),
                'options' => ['id' => 'fi-id_jenis_dokumen_search', 'placeholder' => '-- Pilih --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('Jenis Dokumen') ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'ID_TIPE_DOKUMEN')->widget(Select2::classname(), [
                'data' => [
                    1 => 'No Upload',
                    2 => 'Upload',
                    3 => 'Sinkron'
                ],
                'options' => ['placeholder' => 'Pilih tipe dokumen ...', 'id' => 'fi-tipe_dokumen'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-sm-2">
            <?= $form->field($model, 'IS_REQUIRED')->widget(Select2::classname(), [
                'data' => [
                    0 => 'Tidak',
                    1 => 'Ya'
                ],
                'options' => ['placeholder' => 'Pilih ...', 'id' => 'fi-is_required_search'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
    </div>



    <div class="form-group">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>