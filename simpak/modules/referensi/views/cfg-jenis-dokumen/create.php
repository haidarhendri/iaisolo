<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\referensi\CfgJenisDokumen */

$this->title = 'Tambah Cfg Jenis Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Cfg Jenis Dokumen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cfg-jenis-dokumen-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
