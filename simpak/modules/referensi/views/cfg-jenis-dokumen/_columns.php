<?php
use yii\helpers\Url;

/* @var $searchModel simpak\modules\referensi\models\CfgJenisDokumenSearch */

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_JENIS_UPLOAD',
        'value' => 'JENIS_UPLOAD',
        'label' => $searchModel->getAttributeLabel('ID_JENIS_UPLOAD'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_JENIS_DOKUMEN',
        'value' => 'JENIS_DOKUMEN',
        'label' => $searchModel->getAttributeLabel('ID_JENIS_DOKUMEN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID_TIPE_DOKUMEN',
        'value' => function($model)
        {
            if($model['ID_TIPE_DOKUMEN'] == 1)
            {
                return 'No Upload';
            }
            elseif ($model['ID_TIPE_DOKUMEN'] == 2)
            {
                return 'Upload';
            }
            elseif($model['ID_TIPE_DOKUMEN'] == 3)
            {
                return 'Sinkron';
            }
        },
        'filter' => [
            '0' => 'No Upload',
            '1' => 'Upload',
            '2' => 'Sinkron'
        ],
        'label' => $searchModel->getAttributeLabel('ID_TIPE_DOKUMEN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'IS_REQUIRED',
        'value' => function($model)
        {
            if($model['IS_REQUIRED'] == 0)
            {
                return 'Tidak';
            }
            elseif ($model['IS_REQUIRED'] == 1)
            {
                return 'Ya';
            }
        },
        'filter' => [
            '0' => 'Tidak',
            '1' => 'Ya',
        ],
        'label' => $searchModel->getAttributeLabel('IS_REQUIRED'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'width' => '60px',
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Detail', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
        ],
    ],
];
