<?php

use common\models\referensi\RefJenisDokumen;
use common\models\referensi\RefJenisUpload;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\CfgJenisDokumen */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="cfg-jenis-dokumen-form">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <h2 class="box-title text-uppercase"><?= 'Form Cfg Jenis Dokumen' ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>


            <div class="row">
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'ID_JENIS_UPLOAD')->widget(Select2::className(), [
                        'data' => RefJenisUpload::map(),
                        'options' => ['id' => 'fi-id_jenis_upload', 'placeholder' => '-- Pilih --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Jenis Upload')
                    ?>

                </div>
                <div class="col-sm-6">
                    <?=
                    $form->field($model, 'ID_JENIS_DOKUMEN')->widget(Select2::className(), [
                        'data' => RefJenisDokumen::map(),
                        'options' => ['id' => 'fi-id_jenis_dokumen', 'placeholder' => '-- Pilih --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Jenis Dokumen')
                    ?>

                </div>
            </div>

            <?=
            $form->field($model, 'ID_TIPE_DOKUMEN')->widget(Select2::classname(), [
                'data' => [
                    1 => 'No Upload',
                    2 => 'Upload',
                    3 => 'Sinkron'
                ],
                'options' => ['placeholder' => 'Pilih tipe dokumen ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>

            <div class="row">
                <div class="col-sm-6">            
                    <?=
                    $form->field($model, 'IS_REQUIRED')->widget(Select2::classname(), [
                        'data' => [
                            0 => 'Tidak',
                            1 => 'Ya'
                        ],
                        'options' => ['placeholder' => 'Pilih ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-sm-6">            
                    <?=
                    $form->field($model, 'IS_SHOW')->widget(Select2::classname(), [
                        'data' => [
                            0 => 'Tidak',
                            1 => 'Ya'
                        ],
                        'options' => ['placeholder' => 'Pilih ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                </div>                
            </div>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?=
                    Html::submitButton(
                            $model->isNewRecord ? 'Tambah' : 'Update',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    )
                    ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>