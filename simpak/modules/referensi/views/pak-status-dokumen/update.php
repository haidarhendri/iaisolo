<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefStatusDokumen */
?>
<div class="ref-status-dokumen-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
