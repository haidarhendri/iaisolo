<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefStatusDokumen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-status-dokumen-form">
  <div class="box box-danger box-solid">
    <div class="box-header">
        <h2 class="box-title text-uppercase"><?= 'Form Ref Status Dokumen' ?></h2>
    </div>
      <div class="box-body">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'STATUS_DOKUMEN')->textInput(['maxlength' => true]) ?>


      <?php if (!Yii::$app->request->isAjax){ ?>
          <div class="form-group">
              <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
          </div>
      <?php } ?>

        <?php ActiveForm::end(); ?>

      </div>
  </div>
</div>
