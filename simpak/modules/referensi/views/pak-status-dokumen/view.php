<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\referensi\RefStatusDokumen */
$this->title = 'Detil Ref Pak Status Dokumen';
$this->params['breadcrumbs'][] = ['label' => 'Ref Pak Status Dokumen', 'url' => ['index']];
?>
<div class="ref-status-dokumen-view">
    <div class="box box-danger box-solid">
      <div class="box-header">
          <div class="box-title"><?= 'Detail Ref Status Dokumen' ?></div>
      </div>
        <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'ID',
                'STATUS_DOKUMEN',
            ],
        ]) ?>
      </div>
  </div>
</div>
