<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel common\models\referensi\searches\RefStatusDokumenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ref Pak Status Dokumen';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="ref-status-dokumen-index">
  <div class="box box-danger">
      <div class="box-body">
          <div class="pull-right">
              <?= Html::button(
                  '<i class="glyphicon fa fa-search"></i> Pencarian Lanjut',
                  [
                      'class' => 'btn btn-info search-button',
                      'data-toggle' => 'collapse',
                      'data-target' => '#ref-pak-status-dokumen-search',
                  ]
              ); ?>

              <?= Html::a(
                  '<i class="glyphicon fa fa-list-ul"></i> Referensi',
                  ['/referensi/default/index'],
                  ['class' => 'btn btn-warning back-button']
              ); ?>

          </div>

          <?= $this->render('_search', ['model' => $searchModel]); ?>
      </div>
  </div>
  <div id="ajaxCrudDatatable">
      <?= GridView::widget([
          'id' => 'crud-datatable',
          'dataProvider' => $dataProvider,
          'filterModel' => $searchModel,
          'pjax' => true,
          'columns' => require(__DIR__ . '/_columns.php'),
          'striped' => true,
          'condensed' => true,
          'responsive' => true,
          'responsiveWrap' => false,
          'toolbar' => [
              [
                  'content' => Html::a(
                          '<i class="glyphicon glyphicon-repeat"></i>',
                          ['index'],
                          [
                              'data-pjax' => 1,
                              'class' => 'btn btn-default',
                              'title' => 'Reset Grid',
                          ]
                      ) . '{toggleData} {export}',
              ],
          ],
          'panel' => [
              'type' => 'default',
              'heading' => '<i class="glyphicon glyphicon-list"></i>',
              'before' => Html::a(
                  '<i class="glyphicon glyphicon-plus"></i> Tambah Data',
                  ['create'],
                  ['role' => 'modal-remote', 'title' => 'Tambah Data', 'class' => 'btn btn-success']
              ),
              'after' => Html::a(
                      '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                      ['index'],
                      ['class' => 'btn btn-primary btn-xs']
                  )
                  . '<div class="clearfix"></div>',
          ],
      ]) ?>
  </div>
</div>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
