<?php

/* @var $searchModel simpak\modules\referensi\models\IaiSettingSearch */

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'SETTING_KEY',
        'label' => $searchModel->getAttributeLabel('SETTING_KEY'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'SETTING_VALUE',
        'label' => $searchModel->getAttributeLabel('SETTING_VALUE'),
    ],
];
