<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiSetting */

$this->title = 'Iai Setting';
$this->params['breadcrumbs'][] = ['label' => 'Halaman Pengaturan Variabel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-setting-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Iai Setting' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'SETTING_KEY',
                    'SETTING_VALUE',
                ],
            ]) ?>

        </div>
    </div>
</div>
