<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiSetting */

$this->title = 'Update Iai Setting';
$this->params['breadcrumbs'][] = ['label' => 'Halaman Pengaturan Variabel', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-setting-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
