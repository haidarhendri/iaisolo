<?php
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\referensi\models\IaiSettingSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Halaman Pengaturan Variabel';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-setting-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-IAI-SETTING-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . 'Pencarian',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
        <div class="box-body search-collapse collapse in">
            <?= $this->render('_search', [
                'model' => $searchModel
            ]); ?>
        </div>
    </div>
    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dpSearch,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                    Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    ['index'],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                )
                . '{toggleData}'
                . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar Iai Setting',
            'before' => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . 'Tambah',
                ['create'],
                ['role' => 'modal-remote', 'class' => 'btn btn-success']
            ),
        ],
    ]) ?>

</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    'footer' => '',
]);
Modal::end();
?>
