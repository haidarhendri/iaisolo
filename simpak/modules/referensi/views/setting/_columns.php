<?php
use yii\helpers\Url;

/* @var $searchModel simpak\modules\referensi\models\IaiSettingSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'SETTING_KEY',
        'label' => $searchModel->getAttributeLabel('SETTING_KEY'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'SETTING_VALUE',
        'label' => $searchModel->getAttributeLabel('SETTING_VALUE'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'width' => '100px',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Detail', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
        ],
    ],
];
