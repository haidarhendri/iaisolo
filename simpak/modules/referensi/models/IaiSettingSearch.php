<?php

namespace simpak\modules\referensi\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiSetting;

/**
 * IaiSettingSearch represents the model behind the search form about `common\models\IaiSetting`.
 */
class IaiSettingSearch extends IaiSetting
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['SETTING_KEY', 'SETTING_VALUE'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere(['like', 'SETTING_KEY', $this->SETTING_KEY])
            ->andFilterWhere(['like', 'SETTING_VALUE', $this->SETTING_VALUE]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'SETTING_KEY',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere(['like', 'SETTING_KEY', $this->SETTING_KEY])
            ->andFilterWhere(['like', 'SETTING_VALUE', $this->SETTING_VALUE]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'SETTING_KEY',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiSetting::find();
    }
}
