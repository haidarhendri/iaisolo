<?php

namespace simpak\modules\referensi\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\referensi\CfgJenisDokumen;
use common\models\referensi\RefJenisUpload;
use common\models\referensi\RefJenisDokumen;

/**
 * CfgJenisDokumenSearch represents the model behind the search form about `common\models\referensi\CfgJenisDokumen`.
 */
class CfgJenisDokumenSearch extends CfgJenisDokumen
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ID', 'ID_JENIS_UPLOAD', 'ID_JENIS_DOKUMEN', 'ID_TIPE_DOKUMEN', 'IS_REQUIRED', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['CREATE_DATE', 'CREATE_IP', 'UPDATE_DATE', 'UPDATE_IP'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = CfgJenisDokumen::find()
            ->select([
                'A.*',
                'B.JENIS_DOKUMEN AS JENIS_DOKUMEN',
                'C.JENIS_UPLOAD AS JENIS_UPLOAD',
            ])
            ->from(['A' => static::tableName()])
            ->joinWith([
                'idJenisDokumen' => function (ActiveQuery $q) {
                    return $q->from(['B' => RefJenisDokumen::tableName()]);
                },
                'idJenisUpload' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefJenisUpload::tableName()]);
                },
            ], false)
            ->groupBy('A.ID');

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_JENIS_UPLOAD' => $this->ID_JENIS_UPLOAD,
            'ID_JENIS_DOKUMEN' => $this->ID_JENIS_DOKUMEN,
            'ID_TIPE_DOKUMEN' => $this->ID_TIPE_DOKUMEN,
            'IS_REQUIRED' => $this->IS_REQUIRED,
            'CREATE_DATE' => $this->CREATE_DATE,
            'CREATE_BY' => $this->CREATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
        ]);

        $query->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_JENIS_UPLOAD' => $this->ID_JENIS_UPLOAD,
            'ID_JENIS_DOKUMEN' => $this->ID_JENIS_DOKUMEN,
            'ID_TIPE_DOKUMEN' => $this->ID_TIPE_DOKUMEN,
            'IS_REQUIRED' => $this->IS_REQUIRED,
            'CREATE_DATE' => $this->CREATE_DATE,
            'CREATE_BY' => $this->CREATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
        ]);

        $query->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return CfgJenisDokumen::find();
    }
}
