<?php

namespace simpak\modules\referensi;

/**
 * referensi module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'simpak\modules\referensi\controllers';

    /**
     * @inheritdoc
     */
    public $defaultRoute = 'site/index';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
