<?php

namespace simpak\modules\sekretaris\models;

use common\models\User;
use common\models\AppAuthAssignment;
use common\models\referensi\RefAuthItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */

    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            ['USER_ID', 'ITEM_NAME']
        );
    }

    public function rules()
    {
        return [
            [
                [
                    'ID',
                    'STATUS',
                    'IS_SHOW',
                    'ID_UNIT',
                    'ID_SUB_UNIT'
                ], 'integer'
            ],
            [
                [
                    'USERNAME',
                    'NO_KTA',
                    'EMAIL',
                    'FULL_NAME',
                    'DISPLAY_NAME',
                    'PASSWORD_HASH',
                    'PASSWORD_RESET_TOKEN',
                    'AUTH_KEY',
                    'CREATED_AT',
                    'UPDATED_AT',
                    'LOGIN_AT',
                    'JENIS_USER',
                    'GELAR_DEPAN',
                    'GELAR_BELAKANG',
                    'TEMPAT_LAHIR',
                    'TANGGAL_LAHIR',
                    'JENIS_KELAMIN',
                    'GOL_DARAH',
                    'AGAMA',
                    'ALAMAT_KTP',
                    'NO_HP',
                    'KORWIL'
                ], 'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->select(['A.*', 'B.ITEM_NAME', 'B.USER_ID', 'C.NAME', 'C.TYPE', 'C.DESCRIPTION'])
            ->from(['A' => static::tableName()])
            ->joinWith([
                'appAuthAssignment' => function (ActiveQuery $q) {
                    return $q->from(['B' => AppAuthAssignment::tableName()]);
                },
                'appAuthAssignment.authItem' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefAuthItem::tableName()]);
                }
            ], false)
            ->groupBy('A.ID');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.STATUS' => $this->STATUS,
            'A.CREATED_AT' => $this->CREATED_AT,
            'A.UPDATED_AT' => $this->UPDATED_AT,
            'A.LOGIN_AT' => $this->LOGIN_AT,
            'A.ID_UNIT' => $this->ID_UNIT,
            'A.ID_SUB_UNIT' => $this->ID_SUB_UNIT,
        ]);

        $query->andFilterWhere(['like', 'USERNAME', $this->USERNAME])
            ->andFilterWhere(['like', 'NO_KTA', $this->NO_KTA])
            ->andFilterWhere(['like', 'EMAIL', $this->EMAIL])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'DISPLAY_NAME', $this->DISPLAY_NAME])
            ->andFilterWhere(['like', 'PASSWORD_HASH', $this->PASSWORD_HASH])
            ->andFilterWhere(['like', 'PASSWORD_RESET_TOKEN', $this->PASSWORD_RESET_TOKEN])
            ->andFilterWhere(['like', 'AUTH_KEY', $this->AUTH_KEY])
            ->andFilterWhere(['like', 'JENIS_USER', $this->JENIS_USER])
            ->andFilterWhere(['like', 'GELAR_DEPAN', $this->GELAR_DEPAN])
            ->andFilterWhere(['like', 'GELAR_BELAKANG', $this->GELAR_BELAKANG])
            ->andFilterWhere(['like', 'TEMPAT_LAHIR', $this->TEMPAT_LAHIR])
            ->andFilterWhere(['like', 'JENIS_KELAMIN', $this->JENIS_KELAMIN])
            ->andFilterWhere(['like', 'GOL_DARAH', $this->GOL_DARAH])
            ->andFilterWhere(['like', 'AGAMA', $this->AGAMA])
            ->andFilterWhere(['like', 'ALAMAT_KTP', $this->ALAMAT_KTP])
            ->andFilterWhere(['like', 'NO_HP', $this->NO_HP])
            ->andFilterWhere(['like', 'KORWIL', $this->KORWIL]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'STATUS' => $this->STATUS,
            'CREATED_AT' => $this->CREATED_AT,
            'UPDATED_AT' => $this->UPDATED_AT,
            'LOGIN_AT' => $this->LOGIN_AT,
            'IS_SHOW' => $this->IS_SHOW,
            'TANGGAL_LAHIR' => $this->TANGGAL_LAHIR,
        ]);

        $query->andFilterWhere(['like', 'USERNAME', $this->USERNAME])
            ->andFilterWhere(['like', 'NO_KTA', $this->NO_KTA])
            ->andFilterWhere(['like', 'EMAIL', $this->EMAIL])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'DISPLAY_NAME', $this->DISPLAY_NAME])
            ->andFilterWhere(['like', 'PASSWORD_HASH', $this->PASSWORD_HASH])
            ->andFilterWhere(['like', 'PASSWORD_RESET_TOKEN', $this->PASSWORD_RESET_TOKEN])
            ->andFilterWhere(['like', 'AUTH_KEY', $this->AUTH_KEY])
            ->andFilterWhere(['like', 'JENIS_USER', $this->JENIS_USER])
            ->andFilterWhere(['like', 'GELAR_DEPAN', $this->GELAR_DEPAN])
            ->andFilterWhere(['like', 'GELAR_BELAKANG', $this->GELAR_BELAKANG])
            ->andFilterWhere(['like', 'TEMPAT_LAHIR', $this->TEMPAT_LAHIR])
            ->andFilterWhere(['like', 'JENIS_KELAMIN', $this->JENIS_KELAMIN])
            ->andFilterWhere(['like', 'GOL_DARAH', $this->GOL_DARAH])
            ->andFilterWhere(['like', 'AGAMA', $this->AGAMA])
            ->andFilterWhere(['like', 'ALAMAT_KTP', $this->ALAMAT_KTP])
            ->andFilterWhere(['like', 'NO_HP', $this->NO_HP])
            ->andFilterWhere(['like', 'KORWIL', $this->KORWIL]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return User::find();
    }
}
