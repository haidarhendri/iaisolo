<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\sekretaris\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="user-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'NO_KTA') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'EMAIL') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'FULL_NAME') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'NO_HP') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'GOL_DARAH') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'AGAMA') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'KORWIL') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'JENIS_KELAMIN') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'TEMPAT_LAHIR') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'TANGGAL_LAHIR') ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'ALAMAT_KTP') ?>
        </div>
    </div>


    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>