<?php

/* @var $searchModel simpak\modules\sekretaris\models\UserSearch */

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'USERNAME',
        'label' => $searchModel->getAttributeLabel('USERNAME'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_KTA',
        'label' => $searchModel->getAttributeLabel('NO_KTA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'EMAIL',
        'label' => $searchModel->getAttributeLabel('EMAIL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => $searchModel->getAttributeLabel('FULL_NAME'),
    ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'DISPLAY_NAME',
    //     'label' => $searchModel->getAttributeLabel('DISPLAY_NAME'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'PASSWORD_HASH',
    //     'label' => $searchModel->getAttributeLabel('PASSWORD_HASH'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'PASSWORD_RESET_TOKEN',
    //     'label' => $searchModel->getAttributeLabel('PASSWORD_RESET_TOKEN'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'AUTH_KEY',
    //     'label' => $searchModel->getAttributeLabel('AUTH_KEY'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'STATUS',
    //     'label' => $searchModel->getAttributeLabel('STATUS'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'CREATED_AT',
    //     'label' => $searchModel->getAttributeLabel('CREATED_AT'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'UPDATED_AT',
    //     'label' => $searchModel->getAttributeLabel('UPDATED_AT'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'LOGIN_AT',
    //     'label' => $searchModel->getAttributeLabel('LOGIN_AT'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'JENIS_USER',
    //     'label' => $searchModel->getAttributeLabel('JENIS_USER'),
    // ],
    // [
    //     'class'=>'\kartik\grid\DataColumn',
    //     'attribute' => 'IS_SHOW',
    //     'label' => $searchModel->getAttributeLabel('IS_SHOW'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GELAR_DEPAN',
        'label' => $searchModel->getAttributeLabel('GELAR_DEPAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GELAR_BELAKANG',
        'label' => $searchModel->getAttributeLabel('GELAR_BELAKANG'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TEMPAT_LAHIR',
        'label' => $searchModel->getAttributeLabel('TEMPAT_LAHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_LAHIR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_LAHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENIS_KELAMIN',
        'label' => $searchModel->getAttributeLabel('JENIS_KELAMIN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GOL_DARAH',
        'label' => $searchModel->getAttributeLabel('GOL_DARAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'AGAMA',
        'label' => $searchModel->getAttributeLabel('AGAMA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ALAMAT_KTP',
        'label' => $searchModel->getAttributeLabel('ALAMAT_KTP'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_HP',
        'label' => $searchModel->getAttributeLabel('NO_HP'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'KORWIL',
        'label' => $searchModel->getAttributeLabel('KORWIL'),
    ],
];
