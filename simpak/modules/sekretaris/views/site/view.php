<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'User';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pengguna', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail User' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'ID',
                    'USERNAME',
                    'NO_KTA',
                    'EMAIL:email',
                    'FULL_NAME',
                    'DISPLAY_NAME',
                    'PASSWORD_HASH',
                    'PASSWORD_RESET_TOKEN',
                    'AUTH_KEY',
                    'STATUS',
                    'CREATED_AT',
                    'UPDATED_AT',
                    'LOGIN_AT',
                    'JENIS_USER',
                    'IS_SHOW',
                    'GELAR_DEPAN',
                    'GELAR_BELAKANG',
                    'TEMPAT_LAHIR',
                    'TANGGAL_LAHIR',
                    'JENIS_KELAMIN',
                    'GOL_DARAH',
                    'AGAMA',
                    'ALAMAT_KTP:ntext',
                    'NO_HP',
                    'KORWIL',
                ],
            ]) ?>

        </div>
    </div>
</div>
