<?php

namespace simpak\modules\anggota\controllers;

use Yii;
use common\models\IaiRiwayatSertif;
use common\models\IaiTransaksi;
use common\models\IaiTransaksiDokumen;
use common\models\User;
use Exception;
use simpak\modules\anggota\models\IaiRiwayatSertifSearch;
use yii\base\Application;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * SertifController implements the CRUD actions for IaiRiwayatSertif model.
 */
class SertifController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IaiRiwayatSertif models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IaiRiwayatSertifSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
        ]);
    }

    /**
     * Displays a single IaiRiwayatSertif model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Riwayat Sertif',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                Html::button(
                    'Tutup',
                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new IaiRiwayatSertif model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new IaiRiwayatSertif();
        $detail = new IaiTransaksiDokumen();
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

        $transaksi = new IaiTransaksi([
            'ID_USER' => $userId,
            'URAIAN_KEGIATAN' => IaiRiwayatSertif::URAIAN_KEGIATAN,
        ]);

        $idJenisUpload = 8;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_SERTIF',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $success = $model->save();
                    }
                    if ($success) {
                        $dokumen = $detail->uploadDokumen($dokumen);
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        $this->setDataUser($model);
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Tambah Sertif',
                            'content' => '<span class="text-success">' . 'Tambah Sertif berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Tambah Lagi',
                                    ['create'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Tambah Sertif',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Tambah Sertif',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }
        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $success = $model->save();
                }
                if ($success) {
                    $dokumen = $detail->uploadDokumen($dokumen);
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    $this->setDataUser($model);
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Tambah Sertifikat berhasil.');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Updates an existing IaiRiwayatSertif model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $transaksi = $model->idTransaksi;
        $detail = new IaiTransaksiDokumen();
        $idJenisUpload = 8;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_SERTIF',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            if (isset($dokumen['records'][$index])) {
                $form->setAttributes($dokumen['records'][$index]);
            }
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        $success = $model->save();
                    }
                    if ($success) {
                        $dokumen = $detail->uploadDokumen($dokumen);
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        $this->setDataUser($model);
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Update Riwayat Sertif',
                            'content' => '<span class="text-success">' . 'Update Riwayat Sertif berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Update',
                                    ['update', 'id' => $id],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();

                    return [
                        'title' => 'Update Riwayat Sertif',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Update Riwayat Sertif',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    $success = $model->save();
                }
                if ($success) {
                    $dokumen = $detail->uploadDokumen($dokumen);
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    $this->setDataUser($model);
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Update Riwayat Pekerjaan berhasil');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('update', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Delete an existing IaiRiwayatSertif model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $modelSertif = $this->findModel($id);
        $modelTransaksiDokumen = $this->findModelTransaksiDokumen($modelSertif->ID_TRANSAKSI);

        if (file_exists($modelTransaksiDokumen->FILE_PATH)) {
            if (unlink($modelTransaksiDokumen->FILE_PATH)) {
                $modelTransaksiDokumen->delete();
            }
        }
        
        $modelSertif->delete();
        $modelTransaksi = $this->findModelTransaksi($modelSertif->ID_TRANSAKSI)->delete();

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }
        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus Riwayat Sertif berhasil');
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple existing IaiRiwayatSertif model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the IaiRiwayatSertif model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IaiRiwayatSertif the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IaiRiwayatSertif::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelTransaksi($idTransaksi)
    {
        if (($model = IaiTransaksi::findOne($idTransaksi)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Transaksi tidak ditemukan.');
        }
    }

    protected function findModelTransaksiDokumen($idTransaksi)
    {
        if (($model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $idTransaksi])) !== null) {
            return $model;
        } else {
            // throw new NotFoundHttpException('Transaksi Dokumen tidak ditemukan.');
            return false;
        }
    }

    public function actionFilePreview($id)
    {
        $request = Yii::$app->request;
        $model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $id]);

        $ext = ($model->FILE_NAME) ? substr($model->FILE_NAME, strrpos($model->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Preview File Upload',
                'content' => $this->renderAjax('file-preview', [
                    'model' => $model,
                    'type' => $type,
                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']),
            ];
        } else {
            return $this->render('file-preview', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }

    public function getDataSertif($id)
    {
        $getData = IaiRiwayatSertif::find()
            ->select(['B.ID AS ID_USER', 'A.*'])
            ->from(['A' => IaiRiwayatSertif::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
            ], false)
            ->where([
                'JENIS_SERTIF' => $id,
                'B.ID' => Yii::$app->user->identity->ID
            ])
            ->orderBy(['TGL_AKHIR' => SORT_DESC])->one();

        return $getData;
    }

    public function setDataUser($data)
    {
        $userId = Yii::$app->user->identity->ID;

        switch ($data->JENIS_SERTIF) {
            case '1':
                $data = $this->getDataSertif(1);
                $findUser = User::findOne($userId);
                $findUser->NO_SERKOM = $data->NOMOR_SERTIF;
                $findUser->TGL_AKHIR_SERKOM = $data->TGL_AKHIR;
                $findUser->save();
                break;
            case '2':
                $data = $this->getDataSertif(2);
                $findUser = User::findOne($userId);
                $findUser->NO_SRTA = $data->NOMOR_SERTIF;
                $findUser->TGL_AKHIR_SRTA = $data->TGL_AKHIR;
                $findUser->save();
                break;
            case '3':
                $data = $this->getDataSertif(3);
                $findUser = User::findOne($userId);
                $findUser->NO_SIPA = $data->NOMOR_SERTIF;
                $findUser->TGL_AKHIR_SIPA = $data->TGL_AKHIR;
                $findUser->save();
                break;

            default:
                break;
        }
    }
}
