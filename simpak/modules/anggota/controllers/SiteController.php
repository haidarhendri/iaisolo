<?php

namespace simpak\modules\anggota\controllers;

use app\models\RefKorwil;
use common\models\IaiRiwayatPendidikan;
use common\models\IaiTransaksi;
use Yii;
use common\models\User;
use simpak\modules\anggota\models\UserSearch;
use yii\db\ActiveQuery;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * SiteController implements the CRUD actions for User model.
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $searchModel = new UserSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->search(Yii::$app->request->queryParams);
        $modelProfil = $this->findDataUtama($identity->ID);
        $countAnggota = $searchModel->countAnggota();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
            'modelProfil' => $modelProfil,
            'countAnggota' => $countAnggota,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findDataUtama($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "User #" . $model->USERNAME,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new User model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new User(['scenario' => 'register']);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Tambah User",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]
                    ) . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => "submit"]
                    ),
                ];
            } else {
                if ($model->load($request->post())) {
                    $model->generateAuthKey();
                    $model->setPassword($model->password);
                    if ($model->save()) {
                        Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->ID);
                        return [
                            'title' => "Tambah User",
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]
                            ) . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => "submit"]
                            ),
                        ];
                    } else {
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => "Tambah User",
                            'content' => '<span class="text-success">Data Berhasil Ditambahkan...</span>',
                            'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                        ];
                    }
                } else {
                    return [
                        'title' => "Tambah User",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]
                        ) . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => "submit"]
                        ),
                    ];
                }
            }
        } else {
            if ($model->load($request->post())) {
                $model->generateAuthKey();
                $model->setPassword($model->password);
                if ($model->save()) {
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->ID);
                    return $this->redirect(['view', 'id' => $model->ID]);
                } else {
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update User #" . $model->USERNAME,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]
                    ) . Html::button(
                        'Simpan',
                        ['class' => 'btn btn-primary', 'type' => "submit"]
                    ),
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    Yii::$app->authManager->revokeAll($id);
                    Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->ID);
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Tambah User",
                        'content' => '<span class="text-success">Data Berhasil Ditambahkan...</span>',
                        'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => "modal"]),
                    ];
                } else {
                    return [
                        'title' => "Update User #" . $model->USERNAME,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]
                        ) . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => "submit"]
                        ),
                    ];
                }
            }
        } else {
            if ($model->load($request->post()) && $model->save()) {
                Yii::$app->authManager->revokeAll($id);
                Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->ID);
                return $this->redirect(['view', 'id' => $model->ID]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing User model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {

            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            return $this->redirect(['index']);
        }
    }

    /**
     * Update the user data for the user who has logged in.
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionProfile()
    {
        $model = $this->findModel(Yii::$app->user->id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            }
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    /**
     * Reset password for logged user.
     * It requires three use input for resetting password, they are the old password, new password, and repeat password.
     * The old password will be checked by User::passwordValidation.
     *
     * @return string|\yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionResetPassword()
    {
        $model = $this->findModel(Yii::$app->user->id);
        $model->setScenario('resetPassword');
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password);
            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->ID]);
            };
        }

        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Halaman yang diminta tidak tersedia.');
        }
    }

    protected function findDataUtama($id)
    {
        $model = User::find($id)
        ->select([
            'A.*', 'B.NAMA_WILAYAH'
        ])
        ->from(['A' => User::tableName()])
        ->joinWith([
            'idKorwil' => function (ActiveQuery $q) {
                return $q->from(['B' => RefKorwil::tableName()]);
            },
        ], false)
        ->where(['A.ID' => $id])
        ->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Halaman yang diminta tidak tersedia.');
        }
    }
}
