<?php

namespace simpak\modules\anggota\controllers;

use Yii;
use common\models\IaiRiwayatPendidikan;
use common\models\IaiTransaksi;
use common\models\IaiTransaksiDokumen;
use common\models\User;
use Exception;
use simpak\modules\anggota\models\IaiRiwayatPendidikanSearch;
use yii\base\Application;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * PendidikanController implements the CRUD actions for IaiRiwayatPendidikan model.
 */
class PendidikanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all IaiRiwayatPendidikan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IaiRiwayatPendidikanSearch();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
        ]);
    }

    /**
     * Displays a single IaiRiwayatPendidikan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Riwayat Pendidikan',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                Html::button(
                    'Tutup',
                    ['class' => 'btn btn-default pull-right', 'data-dismiss' => 'modal']
                ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new IaiRiwayatPendidikan model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new IaiRiwayatPendidikan();
        $detail = new IaiTransaksiDokumen();
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;

        $transaksi = new IaiTransaksi([
            'ID_USER' => $userId,
            'URAIAN_KEGIATAN' => IaiRiwayatPendidikan::URAIAN_KEGIATAN,
        ]);

        $idJenisUpload = 2;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_PENDIDIKAN',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        if ($model->IS_UTAMA == 1) {
                            $this->setDefault();
                        }
                        $success = $model->save();
                    }
                    if ($model->IS_UTAMA == 1) {
                        $findUser = User::findOne($userId);
                        $findUser->PERGURUAN_TINGGI = $model->NAMA_INSTITUSI;
                        $findUser->NO_IJAZAH = $model->NOMOR_IJAZAH;
                        $findUser->save();
                    }
                    $dokumen = $detail->uploadDokumen($dokumen);
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Tambah Riwayat Pendidikan',
                            'content' => '<span class="text-success">' . 'Tambah Riwayat Pendidikan berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Tambah Lagi',
                                    ['create'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();
                    return [
                        'title' => 'Tambah Riwayat Pendidikan',
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Tambah Riwayat Pendidikan',
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    if ($model->IS_UTAMA == 1) {
                        $this->setDefault();
                    }
                    $success = $model->save();
                }
                if ($model->IS_UTAMA == 1) {
                    $findUser = User::findOne($userId);
                    $findUser->PERGURUAN_TINGGI = $model->NAMA_INSTITUSI;
                    $findUser->NO_IJAZAH = $model->NOMOR_IJAZAH;
                    $findUser->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Tambah Riwayat Pendidikan berhasil.');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('create', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Updates an existing IaiRiwayatPendidikan model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate($id)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $transaksi = $model->idTransaksi;
        $detail = new IaiTransaksiDokumen();
        $idJenisUpload = 2;

        $dokumen = [
            'cfgdokumen' => $detail->getCfgDokumen($idJenisUpload),
            'uploads' => $detail->getUploadDokumen($idJenisUpload),
            'records' => $detail->getRecordDokumen($transaksi->ID, $idJenisUpload),
            'model' => 'IAI_RIWAYAT_PENDIDIKAN',
        ];

        foreach ($dokumen['uploads'] as $index => $upload) {
            $form = new IaiTransaksiDokumen();
            $form->setAttributes($upload);
            if (isset($dokumen['records'][$index])) {
                $form->setAttributes($dokumen['records'][$index]);
            }
            $dokumen['form'][$index] = $form;
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $detail->load($request->post())) {
                $detail->validate();
                $model->validate();
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($success = $transaksi->save()) {
                        $dokumen['idTransaksi'] = $transaksi->ID;
                        $model->ID_TRANSAKSI = $transaksi->ID;
                        if ($model->IS_UTAMA == 1) {
                            $this->setDefault();
                        }
                        $success = $model->save();
                    }
                    if ($model->IS_UTAMA == 1) {
                        $findUser = User::findOne($userId);
                        $findUser->PERGURUAN_TINGGI = $model->NAMA_INSTITUSI;
                        $findUser->NO_IJAZAH = $model->NOMOR_IJAZAH;
                        $findUser->save();
                    }
                    $dokumen = $detail->uploadDokumen($dokumen);
                    if ($success) {
                        $success = $dokumen['success'];
                    }
                    if ($success) {
                        $transaction->commit();
                        foreach ($dokumen['deleted'] as $deleted) {
                            unlink($deleted);
                        }

                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'title' => 'Update Riwayat Pendidikan',
                            'content' => '<span class="text-success">' . 'Update Riwayat Pendidikan berhasil' . '</span>',
                            'footer' =>
                            Html::button(
                                'Tutup',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                            )
                                . Html::a(
                                    'Update',
                                    ['update', 'id' => $id],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                                ),
                        ];
                    }
                    throw new Exception('Error while saving the data.');
                } catch (Exception $e) {
                    $transaction->rollBack();
                    return [
                        'title' => 'Update Riwayat Pendidikan',
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'dokumen' => $dokumen,
                        ]),
                        'footer' =>
                        Html::button(
                            'Tutup',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                            . Html::button(
                                'Simpan',
                                ['class' => 'btn btn-primary', 'type' => 'submit']
                            ),
                    ];
                }
            } else {
                return [
                    'title' => 'Update Riwayat Pendidikan',
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'dokumen' => $dokumen,
                    ]),
                    'footer' =>
                    Html::button(
                        'Tutup',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                        . Html::button(
                            'Simpan',
                            ['class' => 'btn btn-primary', 'type' => 'submit']
                        ),
                ];
            }
        }
        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $detail->load($request->post())) {
            $detail->validate();
            $model->validate();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($success = $transaksi->save()) {
                    $dokumen['idTransaksi'] = $transaksi->ID;
                    $model->ID_TRANSAKSI = $transaksi->ID;
                    if ($model->IS_UTAMA == 1) {
                        $this->setDefault();
                    }
                    $success = $model->save();
                }
                if ($model->IS_UTAMA == 1) {
                    $findUser = User::findOne($userId);
                    $findUser->PERGURUAN_TINGGI = $model->NAMA_INSTITUSI;
                    $findUser->NO_IJAZAH = $model->NOMOR_IJAZAH;
                    $findUser->save();
                }
                $dokumen = $detail->uploadDokumen($dokumen);
                if ($success) {
                    $success = $dokumen['success'];
                }
                if ($success) {
                    $transaction->commit();
                    foreach ($dokumen['deleted'] as $deleted) {
                        unlink($deleted);
                    }
                    Yii::$app->session->setFlash('success', 'Update Riwayat Pendidikan berhasil');
                    return $this->redirect(['index']);
                }
                throw new Exception('Error while saving the data.');
            } catch (Exception $e) {
                $transaction->rollBack();
                return $this->render('update', [
                    'model' => $model,
                    'dokumen' => $dokumen,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'dokumen' => $dokumen,
            ]);
        }
    }

    /**
     * Delete an existing IaiRiwayatPendidikan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $modelPendidikan = $this->findModel($id);
        $modelTransaksiDokumen = $this->findModelTransaksiDokumen($modelPendidikan->ID_TRANSAKSI);

        if ($modelPendidikan->IS_UTAMA == 1) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => 'Hapus Riwayat Pendidikan',
                'content' => '<span class="text-danger">' . 'Tidak ada Riwayat Pendidikan Utama. Hapus Riwayat Pendidikan gagal!' . '</span>',
                'footer' =>
                Html::button(
                    'Tutup',
                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                ),
            ];
        }

        if (is_file($modelTransaksiDokumen->FILE_PATH)) {
            unlink($modelTransaksiDokumen->FILE_PATH);
        }

        $modelTransaksiDokumen->delete();
        $modelTransaksi = $this->findModelTransaksi($modelPendidikan->ID_TRANSAKSI)->delete();
        $modelPendidikan->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', 'Hapus Riwayat Pendidikan berhasil');
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple existing IaiRiwayatPendidikan model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the IaiRiwayatPendidikan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IaiRiwayatPendidikan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IaiRiwayatPendidikan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Model tidak ditemukan.');
        }
    }

    protected function findModelTransaksi($idTransaksi)
    {
        if (($model = IaiTransaksi::findOne($idTransaksi)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Transaksi tidak ditemukan.');
        }
    }

    protected function findModelTransaksiDokumen($idTransaksi)
    {
        if (($model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $idTransaksi])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Transaksi Dokumen tidak ditemukan.');
        }
    }

    public function setDefault()
    {
        $model = new IaiRiwayatPendidikanSearch();
        $cariPendidikanAktif = $model->cariPendidikanAktif();

        $implode = array_map(function ($cariPendidikanAktif) {
            return $cariPendidikanAktif['ID_TRANSAKSI'];
        }, $cariPendidikanAktif);

        $update = IaiRiwayatPendidikan::updateAll(['IS_UTAMA' => 0], ['AND', 'IS_UTAMA = 1', ['IN', 'ID_TRANSAKSI', $implode]]);

        return true;
    }

    public function actionFilePreview($id)
    {
        $request = Yii::$app->request;
        $model = IaiTransaksiDokumen::findOne(['ID_TRANSAKSI' => $id]);

        $ext = ($model->FILE_NAME) ? substr($model->FILE_NAME, strrpos($model->FILE_NAME, '.') + 1) : 'jpg';

        if (in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $type = 'image';
        } else {
            $type = 'pdf';
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => 'Preview File Upload',
                'content' => $this->renderAjax('file-preview', [
                    'model' => $model,
                    'type' => $type,
                ]),
                'footer' => Html::button('Tutup', ['class' => 'btn btn-default', 'data-dismiss' => 'modal']),
            ];
        } else {
            return $this->render('file-preview', [
                'model' => $model,
                'type' => $type,
            ]);
        }
    }
}
