<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\anggota\models\IaiRiwayatIuranSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="iai-riwayat-iuran-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'FULL_NAME') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'NOMINAL') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'TANGGAL_AWAL')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TANGGAL_AKHIR')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'STATUS_BAYAR') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TANGGAL_BAYAR')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>