<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatPekerjaan */

$this->title = 'Update Riwayat Pekerjaan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Pekerjaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-pekerjaan-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
