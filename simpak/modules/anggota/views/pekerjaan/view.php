<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatPekerjaan */

$this->title = 'Riwayat Pekerjaan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Pekerjaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-pekerjaan-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Riwayat Pekerjaan' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'TEMPAT_KERJA',
                    'JABATAN',
                    'TANGGAL_AWAL',
                    'TANGGAL_AKHIR',
                    'IS_UTAMA',
                ],
            ]) ?>

        </div>
    </div>
</div>
