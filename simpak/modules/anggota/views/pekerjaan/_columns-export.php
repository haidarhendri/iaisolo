<?php

/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatPekerjaanSearch */

return [
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TEMPAT_KERJA',
        'label' => $searchModel->getAttributeLabel('TEMPAT_KERJA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JABATAN',
        'label' => $searchModel->getAttributeLabel('JABATAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_AWAL',
        'label' => $searchModel->getAttributeLabel('TANGGAL_AWAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_AKHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'IS_UTAMA',
        'label' => $searchModel->getAttributeLabel('IS_UTAMA'),
    ],
];
