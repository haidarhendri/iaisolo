<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'User';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Pengguna', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail User' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'FULL_NAME',
                    'EMAIL:email',
                    'NO_KTA',
                    'NO_HP',
                    'GELAR_DEPAN',
                    'GELAR_BELAKANG',
                    'ALAMAT_KTP:ntext',
                    'ALAMAT_DOMISILI:ntext',
                    'TEMPAT_LAHIR',
                    'TANGGAL_LAHIR',
                    'JENIS_KELAMIN',
                    'GOL_DARAH',
                    'AGAMA',
                    'TEMPAT_KERJA',
                    'JABATAN_KERJA',
                    'PERGURUAN_TINGGI',
                    'NO_IJAZAH',
                    'NO_SERKOM',
                    'TGL_AKHIR_SERKOM',
                    'NO_SRTA',
                    'TGL_AKHIR_SRTA',
                    'NO_SIPA',
                    'TGL_AKHIR_SIPA',
                    'idKorwil.NAMA_WILAYAH',
                ],
            ]) ?>

        </div>
    </div>
</div>