<?php

/* @var $searchModel simpak\modules\anggota\models\UserSearch */

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => $searchModel->getAttributeLabel('FULL_NAME'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'EMAIL',
        'label' => $searchModel->getAttributeLabel('EMAIL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_KTA',
        'label' => $searchModel->getAttributeLabel('NO_KTA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_HP',
        'label' => $searchModel->getAttributeLabel('NO_HP'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GELAR_DEPAN',
        'label' => $searchModel->getAttributeLabel('GELAR_DEPAN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GELAR_BELAKANG',
        'label' => $searchModel->getAttributeLabel('GELAR_BELAKANG'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ALAMAT_KTP',
        'label' => $searchModel->getAttributeLabel('ALAMAT_KTP'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ALAMAT_DOMISILI',
        'label' => $searchModel->getAttributeLabel('ALAMAT_DOMISILI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TEMPAT_LAHIR',
        'label' => $searchModel->getAttributeLabel('TEMPAT_LAHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_LAHIR',
        'label' => $searchModel->getAttributeLabel('TANGGAL_LAHIR'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENIS_KELAMIN',
        'label' => $searchModel->getAttributeLabel('JENIS_KELAMIN'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'GOL_DARAH',
        'label' => $searchModel->getAttributeLabel('GOL_DARAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'AGAMA',
        'label' => $searchModel->getAttributeLabel('AGAMA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'AGAMA',
        'label' => $searchModel->getAttributeLabel('AGAMA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TEMPAT_KERJA',
        'label' => $searchModel->getAttributeLabel('TEMPAT_KERJA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JABATAN_KERJA',
        'label' => $searchModel->getAttributeLabel('JABATAN_KERJA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'PERGURUAN_TINGGI',
        'label' => $searchModel->getAttributeLabel('PERGURUAN_TINGGI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_IJAZAH',
        'label' => $searchModel->getAttributeLabel('NO_IJAZAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_SERKOM',
        'label' => $searchModel->getAttributeLabel('NO_SERKOM'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR_SERKOM',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR_SERKOM'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_SRTA',
        'label' => $searchModel->getAttributeLabel('NO_SRTA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR_SRTA',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR_SRTA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NO_SIPA',
        'label' => $searchModel->getAttributeLabel('NO_SIPA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR_SIPA',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR_SIPA'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'KORWIL',
        'value' => 'idKorwil.NAMA_WILAYAH',
        'label' => $searchModel->getAttributeLabel('KORWIL'),
    ],
];
