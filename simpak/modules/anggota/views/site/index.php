<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\anggota\models\UserSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Dashboard Pengguna';
$this->params['breadcrumbs'][] = $this->title;

$user = Yii::$app->user;
$identity = $user->identity;

?>

<?php if ($user->can('role_superuser')) { ?>
    <div class="row">
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['BANJARSARI'] ?></h3>
                    <p>BANJARSARI</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Banjarsari" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['JEBRES'] ?></h3>
                    <p>JEBRES</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Jebres" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['LAWEYAN'] ?></h3>
                    <p>LAWEYAN</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Laweyan" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['SERENGAN'] ?></h3>
                    <p>SERENGAN DAN PS. KLIWON</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Serengan" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['NON_KORWIL'] ?></h3>
                    <p>Anggota PC SKA-Non Korwil</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Non%20Korwil" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <div class="col-lg-2 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?= $countAnggota['NON_ANGGOTA'] ?></h3>
                    <p>Non Anggota PC-SKA</p>
                </div>
                <a href="?UserSearch%5BKORWIL%5D=Non%20Anggota" class="small-box-footer">
                    Selengkapnya <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>
<?php } ?>

<?= $this->render('@common/widgets/Tagihan'); ?>

<div class="user-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-USER-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . 'Pencarian',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
        <div class="box-body search-collapse collapse">
            <?= $this->render('_search', [
                'model' => $searchModel
            ]); ?>
        </div>
    </div>
    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dpSearch,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    ['index'],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                )
                    . '{toggleData}'
                    . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar User',
        ],
    ]) ?>

</div>
<div class="user-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Profil Pengguna' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $modelProfil,
                'attributes' => [
                    'FULL_NAME',
                    'EMAIL:email',
                    'NO_KTA',
                    'NO_HP',
                    'GELAR_DEPAN',
                    'GELAR_BELAKANG',
                    'ALAMAT_KTP:ntext',
                    'ALAMAT_DOMISILI:ntext',
                    'TEMPAT_LAHIR',
                    'TANGGAL_LAHIR',
                    'JENIS_KELAMIN',
                    'GOL_DARAH',
                    'AGAMA',
                    'TEMPAT_KERJA',
                    'JABATAN_KERJA',
                    'PERGURUAN_TINGGI',
                    'NO_IJAZAH',
                    'NO_SERKOM',
                    'TGL_AKHIR_SERKOM',
                    'NO_SRTA',
                    'TGL_AKHIR_SRTA',
                    'NO_SIPA',
                    'TGL_AKHIR_SIPA',
                    'idKorwil.NAMA_WILAYAH',
                ],
            ]) ?>

        </div>
    </div>
</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    "size" => "modal-lg",
    'footer' => '',
]);
Modal::end();
?>