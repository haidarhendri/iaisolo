<?php

/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatSertifSearch */

return [
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_TRANSAKSI',
        'label' => $searchModel->getAttributeLabel('ID_TRANSAKSI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_USER',
        'label' => $searchModel->getAttributeLabel('ID_USER'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENIS_SERTIF',
        'label' => $searchModel->getAttributeLabel('JENIS_SERTIF'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMOR_SERTIF',
        'label' => $searchModel->getAttributeLabel('NOMOR_SERTIF'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('TGL_AWAL'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
