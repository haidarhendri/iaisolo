<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatSertif */

$this->title = 'Tambah Iai Riwayat Sertif';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Sertif', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-sertif-create">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
