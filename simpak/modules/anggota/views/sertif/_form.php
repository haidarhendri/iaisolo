<?php

use common\models\RefJenisSertif;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatSertif */
/* @var $form yii\widgets\ActiveForm */

$uploads = $dokumen['uploads'];
$records = $dokumen['records'];
$files = $dokumen['form'];
?>
<div class="iai-riwayat-sertif-form">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <h2 class="box-title text-uppercase"><?= 'Form Iai Riwayat Sertif' ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'JENIS_SERTIF')->widget(Select2::className(), [
                        'data' => ArrayHelper::map(
                            RefJenisSertif::find()
                                ->select(['ID', 'NAMA_SERTIF',])->orderBy(['ID' => SORT_DESC])
                                ->asArray()
                                ->all(),
                            'ID',
                            'NAMA_SERTIF'
                        ),
                        'options' => ['placeholder' => '-- Pilih --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Jenis Sertif') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'NOMOR_SERTIF')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'TGL_AWAL')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'TGL_AKHIR')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
            </div>

            <div class="callout callout-danger">
                <h4>Perhatian !</h4>
                <p><b>Tidak semua dokumen wajib diupload!</b> Hanya Dokumen yang dibutuhkan sesuai sertifikat yang dipilih.</p>
            </div>

            <?= GridView::widget([
                'id' => 'cd-dokumen',
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $uploads,
                    'key' => 'ID',
                    'pagination' => false,
                ]),
                'pjax' => true,
                'striped' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => false,
                'summary' => false,
                'rowOptions' => function ($model) use ($records) {
                    $id = ArrayHelper::getValue($model, 'ID');
                    $record = ArrayHelper::getValue($records, $id);

                    return [];
                },
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'width' => '50%',
                        'attribute' => 'JENIS_DOKUMEN',
                        'label' => 'Nama Dokumen',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'width' => '40%',
                        'header' => 'Upload',
                        'dropdown' => false,
                        'vAlign' => 'top',
                        'template' => '{form}',
                        'buttons' => [
                            'form' => function ($url, $upload, $key) use ($form, $model, $files) {
                                $file = ArrayHelper::getValue($files, $key);
                                $template = '<div class="input-group {class}">{caption}<div class="input-group-btn">';

                                if ($file->FILE_URL) {
                                    $template .= Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $file->FILE_URL,
                                        ['class' => 'btn btn-success', 'role' => 'button', 'data-pjax' => 0, 'target' => '_blank']
                                    );
                                }

                                $template .= '{browse}</div></div>';

                                return $form
                                    ->field($file, "[$key]file")
                                    ->widget(FileInput::class, [
                                        'options' => [
                                            'multiple' => false,
                                            'accept' => 'image/*, application/pdf',
                                        ],
                                        'pluginOptions' => [
                                            'layoutTemplates' => ['main1' => $template],
                                            'browseLabel' => Yii::$app->request->isAjax ? '' : 'Pilih',
                                        ],
                                    ])
                                    ->label(false);

                                return '';
                            },
                        ],
                    ],
                ],
            ]) ?>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton(
                        $model->isNewRecord ? 'Tambah' : 'Update',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                    )
                    ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>