<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatSertif */

$this->title = 'Iai Riwayat Sertif';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Sertif', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-sertif-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Iai Riwayat Sertif' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'JENIS_SERTIF',
                    'NOMOR_SERTIF',
                    'TGL_AWAL',
                    'TGL_AKHIR',
                ],
            ]) ?>

        </div>
    </div>
</div>
