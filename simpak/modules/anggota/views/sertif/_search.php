<?php

use common\models\RefJenisSertif;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\anggota\models\IaiRiwayatSertifSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="iai-riwayat-sertif-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'JENIS_SERTIF')->widget(Select2::className(), [
                'data' => ArrayHelper::map(
                    RefJenisSertif::find()
                        ->select(['ID', 'NAMA_SERTIF',])->orderBy(['ID' => SORT_DESC])
                        ->asArray()
                        ->all(),
                    'ID',
                    'NAMA_SERTIF'
                ),
                'options' => ['placeholder' => '-- Pilih --'],
                'pluginOptions' => ['allowClear' => true],
            ])->label('Jenis Sertif') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'NOMOR_SERTIF') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'TGL_AWAL')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TGL_AKHIR')->widget(DatePicker::className(), [
                'layout' => '{input}{picker}',
                'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
            ]); ?>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>