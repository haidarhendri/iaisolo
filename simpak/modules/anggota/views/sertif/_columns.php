<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatSertifSearch */

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Lengkap',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'NAMA_SERTIF',
        'value' => 'NAMA_SERTIF',
        'label' => $searchModel->getAttributeLabel('JENIS_SERTIF'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'NOMOR_SERTIF',
        'label' => $searchModel->getAttributeLabel('NOMOR_SERTIF'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'TGL_AWAL',
        'label' => $searchModel->getAttributeLabel('TGL_AWAL'),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'TGL_AKHIR',
        'label' => $searchModel->getAttributeLabel('TGL_AKHIR'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Lihat',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $model['ID_TRANSAKSI']]);
        },
        'template' => '{file-preview}',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'buttons' => [
            'file-preview' => function ($url) {
                return Html::a('<i class="fa fa-eye"></i> Lihat File', $url, [
                    'id' => 'button-file-preview',
                    'role' => 'modal-remote',
                    'title' => 'Lihat',
                    'class' => 'btn btn-xs btn-info',
                    'data-toggle' => 'tooltip',
                    'data-pjax' => 0,
                ]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'width' => '100px',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-warning'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
