<?php
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatPendidikanSearch */
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = 'Dashboard Riwayat Pendidikan';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('@common/widgets/Tagihan'); ?>

<div class="iai-riwayat-pendidikan-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-IAI-RIWAYAT-PENDIDIKAN-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . 'Pencarian',
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
        <div class="box-body search-collapse collapse">
            <?= $this->render('_search', [
                'model' => $searchModel
            ]); ?>
        </div>
    </div>
    <?= GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dpSearch,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                    Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>',
                    ['index'],
                    ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => 'Reset']
                )
                . '{toggleData}'
                . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . 'Daftar Riwayat Pendidikan',
            'before' => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . 'Tambah',
                ['create'],
                ['role' => 'modal-remote', 'class' => 'btn btn-success']
            ),
            'after' => BulkButtonWidget::widget([
                'buttons' => Html::a(
                    '<i class="glyphicon glyphicon-trash"></i> ' . 'Delete All',
                    ['bulk-delete'],
                    [
                        'class' => 'btn btn-danger btn-xs',
                        'role' => 'modal-remote-bulk',
                        'data-confirm' => false,
                        'data-method' => false,
                        'data-request-method' => 'post',
                        'data-confirm-title' => 'Apakah Anda Yakin?',
                        'data-confirm-message' => 'Apakah anda yakin untuk menghapus item ini',
                    ]
                ),
            ])
            . '<div class="clearfix"></div>',
        ],
        'rowOptions' => function ($model) {
            if ($model['IS_UTAMA'] == 1) {
                return ['class' => 'success'];
            }

            return [];
        },
    ]) ?>

</div>
<?php
Modal::begin([
    'id' => 'ajaxCrudModal',
    "size"=> "modal-lg",
    'footer' => '',
]);
Modal::end();
?>
