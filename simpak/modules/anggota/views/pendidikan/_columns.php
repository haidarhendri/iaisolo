<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatPendidikanSearch */

return [
    // [
    //     'class' => 'kartik\grid\CheckboxColumn',
    //     'width' => '20px',
    // ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FULL_NAME',
        'label' => 'Nama Lengkap',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENJANG',
        'label' => $searchModel->getAttributeLabel('JENJANG'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NAMA_INSTITUSI',
        'label' => $searchModel->getAttributeLabel('NAMA_INSTITUSI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMOR_IJAZAH',
        'label' => $searchModel->getAttributeLabel('NOMOR_IJAZAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_IJAZAH',
        'label' => $searchModel->getAttributeLabel('TANGGAL_IJAZAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'IS_UTAMA',
        'format' => 'raw',
        'value' => function ($model) {
            switch ($model['IS_UTAMA']) {
                case 0:
                    return '<span class="label label-default">Bukan Utama</span>';
                case 1:
                    return '<span class="label label-success">Utama</span>';
            }
        },
        'label' => $searchModel->getAttributeLabel('IS_UTAMA'),
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'header' => 'Lihat',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $model['ID_TRANSAKSI']]);
        },
        'template' => '{file-preview}',
        'contentOptions' => ['style' => ['white-space' => 'nowrap']],
        'buttons' => [
            'file-preview' => function ($url) {
                return Html::a('<i class="fa fa-eye"></i> Lihat File', $url, [
                    'id' => 'button-file-preview',
                    'role' => 'modal-remote',
                    'title' => 'Lihat',
                    'class' => 'btn btn-xs btn-info',
                    'data-toggle' => 'tooltip',
                    'data-pjax' => 0,
                ]);
            },
        ],
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'width' => '100px',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'visibleButtons' => [
            // 'update' => function ($model) {
            //     return ($model['IS_UTAMA'] == 0) ? true : false;
            // },
            'delete' => function ($model) {
                return ($model['IS_UTAMA'] != 1) ? true : false;
            },
        ],
        'viewOptions' => [
            'role' => 'modal-remote',
            'title' => 'Lihat',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-success'
        ],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Ubah',
            'data-toggle' => 'tooltip',
            'class' => 'btn btn-xs btn-flat btn-warning'
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Hapus',
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Apakah anda yakin?',
            'data-confirm-message' => 'Apakah anda yakin untuk menghapus daftar ini?',
            'class' => 'btn btn-xs btn-flat btn-danger',
        ],
    ],
];
