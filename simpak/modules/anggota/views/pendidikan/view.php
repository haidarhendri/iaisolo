<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatPendidikan */

$this->title = 'Riwayat Pendidikan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Pendidikan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-pendidikan-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= 'Detail Riwayat Pendidikan' ?></div>
        </div>
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'JENJANG',
                    'NAMA_INSTITUSI',
                    'NOMOR_IJAZAH',
                    'TANGGAL_IJAZAH',
                    'FILE_IJAZAH',
                ],
            ]) ?>

        </div>
    </div>
</div>
