<?php

/* @var $searchModel simpak\modules\anggota\models\IaiRiwayatPendidikanSearch */

return [
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'ID',
        // 'label' => $searchModel->getAttributeLabel('ID'),
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'ID_USER',
        'label' => $searchModel->getAttributeLabel('ID_USER'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'JENJANG',
        'label' => $searchModel->getAttributeLabel('JENJANG'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NAMA_INSTITUSI',
        'label' => $searchModel->getAttributeLabel('NAMA_INSTITUSI'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'NOMOR_IJAZAH',
        'label' => $searchModel->getAttributeLabel('NOMOR_IJAZAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'TANGGAL_IJAZAH',
        'label' => $searchModel->getAttributeLabel('TANGGAL_IJAZAH'),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'FILE_IJAZAH',
        'label' => $searchModel->getAttributeLabel('FILE_IJAZAH'),
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_BY',
        // 'label' => $searchModel->getAttributeLabel('CREATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('CREATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_BY',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_BY'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_DATE',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_DATE'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'CREATE_IP',
        // 'label' => $searchModel->getAttributeLabel('CREATE_IP'),
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute' => 'UPDATE_IP',
        // 'label' => $searchModel->getAttributeLabel('UPDATE_IP'),
    // ],
];
