<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model simpak\modules\anggota\models\IaiRiwayatPendidikanSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="iai-riwayat-pendidikan-search search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'JENJANG') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'NAMA_INSTITUSI') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'NOMOR_IJAZAH') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'TANGGAL_IJAZAH') ?>
        </div>
    </div>

    <div class="form-group" style="margin-bottom: 0">
        <?= Html::submitButton(
            '<i class="fa fa-cog"></i> ' . 'Proses',
            ['class' => 'btn btn-primary']
        ) ?>

        <?= Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . 'Reset',
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>