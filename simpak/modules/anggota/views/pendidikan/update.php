<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatPendidikan */

$this->title = 'Update Riwayat Pendidikan';
$this->params['breadcrumbs'][] = ['label' => 'Dashboard Riwayat Pendidikan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="iai-riwayat-pendidikan-update">
    <?= $this->render('_form', [
        'model' => $model,
        'dokumen' => $dokumen,
    ]) ?>
</div>
