<?php

use app\models\RefJenjang;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\date\DatePicker;
use kartik\file\FileInput;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\IaiRiwayatPendidikan */
/* @var $form yii\widgets\ActiveForm */

$uploads = $dokumen['uploads'];
$records = $dokumen['records'];
$files = $dokumen['form'];

// $this->registerJs($this->render('_script.js'));
$this->registerCss('#cd-dokumen th, #cd-dokumen td{white-space:normal !important}');

CrudAsset::register($this);
?>
<div class="iai-riwayat-pendidikan-form">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <h2 class="box-title text-uppercase"><?= 'Form Riwayat Pendidikan' ?></h2>
        </div>
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'JENJANG')->widget(Select2::className(), [
                        'data' => ArrayHelper::map(
                            RefJenjang::find()
                                ->select(['ID', 'JENJANG',])->orderBy(['ID' => SORT_DESC])
                                ->asArray()
                                ->all(),
                            'ID',
                            'JENJANG'
                        ),
                        'options' => ['placeholder' => '-- Pilih --'],
                        'pluginOptions' => ['allowClear' => true],
                    ])->label('Jenjang') ?>
                </div>
                <div class="col-md-10">
                    <?= $form->field($model, 'NAMA_INSTITUSI')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'NOMOR_IJAZAH')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'TANGGAL_IJAZAH')->widget(DatePicker::className(), [
                        'layout' => '{input}{picker}',
                        'options' => ['placeholder' => 'Format: yyyy-mm-dd'],
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd'],
                    ]); ?>
                </div>
            </div>

            <?= GridView::widget([
                'id' => 'cd-dokumen',
                'dataProvider' => new ArrayDataProvider([
                    'allModels' => $uploads,
                    'key' => 'ID',
                    'pagination' => false,
                ]),
                'pjax' => true,
                'striped' => true,
                'condensed' => false,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => false,
                'summary' => false,
                'rowOptions' => function ($model) use ($records) {
                    $id = ArrayHelper::getValue($model, 'ID');
                    $record = ArrayHelper::getValue($records, $id);

                    return [];
                },
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'width' => '50%',
                        'attribute' => 'JENIS_DOKUMEN',
                        'label' => 'Nama Dokumen',
                    ],
                    [
                        'class' => 'kartik\grid\ActionColumn',
                        'width' => '40%',
                        'header' => 'Upload',
                        'dropdown' => false,
                        'vAlign' => 'top',
                        'template' => '{form}',
                        'buttons' => [
                            'form' => function ($url, $upload, $key) use ($form, $model, $files) {
                                $file = ArrayHelper::getValue($files, $key);
                                $template = '<div class="input-group {class}">{caption}<div class="input-group-btn">';

                                if ($file->FILE_URL) {
                                    $template .= Html::a(
                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                        $file->FILE_URL,
                                        ['class' => 'btn btn-success', 'role' => 'button', 'data-pjax' => 0, 'target' => '_blank']
                                    );
                                }

                                $template .= '{browse}</div></div>';

                                return $form
                                    ->field($file, "[$key]file")
                                    ->widget(FileInput::class, [
                                        'options' => [
                                            'multiple' => false,
                                            'accept' => 'image/*, application/pdf',
                                        ],
                                        'pluginOptions' => [
                                            'layoutTemplates' => ['main1' => $template],
                                            'browseLabel' => Yii::$app->request->isAjax ? '' : 'Pilih',
                                        ],
                                    ])

                                    ->label(false);

                                return '';
                            },
                        ],
                    ],
                ],
            ]) ?>
            <hr>
            <div class="pull-right">
                <?= $form->field($model, 'IS_UTAMA')->checkBox(['label' => ' Jadikan Pendidikan Tertinggi', 'uncheck' => 0, 'selected' => 1]); ?>
            </div>

            <?php if (!Yii::$app->request->isAjax) { ?>
                <div class="form-group">
                    <?= Html::submitButton(
                            $model->isNewRecord ? 'Tambah' : 'Update',
                            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
                        )
                        ?>
                </div>
            <?php } ?>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>