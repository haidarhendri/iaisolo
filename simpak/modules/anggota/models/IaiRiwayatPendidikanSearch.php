<?php

namespace simpak\modules\anggota\models;

use app\models\RefJenjang;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatPendidikan;
use common\models\IaiTransaksi;
use common\models\User;
use Yii;

/**
 * IaiRiwayatPendidikanSearch represents the model behind the search form about `common\models\IaiRiwayatPendidikan`.
 */
class IaiRiwayatPendidikanSearch extends IaiRiwayatPendidikan
{
    public $FULL_NAME;
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['FULL_NAME']);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'IS_UTAMA', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['JENJANG', 'NAMA_INSTITUSI', 'NOMOR_IJAZAH', 'TANGGAL_IJAZAH', 'FILE_IJAZAH',
            'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP', 'FULL_NAME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {        
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.FULL_NAME',
                'C.JENJANG',
            ])
            ->from(['A' => IaiRiwayatPendidikan::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
                'idJenjang' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefJenjang::tableName()]);
                },
            ], false)
            ->orderBy(['A.IS_UTAMA' => SORT_DESC]);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['B.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'A.TANGGAL_IJAZAH' => $this->TANGGAL_IJAZAH,
            'A.IS_UTAMA' => $this->IS_UTAMA,
            'A.CREATE_BY' => $this->CREATE_BY,
            'A.CREATE_DATE' => $this->CREATE_DATE,
            'A.UPDATE_BY' => $this->UPDATE_BY,
            'A.UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'C.JENJANG', $this->JENJANG])
            ->andFilterWhere(['like', 'A.NAMA_INSTITUSI', $this->NAMA_INSTITUSI])
            ->andFilterWhere(['like', 'A.NOMOR_IJAZAH', $this->NOMOR_IJAZAH])
            ->andFilterWhere(['like', 'B.FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'A.FILE_IJAZAH', $this->FILE_IJAZAH])
            ->andFilterWhere(['like', 'A.CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'A.UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_IJAZAH' => $this->TANGGAL_IJAZAH,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'JENJANG', $this->JENJANG])
            ->andFilterWhere(['like', 'NAMA_INSTITUSI', $this->NAMA_INSTITUSI])
            ->andFilterWhere(['like', 'NOMOR_IJAZAH', $this->NOMOR_IJAZAH])
            ->andFilterWhere(['like', 'FILE_IJAZAH', $this->FILE_IJAZAH])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatPendidikan::find();
    }

    public function cariPendidikanAktif()
    {
        $model = IaiRiwayatPendidikan::find()
            ->select(['B.ID AS ID_USER', 'A.ID_TRANSAKSI', 'A.IS_UTAMA'])
            ->from(['A' => IaiRiwayatPendidikan::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
            ], false)
            ->where(['B.ID' => Yii::$app->user->identity->ID])
            ->asArray()->all();

        return $model;
    }
}
