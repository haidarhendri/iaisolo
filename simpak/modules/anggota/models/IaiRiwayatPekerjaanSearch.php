<?php

namespace simpak\modules\anggota\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatPekerjaan;
use common\models\IaiTransaksi;
use common\models\User;
use Yii;

/**
 * IaiRiwayatPekerjaanSearch represents the model behind the search form about `common\models\IaiRiwayatPekerjaan`.
 */
class IaiRiwayatPekerjaanSearch extends IaiRiwayatPekerjaan
{
    public $FULL_NAME;

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['FULL_NAME']);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'IS_UTAMA', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['TEMPAT_KERJA', 'JABATAN', 'TANGGAL_AWAL', 'TANGGAL_AKHIR', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP', 'FULL_NAME'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.FULL_NAME'
            ])
            ->from(['A' => IaiRiwayatPekerjaan::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
            ], false)
            ->orderBy(['A.IS_UTAMA' => SORT_DESC]);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['B.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_AWAL' => $this->TANGGAL_AWAL,
            'TANGGAL_AKHIR' => $this->TANGGAL_AKHIR,
            'IS_UTAMA' => $this->IS_UTAMA,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'TEMPAT_KERJA', $this->TEMPAT_KERJA])
            ->andFilterWhere(['like', 'JABATAN', $this->JABATAN])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_AWAL' => $this->TANGGAL_AWAL,
            'TANGGAL_AKHIR' => $this->TANGGAL_AKHIR,
            'IS_UTAMA' => $this->IS_UTAMA,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'TEMPAT_KERJA', $this->TEMPAT_KERJA])
            ->andFilterWhere(['like', 'JABATAN', $this->JABATAN])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatPekerjaan::find();
    }

    public function cariPekerjaanAktif()
    {
        $model = IaiRiwayatPekerjaan::find()
            ->select(['B.ID AS ID_USER', 'A.ID_TRANSAKSI', 'A.IS_UTAMA'])
            ->from(['A' => IaiRiwayatPekerjaan::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
            ], false)
            ->where(['B.ID' => Yii::$app->user->identity->ID])
            ->asArray()->all();

        return $model;
    }
}
