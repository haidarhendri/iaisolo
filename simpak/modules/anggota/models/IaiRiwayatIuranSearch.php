<?php

namespace simpak\modules\anggota\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatIuran;
use common\models\User;
use simpak\models\RefIuran;
use Yii;

/**
 * IaiRiwayatIuranSearch represents the model behind the search form about `common\models\IaiRiwayatIuran`.
 */
class IaiRiwayatIuranSearch extends IaiRiwayatIuran
{
    public $FULL_NAME;
    public $NAMA_IURAN;
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['FULL_NAME', 'NAMA_IURAN']);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'STATUS_BAYAR', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['NOMINAL', 'TANGGAL_AWAL', 'TANGGAL_AKHIR', 'TANGGAL_BAYAR', 'CREATE_DATE',
            'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP', 'FULL_NAME', 'NAMA_IURAN'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.FULL_NAME',
                'C.NAMA_IURAN'
            ])
            ->from(['A' => IaiRiwayatIuran::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
                'jenisIuran' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefIuran::tableName()]);
                },
            ], false)
            ->orderBy([
                'A.TANGGAL_AWAL' => SORT_DESC,
                'A.STATUS_BAYAR' => SORT_ASC
            ]);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['B.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'STATUS_BAYAR' => $this->STATUS_BAYAR,
            'TANGGAL_AWAL' => $this->TANGGAL_AWAL,
            'TANGGAL_AKHIR' => $this->TANGGAL_AKHIR,
            'TANGGAL_BAYAR' => $this->TANGGAL_BAYAR,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'NOMINAL', $this->NOMINAL])
            // ->andFilterWhere(['like', 'STATUS_BAYAR', $this->STATUS_BAYAR])
            ->andFilterWhere(['like', 'FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'NAMA_IURAN', $this->NAMA_IURAN])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'TANGGAL_AWAL' => $this->TANGGAL_AWAL,
            'TANGGAL_AKHIR' => $this->TANGGAL_AKHIR,
            'TANGGAL_BAYAR' => $this->TANGGAL_BAYAR,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'NOMINAL', $this->NOMINAL])
            ->andFilterWhere(['like', 'STATUS_BAYAR', $this->STATUS_BAYAR])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatIuran::find();
    }
}
