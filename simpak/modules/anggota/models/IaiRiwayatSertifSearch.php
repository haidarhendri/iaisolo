<?php

namespace simpak\modules\anggota\models;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use common\models\IaiRiwayatSertif;
use common\models\RefJenisSertif;
use common\models\User;
use Yii;

/**
 * IaiRiwayatSertifSearch represents the model behind the search form about `common\models\IaiRiwayatSertif`.
 */
class IaiRiwayatSertifSearch extends IaiRiwayatSertif
{
    public $FULL_NAME;
    public $NAMA_SERTIF;
    
    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return array_merge(parent::attributes(), ['FULL_NAME']);
    }

    public function rules()
    {
        return [
            [['ID', 'ID_TRANSAKSI', 'ID_USER', 'JENIS_SERTIF', 'CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['NOMOR_SERTIF', 'TGL_AWAL', 'TGL_AKHIR', 'CREATE_DATE', 'UPDATE_DATE', 'CREATE_IP', 'UPDATE_IP',
            'FULL_NAME', 'NAMA_SERTIF'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user;
        $identity = $user->identity;

        $this->load($params);
        $query = static::query()
            ->select([
                'A.*',
                'B.FULL_NAME',
                'C.NAMA_SERTIF',
            ])
            ->from(['A' => IaiRiwayatSertif::tableName()])
            ->joinWith([
                'idUser' => function (ActiveQuery $q) {
                    return $q->from(['B' => User::tableName()]);
                },
                'idJenisSertif' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefJenisSertif::tableName()]);
                },
            ], false);

        if ($user->can('role_anggota') && !$user->can('role_superuser')) {
            $query->andWhere(['B.ID' => $identity->ID]);
        }

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'A.ID_USER' => $this->ID_USER,
            'A.TGL_AWAL' => $this->TGL_AWAL,
            'A.TGL_AKHIR' => $this->TGL_AKHIR,
            'A.CREATE_BY' => $this->CREATE_BY,
            'A.CREATE_DATE' => $this->CREATE_DATE,
            'A.UPDATE_BY' => $this->UPDATE_BY,
            'A.UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'A.NOMOR_SERTIF', $this->NOMOR_SERTIF])
            ->andFilterWhere(['like', 'B.FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'C.NAMA_SERTIF', $this->NAMA_SERTIF])
            ->andFilterWhere(['like', 'A.CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'A.UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        $query->andFilterWhere([
            'ID' => $this->ID,
            'ID_TRANSAKSI' => $this->ID_TRANSAKSI,
            'ID_USER' => $this->ID_USER,
            'JENIS_SERTIF' => $this->JENIS_SERTIF,
            'TGL_AWAL' => $this->TGL_AWAL,
            'TGL_AKHIR' => $this->TGL_AKHIR,
            'CREATE_BY' => $this->CREATE_BY,
            'CREATE_DATE' => $this->CREATE_DATE,
            'UPDATE_BY' => $this->UPDATE_BY,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'NOMOR_SERTIF', $this->NOMOR_SERTIF])
            ->andFilterWhere(['like', 'CREATE_IP', $this->CREATE_IP])
            ->andFilterWhere(['like', 'UPDATE_IP', $this->UPDATE_IP]);

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => 'ID',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return IaiRiwayatSertif::find();
    }
}
