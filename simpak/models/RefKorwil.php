<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "REF_KORWIL".
 *
 * @property int $ID
 * @property string|null $NAMA_WILAYAH
 */
class RefKorwil extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_KORWIL';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['NAMA_WILAYAH'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NAMA_WILAYAH' => 'Nama Wilayah',
        ];
    }
    
    public static function map($key = 'ID', $value = 'NAMA_WILAYAH', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, 'VALUE' => $value]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
