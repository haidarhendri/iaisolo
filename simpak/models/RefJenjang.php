<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "REF_JENJANG".
 *
 * @property int $ID
 * @property string $JENJANG
 */
class RefJenjang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_JENJANG';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['JENJANG'], 'required'],
            [['JENJANG'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'JENJANG' => 'Jenjang',
        ];
    }
}
