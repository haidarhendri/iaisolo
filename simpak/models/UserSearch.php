<?php

namespace simpak\models;

use common\models\User;
use common\models\AppAuthAssignment;
use common\models\referensi\RefAuthItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */

    public function attributes()
    {
        return array_merge(
            parent::attributes(),
            ['USER_ID', 'ITEM_NAME']
        );
    }

    public function rules()
    {
        return [
            [['ID', 'STATUS', 'USER_ID', 'ID_UNIT', 'ID_SUB_UNIT'], 'integer'],
            [['USERNAME', 'ITEM_NAME', 'EMAIL', 'FULL_NAME', 'DISPLAY_NAME', 'PASSWORD_HASH', 'PASSWORD_RESET_TOKEN', 'AUTH_KEY', 'CREATED_AT', 'UPDATED_AT', 'LOGIN_AT'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->select(['A.*', 'B.ITEM_NAME', 'B.USER_ID', 'C.NAME', 'C.TYPE', 'C.DESCRIPTION'])
            ->from(['A' => static::tableName()])
            ->joinWith([
                'appAuthAssignment' => function (ActiveQuery $q) {
                    return $q->from(['B' => AppAuthAssignment::tableName()]);
                },
                'appAuthAssignment.authItem' => function (ActiveQuery $q) {
                    return $q->from(['C' => RefAuthItem::tableName()]);
                }
            ], false)
            ->groupBy('A.ID');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'A.ID' => $this->ID,
            'A.STATUS' => $this->STATUS,
            'A.CREATED_AT' => $this->CREATED_AT,
            'A.UPDATED_AT' => $this->UPDATED_AT,
            'A.LOGIN_AT' => $this->LOGIN_AT,
            'A.ID_UNIT' => $this->ID_UNIT,
            'A.ID_SUB_UNIT' => $this->ID_SUB_UNIT,
        ]);

        $query->andFilterWhere(['like', 'A.USERNAME', $this->USERNAME])
            ->andFilterWhere(['like', 'A.EMAIL', $this->EMAIL])
            ->andFilterWhere(['like', 'A.FULL_NAME', $this->FULL_NAME])
            ->andFilterWhere(['like', 'A.DISPLAY_NAME', $this->DISPLAY_NAME])
            ->andFilterWhere(['like', 'A.PASSWORD_HASH', $this->PASSWORD_HASH])
            ->andFilterWhere(['like', 'A.PASSWORD_RESET_TOKEN', $this->PASSWORD_RESET_TOKEN])
            ->andFilterWhere(['like', 'A.AUTH_KEY', $this->AUTH_KEY])
            ->andFilterWhere(['like', 'B.ITEM_NAME', $this->ITEM_NAME])
            ->andFilterWhere(['like', 'B.USER_ID', $this->USER_ID]);

        return $dataProvider;
    }
}
