<?php

namespace simpak\models;

use Yii;
use yii\base\Application;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Request;

/**
 * This is the model class for table "REF_IURAN".
 *
 * @property int $ID
 * @property string|null $NAMA_IURAN
 * @property string|null $TGL_AWAL
 * @property string|null $TGL_AKHIR
 * @property string|null $NOMINAL
 * @property int|null $CREATE_BY
 * @property string|null $CREATE_DATE
 * @property int|null $UPDATE_BY
 * @property string|null $UPDATE_DATE
 * @property string|null $CREATE_IP
 * @property string|null $UPDATE_IP
 */
class RefIuran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'REF_IURAN';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['TGL_AWAL', 'TGL_AKHIR', 'CREATE_DATE', 'UPDATE_DATE'], 'safe'],
            [['CREATE_BY', 'UPDATE_BY'], 'integer'],
            [['NAMA_IURAN', 'NOMINAL'], 'string', 'max' => 255],
            [['CREATE_IP', 'UPDATE_IP'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'NAMA_IURAN' => 'Nama Iuran',
            'TGL_AWAL' => 'Tgl Awal',
            'TGL_AKHIR' => 'Tgl Akhir',
            'NOMINAL' => 'Nominal',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
            'CREATE_IP' => 'Create Ip',
            'UPDATE_IP' => 'Update Ip',
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        $userId = (Yii::$app instanceof Application && Yii::$app->user) ? Yii::$app->user->id : null;
        $userIP = (Yii::$app->request instanceof Request) ? Yii::$app->request->userIP : null;

        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->setAttributes([
                    'CREATE_DATE' => new Expression('NOW()'),
                    'CREATE_BY' => $userId,
                    'CREATE_IP' => $userIP,
                ]);
            }

            $this->setAttributes([
                'UPDATE_DATE' => new Expression('NOW()'),
                'UPDATE_BY' => $userId,
                'UPDATE_IP' => $userIP,
            ]);

            return true;
        }

        return false;
    }
    
    public static function map($key = 'ID', $value = 'AGENDA_NAMA', $conditions = [])
    {
        $query = static::find()->select(['KEY' => $key, 'VALUE' => $value]);

        if ($orderBy = ArrayHelper::remove($conditions, 'orderBy')) {
            $query->orderBy($orderBy);
        }

        if ($conditions) {
            $query->andWhere($conditions);
        }

        return ArrayHelper::map($query->asArray()->all(), 'KEY', 'VALUE');
    }
}
