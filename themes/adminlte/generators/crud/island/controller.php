<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->controllerClass);
$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $searchModelAlias = $searchModelClass . 'Search';
}

/* @var $class ActiveRecordInterface */
$class = $generator->modelClass;
$pks = $class::primaryKey();
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamComments = $generator->generateActionParamComments();
$singular = Inflector::singularize(Inflector::camel2words($modelClass));
$plural = Inflector::pluralize(Inflector::camel2words($modelClass));

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->controllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($generator->modelClass, '\\') ?>;
<?php if (!empty($generator->searchModelClass)): ?>
use <?= ltrim($generator->searchModelClass, '\\') . (isset($searchModelAlias) ? " as $searchModelAlias" : "") ?>;
<?php else: ?>
use yii\data\ActiveDataProvider;
<?php endif; ?>
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\helpers\Html;

/**
 * <?= $controllerClass ?> implements the CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all <?= $modelClass ?> models.
     * @return mixed
     */
    public function actionIndex()
    {    
<?php if (!empty($generator->searchModelClass)): ?>
        $searchModel = new <?= isset($searchModelAlias) ? $searchModelAlias : $searchModelClass ?>();
        $dpSearch = $searchModel->search(Yii::$app->request->queryParams);
        $dpExport = $searchModel->export(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dpSearch' => $dpSearch,
            'dpExport' => $dpExport,
        ]);
<?php else: ?>
        $dataProvider = new ActiveDataProvider([
            'query' => <?= $modelClass ?>::find(),
        ]);

        return $this->render('index', [
            'dpSearch' => $dataProvider,
            'dpExport' => clone $dataProvider,
        ]);
<?php endif; ?>
    }

    /**
     * Displays a single <?= $modelClass ?> model.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionView(<?= $actionParams ?>)
    {
        $request = Yii::$app->request;
        $model = $this->findModel(<?= $actionParams ?>);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => <?= $generator->generateString($singular) ?>,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        <?= $generator->generateString('Tutup') ?>,
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::a(
                        <?= $generator->generateString('Update') ?>,
                        ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                    ),
            ];
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new <?= $modelClass ?> model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new <?= $modelClass ?>();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => <?= $generator->generateString('Tambah ' . $singular) ?>,
                    'content' => '<span class="text-success">' . <?= $generator->generateString('Tambah ' . strtolower($singular) . ' berhasil') ?> . '</span>',
                    'footer' =>
                        Html::button(
                            <?= $generator->generateString('Tutup'); ?>,
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            <?= $generator->generateString('Tambah Lagi'); ?>,
                            ['create'],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => <?= $generator->generateString('Tambah ' . $singular) ?>,
                'content' => $this->renderAjax('create', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        <?= $generator->generateString('Tutup'); ?>,
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        <?= $generator->generateString('Simpan'); ?>,
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
            ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', <?= $generator->generateString('Tambah ' . strtolower($singular) . ' berhasil.') ?>);
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing <?= $modelClass ?> model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     * @throws \Throwable
     */
    public function actionUpdate(<?= $actionParams ?>)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($model->load($request->post()) && $model->save()) {
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'title' => <?= $generator->generateString('Update ' . $singular) ?>,
                    'content' => '<span class="text-success">' . <?= $generator->generateString('Update ' . strtolower($singular) . ' berhasil') ?> . '</span>',
                    'footer' =>
                        Html::button(
                            <?= $generator->generateString('Tutup') ?>,
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                        )
                        . Html::a(
                            <?= $generator->generateString('Update') ?>,
                            ['update', 'id' => $id],
                            ['class' => 'btn btn-primary', 'role' => 'modal-remote']
                        ),
                ];
            }

            return [
                'title' => <?= $generator->generateString('Update ' . $singular) ?>,
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                ]),
                'footer' =>
                    Html::button(
                        <?= $generator->generateString('Tutup') ?>,
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => 'modal']
                    )
                    . Html::button(
                        <?= $generator->generateString('Simpan') ?>,
                        ['class' => 'btn btn-primary', 'type' => 'submit']
                    ),
                ];
        }

        /*
         * Process for non-ajax request
         */
        if ($model->load($request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', <?= $generator->generateString('Update ' . strtolower($singular) . ' berhasil') ?>);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Delete an existing <?= $modelClass ?> model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     * @throws \Throwable
     */
    public function actionDelete(<?= $actionParams ?>)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', <?= $generator->generateString('Hapus ' . strtolower($singular) . 'berhasil') ?>);
        return $this->redirect(['index']);
    }

     /**
     * Delete multiple existing <?= $modelClass ?> model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys

        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
             * Process for ajax request
             */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        }

        /*
         * Process for non-ajax request
         */
        Yii::$app->session->setFlash('success', Yii::t('app', 'Hapus user berhasil.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?=                   $modelClass ?> the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
