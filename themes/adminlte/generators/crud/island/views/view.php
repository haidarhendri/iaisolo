<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$singular = Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
$plural = Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
echo "<?php\n";
?>

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

$this->title = <?= $generator->generateString($singular) ?>;
$this->params['breadcrumbs'][] = ['label' => <?= $generator->generateString('Dashboard ' . $singular) ?>, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-view">
    <div class="box box-danger box-solid">
        <div class="box-header">
            <div class="box-title"><?= "<?= " . $generator->generateString("Detail " . $singular) . " ?>" ?></div>
        </div>
        <div class="box-body">
            <?= "<?= " ?>DetailView::widget([
                'model' => $model,
                'attributes' => [
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "                    '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        echo "                    '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
    }
}
?>
                ],
            ]) ?>

        </div>
    </div>
</div>
