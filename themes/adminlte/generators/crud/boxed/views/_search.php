<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->searchModelClass, '\\') ?> */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss('.search-form{margin-bottom: 20px; padding-bottom: 5px; border-bottom: 1px solid #f4f4f4;}');
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-search search-form search-collapse collapse in">
    <?= "<?php " ?>$form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
<?php if ($generator->enablePjax): ?>
        'options' => [
            'data-pjax' => 1
        ],
<?php endif; ?>
    ]); ?>

<?php
$count = 0;
foreach ($generator->getColumnNames() as $attribute) {
    if (++$count < 6) {
        echo "    <?= " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    } else {
        echo "    <?php // echo " . $generator->generateActiveSearchField($attribute) . " ?>\n\n";
    }
}
?>
    <div class="form-group">
        <?= "<?= " ?>Html::submitButton(
            '<i class="fa fa-cog"></i> ' . <?= $generator->generateString('Proses') ?>,
            ['class' => 'btn btn-primary']
        ) ?>

        <?= "<?= " ?>Html::a(
            '<i class="glyphicon glyphicon-refresh"></i> ' . <?= $generator->generateString('Reset') ?>,
            ['index'],
            ['class' => 'btn btn-default']
        ) ?>

    </div>

    <?= "<?php " ?>ActiveForm::end(); ?>

</div>
