<?php

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$actionParams = $generator->generateActionParams();

echo "<?php\n";

?>

<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>

return [
<?php
if (empty($generator->searchModelClass)) {
    $count = 0;
    foreach ($generator->getColumnNames() as $name) {
        if (in_array($name, ['ID', 'CREATE_DATE', 'CREATE_BY', 'CREATE_IP', 'UPDATE_DATE', 'UPDATE_BY', 'UPDATE_IP'])){
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "    // ],\n";
        } else {
            echo "    [\n";
            echo "        'class'=>'\kartik\grid\DataColumn',\n";
            echo "        'attribute' => '" . $name . "',\n";
            echo "    ],\n";
        }
    }
} else {
    $count = 0;
    foreach ($generator->getColumnNames() as $name) {
        if (in_array($name, ['ID', 'CREATE_DATE', 'CREATE_BY', 'CREATE_IP', 'UPDATE_DATE', 'UPDATE_BY', 'UPDATE_IP'])){
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "        // 'label' => \$searchModel->getAttributeLabel('" . $name . "'),\n";
            echo "    // ],\n";
        } else {
            echo "    [\n";
            echo "        'class'=>'\kartik\grid\DataColumn',\n";
            echo "        'attribute' => '" . $name . "',\n";
            echo "        'label' => \$searchModel->getAttributeLabel('" . $name . "'),\n";
            echo "    ],\n";
        }
    }
}
?>
];
