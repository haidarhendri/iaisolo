<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$singular = Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
$plural = Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
echo "<?php\n";
?>
use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dpSearch yii\data\SqlDataProvider|yii\data\ActiveDataProvider */
/* @var $dpExport yii\data\SqlDataProvider|yii\data\ActiveDataProvider */

CrudAsset::register($this);
$this->registerCss('#crud-datatable th, #crud-datatable td{white-space:normal} #crud-datatable .panel{margin-bottom: 0}');

$this->title = <?= $generator->generateString('Dashboard ' . $singular) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <div class="box box-danger">
        <div class="box-header with-border">
            <?= '<?= '?>ExportMenu::widget([
                'container' => ['class' => 'btn-group', 'role' => 'group'],
                'dataProvider' => $dpExport,
                'columns' => require(__DIR__ . '/_columns-export.php'),
                'fontAwesome' => true,
                'columnSelectorOptions' => ['label' => 'Kolom'],
                'columnSelectorMenuOptions' => ['style' => ['height' => '240px', 'overflow-y' => 'auto']],
                'dropdownOptions' => ['label' => 'Export', 'class' => 'btn btn-default'],
                'batchSize' => $dpExport->pagination->pageSize,
                'target' => '_blank',
                'stream' => true,
                'deleteAfterSave' => true,
                'filename' => 'EXPORT-DATA-<?= strtoupper(Inflector::slug(Inflector::camel2id($singular))) ?>-' . date('Y-m-d'),
            ]); ?>

            <div class="pull-right">
                <?= '<?= ' ?>Html::button(
                    '<i class="glyphicon fa fa-search"></i> ' . <?= $generator->generateString('Pencarian') ?>,
                    [
                        'class' => 'btn btn-info search-button',
                        'data-toggle' => 'collapse',
                        'data-target' => '.search-collapse',
                    ]
                ) ?>

                <?= '<?= '?>Html::a(
                    '<i class="glyphicon glyphicon-refresh"></i>',
                    ['index'],
                    ['class' => 'btn btn-warning']
                ) ?>

            </div>
        </div>
        <div class="box-body">
            <?= '<?= ' ?>$this->render('_search', [
                'model' => $searchModel
            ]); ?>

            <?= '<?= ' ?>GridView::widget([
                'id' => 'crud-datatable',
                'dataProvider' => $dpSearch,
                'pjax' => true,
                'columns' => require(__DIR__ . '/_columns.php'),
                'toolbar' => [
                [
                    'content' =>
                        Html::a(
                            '<i class="glyphicon glyphicon-repeat"></i>',
                            ['index'],
                            ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => <?= $generator->generateString('Reset') ?>]
                        )
                        . '{toggleData}'
                        . '{export}',
                    ],
                ],
                'striped' => true,
                'condensed' => true,
                'responsive' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'type' => 'default',
                    'heading' => '<i class="glyphicon glyphicon-list"></i> ' . <?= $generator->generateString('Daftar ' . $singular) ?>,
                    'before' =>
                        Html::a(
                            '<i class="glyphicon glyphicon-plus"></i> ' . <?= $generator->generateString('Tambah') ?>,
                            ['create'],
                            ['role' => 'modal-remote', 'class' => 'btn btn-success']
                        ),
                    'after' => BulkButtonWidget::widget([
                        'buttons' => Html::a(
                            '<i class="glyphicon glyphicon-trash"></i> ' . <?= $generator->generateString('Delete All') ?>,
                            ['bulk-delete'],
                            [
                                'class' => 'btn btn-danger btn-xs',
                                'role' => 'modal-remote-bulk',
                                'data-confirm' => false,
                                'data-method' => false,
                                'data-request-method' => 'post',
                                'data-confirm-title' => <?= $generator->generateString('Apakah Anda Yakin?') ?>,
                                'data-confirm-message' => <?= $generator->generateString('Apakah anda yakin untuk menghapus item ini') ?>,
                            ]
                        ),
                    ])
                    . '<div class="clearfix"></div>',
                ],
            ]) ?>

        </div>
    </div>
</div>
<?= "<?php\n" ?>
Modal::begin([
    'id' => 'ajaxCrudModal',
    'footer' => '',
]);
Modal::end();
?>
