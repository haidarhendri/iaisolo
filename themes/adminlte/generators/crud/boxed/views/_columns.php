<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$actionParams = $generator->generateActionParams();

echo "<?php\n";

?>
use yii\helpers\Url;

<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
<?php
if (empty($generator->searchModelClass)) {
    $count = 0;
    foreach ($generator->getColumnNames() as $name) {
        if (in_array($name, ['ID', 'CREATE_DATE', 'CREATE_BY', 'CREATE_IP', 'UPDATE_DATE', 'UPDATE_BY', 'UPDATE_IP'])){
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "    // ],\n";
        } else if (++$count < 6) {
            echo "    [\n";
            echo "        'class'=>'\kartik\grid\DataColumn',\n";
            echo "        'attribute' => '" . $name . "',\n";
            echo "    ],\n";
        } else {
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "    // ],\n";
        }
    }
} else {
    $count = 0;
    foreach ($generator->getColumnNames() as $name) {
        if (in_array($name, ['ID', 'CREATE_DATE', 'CREATE_BY', 'CREATE_IP', 'UPDATE_DATE', 'UPDATE_BY', 'UPDATE_IP'])){
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "        // 'label' => \$searchModel->getAttributeLabel('" . $name . "'),\n";
            echo "    // ],\n";
        } else if (++$count < 6) {
            echo "    [\n";
            echo "        'class'=>'\kartik\grid\DataColumn',\n";
            echo "        'attribute' => '" . $name . "',\n";
            echo "        'label' => \$searchModel->getAttributeLabel('" . $name . "'),\n";
            echo "    ],\n";
        } else {
            echo "    // [\n";
            echo "        // 'class'=>'\kartik\grid\DataColumn',\n";
            echo "        // 'attribute' => '" . $name . "',\n";
            echo "        // 'label' => \$searchModel->getAttributeLabel('" . $name . "'),\n";
            echo "    // ],\n";
        }
    }
}
?>
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'top',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => <?= $generator->generateString('Detail') ?>, 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => <?= $generator->generateString('Update') ?>, 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => <?= $generator->generateString('Delete') ?>,
            'data-confirm' => false,
            'data-method' => false,
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => <?= $generator->generateString('Are you sure?') ?>,
            'data-confirm-message' => <?= $generator->generateString('Are you sure want to delete this item') ?>,
        ],
    ],
];
