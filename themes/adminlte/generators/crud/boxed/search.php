<?php
/**
 * This is the template for generating CRUD search class of the specified model.
 */

use yii\helpers\StringHelper;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelClass = StringHelper::basename($generator->modelClass);
$searchModelClass = StringHelper::basename($generator->searchModelClass);
if ($modelClass === $searchModelClass) {
    $modelAlias = $modelClass . 'Model';
}
$rules = $generator->generateSearchRules();
$labels = $generator->generateSearchLabels();
$searchAttributes = $generator->getSearchAttributes();
$searchConditions = $generator->generateSearchConditions();

echo "<?php\n";
?>

namespace <?= StringHelper::dirname(ltrim($generator->searchModelClass, '\\')) ?>;

use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\ActiveQuery;
use <?= ltrim($generator->modelClass, '\\') . (isset($modelAlias) ? " as $modelAlias" : "") ?>;

/**
 * <?= $searchModelClass ?> represents the model behind the search form about `<?= $generator->modelClass ?>`.
 */
class <?= $searchModelClass ?> extends <?= isset($modelAlias) ? $modelAlias : $modelClass ?>

{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            <?= implode(",\n            ", $rules) ?>,
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        <?= implode("\n        ", $searchConditions) ?>

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => '<?= $generator->modelClass::primaryKey()[0]; ?>',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Creates data provider instance with search query applied for export
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function export($params)
    {
        $this->load($params);
        $query = static::query();

        if (!$this->validate()) {
            // Don't show data when not valid
            $query->andWhere('0 = 1');
        }

        <?= implode("\n        ", $searchConditions) ?>

        return new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'key' => '<?= $generator->modelClass::primaryKey()[0]; ?>',
            'totalCount' => $query->count(),
            'sort' => ['attributes' => $this->attributes()],
            'pagination' => ['defaultPageSize' => 20],
        ]);
    }

    /**
     * Create query for data provider
     *
     * @return ActiveQuery
     */
    public static function query()
    {
        return <?= isset($modelAlias) ? $modelAlias : $modelClass ?>::find();
    }
}
