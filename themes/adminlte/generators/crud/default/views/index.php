<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$nameAttribute = $generator->getNameAttribute();
$singular = Inflector::singularize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
$plural = Inflector::pluralize(Inflector::camel2words(StringHelper::basename($generator->modelClass)));
echo "<?php\n";
?>
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
<?= !empty($generator->searchModelClass) ? "/* @var \$searchModel " . ltrim($generator->searchModelClass, '\\') . " */\n" : '' ?>
/* @var $dataProvider yii\data\ActiveDataProvider */

CrudAsset::register($this);

$this->title = <?= $generator->generateString('Dashboard ' . $singular) ?>;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="<?= Inflector::camel2id(StringHelper::basename($generator->modelClass)) ?>-index">
    <?= '<?= ' ?>GridView::widget([
        'id' => 'crud-datatable',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'columns' => require(__DIR__ . '/_columns.php'),
        'toolbar' => [
            [
                'content' =>
                    Html::a(
                        '<i class="glyphicon glyphicon-plus"></i>',
                        ['create'],
                        [
                            'role' => 'modal-remote',
                            'title' => <?= $generator->generateString('Tambah') ?>,
                            'class' => 'btn btn-default',
                        ]
                    )
                    . Html::a(
                        '<i class="glyphicon glyphicon-repeat"></i>',
                        ['index'],
                        ['data-pjax' => 1, 'class' => 'btn btn-default', 'title' => <?= $generator->generateString('Reset') ?>]
                    )
                    . '{toggleData}'
                    . '{export}',
            ],
        ],
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'responsiveWrap' => false,
        'panel' => [
            'type' => 'default',
            'heading' => '<i class="glyphicon glyphicon-list"></i> ' . <?= $generator->generateString('Daftar ' . $singular) ?>,
            'before' => '<em>* ' . <?= $generator->generateString('Ubah ukuran kolom tabel seperti pada excel dengan menarik tepian kolom.') ?> . '</em>',
            'after' => BulkButtonWidget::widget([
                'buttons' => Html::a(
                    '<i class="glyphicon glyphicon-trash"></i> ' . <?= $generator->generateString('Delete All') ?>,
                    ['bulk-delete'],
                    [
                        'class' => 'btn btn-danger btn-xs',
                        'role' => 'modal-remote-bulk',
                        'data-confirm' => false,
                        'data-method' => false,
                        'data-request-method' => 'post',
                        'data-confirm-title' => <?= $generator->generateString('Apakah Anda Yakin?') ?>,
                        'data-confirm-message' => <?= $generator->generateString('Apakah anda yakin untuk menghapus item ini') ?>,
                    ]
                ),
            ])
            . '<div class="clearfix"></div>',
        ],
    ]) ?>

</div>
<?= "<?php\n" ?>
Modal::begin([
    'id' => 'ajaxCrudModal',
    'footer' => '',
]);
Modal::end();
?>
