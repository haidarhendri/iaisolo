<?php
/**
 * @author Haidar H. Setyawan <13nightevil@gmail.com>
 * @copyright Copyright (c) 2019 Haidar H. Setyawan
 */

namespace themes\adminlte\assets;

use yii\web\AssetBundle;

/**
 * Class SlimScrollAsset
 * @package themes\adminlte\assets
 */
class SlimScrollAsset extends AssetBundle
{
    public $sourcePath = '@themes/adminlte/dist/slimscroll';

    public $js = [
        'jquery.slimscroll.min.js',
    ];
}
