<?php
/**
 * @author Haidar H. Setyawan <13nightevil@gmail.com>
 * @copyright Copyright (c) 2019 Haidar H. Setyawan
 */

namespace themes\adminlte\assets;

use yii\web\AssetBundle;

/**
 * Class FastClickAsset
 * @package themes\adminlte\assets
 */
class FastClickAsset extends AssetBundle
{
    public $sourcePath = '@themes/adminlte/dist/fastclick';

    public $js = [
        'fastclick.min.js',
    ];
}
