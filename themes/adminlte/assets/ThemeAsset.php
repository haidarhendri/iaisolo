<?php
/**
 * @link https://iaisolo.id/admin
 * @author Haidar H. Setyawan <haidarhendri7@gmail.com>
 * @copyright Copyright (c) 2019 NEXPLAN
 */

namespace themes\adminlte\assets;

use Exception;
use yii\helpers\ArrayHelper;
use yii\web\AssetBundle;

/**
 * Class ThemeAsset
 * @package themes\adminlte\assets
 */
class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@themes/adminlte/dist/adminlte';

    public $css = [
        'css/AdminLTE.min.css',
    ];

    public $js = [
        'js/app.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'themes\adminlte\assets\SlimScrollAsset',
        'themes\adminlte\assets\FastClickAsset',
    ];

    public $options = [
        'class' => 'sidebar-mini',
    ];

    public $left = true;

    public $right = true;

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function init()
    {
        // Append skin color file if specified
        $skin = $this->options['skin'] = ArrayHelper::getValue($this->options, 'skin', '_all-skins');

        if (('_all-skins' !== $skin) && (strpos($skin, 'skin-') !== 0)) {
            throw new Exception('Invalid skin specified');
        }

        $this->css[] = sprintf('css/skins/%s.min.css', $skin);
        parent::init();
    }

    /**
     * Generate css class for html body
     */
    public function getBodyClass()
    {
        return $this->options['skin'] . ' ' . $this->options['class'];
    }
}
